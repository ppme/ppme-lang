# PPME-Lang #

## Setup ##
We advice to setup this language implementation (PPME-Lang) in MPS via the top-level project [**PPME**](https://bitbucket.org/ppme/ppme) which will download an instance of MPS and all required plugins. Once you have imported the project properly in MPS you can just build all languages via `Build > Make Project`.

Afterwards, you can inspect the provided example for the Gray-Scott reaction-diffusion system located in the `ppml.sandbox` package.

## License ##
This project, the languages in PPME-Lang, is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

- - - 
[PPME@bitbucket](https://bitbucket.org/ppme/ppme) | [PPME-Lang@bitbucket](https://bitbucket.org/ppme/ppme-lang)