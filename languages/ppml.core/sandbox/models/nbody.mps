<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:059aabed-89f7-431c-917c-f75f682b9e2b(nbody)">
  <persistence version="9" />
  <languages>
    <use id="92d2ea16-5a42-4fdf-a676-c7604efe3504" name="de.slisson.mps.richtext" version="-1" />
    <devkit ref="2ddb73c1-8857-47ab-adb2-5c8e43ee1765(de.ppme.lang)" />
  </languages>
  <imports />
  <registry>
    <language id="7d7a484c-83ab-4a7c-b9f3-8672c02bd0f6" name="de.ppme.statements">
      <concept id="6199572117285223602" name="de.ppme.statements.structure.VariableDeclarationStatement" flags="ng" index="$eJ5c">
        <child id="6199572117285223607" name="variableDeclaration" index="$eJ59" />
      </concept>
      <concept id="6145176214748918949" name="de.ppme.statements.structure.VariableReference" flags="ng" index="B0vAg">
        <reference id="6145176214748918980" name="variableDeclaration" index="B0vBL" />
      </concept>
      <concept id="6145176214748596765" name="de.ppme.statements.structure.Statement" flags="ng" index="B3gsC" />
      <concept id="6145176214748596974" name="de.ppme.statements.structure.StatementList" flags="ng" index="B3gvr">
        <child id="6145176214748596975" name="statement" index="B3gvq" />
      </concept>
      <concept id="6145176214748598314" name="de.ppme.statements.structure.ExpressionStatement" flags="ng" index="B3hOv">
        <child id="6145176214748598315" name="expression" index="B3hOu" />
      </concept>
      <concept id="6145176214748692240" name="de.ppme.statements.structure.VariableDeclaration" flags="ng" index="B3B8_" />
      <concept id="7780396616246534422" name="de.ppme.statements.structure.AbstractLoopStatement" flags="ng" index="1wZMim">
        <child id="7780396616246534427" name="body" index="1wZMir" />
      </concept>
      <concept id="4084501901880777297" name="de.ppme.statements.structure.IfStatement" flags="ng" index="3EFsE7">
        <child id="4084501901880821507" name="ifTrue" index="3EF9vl" />
        <child id="4084501901880777616" name="condition" index="3EFsH6" />
      </concept>
      <concept id="4084501901880637513" name="de.ppme.statements.structure.PrintStatement" flags="ng" index="3EFUyv">
        <child id="2152092565556633494" name="interval" index="w7HHv" />
        <child id="4084501901880638497" name="expression" index="3EFUNR" />
      </concept>
    </language>
    <language id="a206eff4-e667-4146-8006-8cce4ef80954" name="de.ppme.modules">
      <concept id="6145176214748008889" name="de.ppme.modules.structure.Phase" flags="ng" index="Bt1Mc">
        <child id="6145176214748202931" name="body" index="BtKE6" />
      </concept>
      <concept id="8340651020914474434" name="de.ppme.modules.structure.Module" flags="ng" index="32DcKF">
        <reference id="7404899961792452988" name="cfg" index="HcygO" />
        <child id="6145176214748042437" name="listOfPhases" index="Bt9BK" />
        <child id="3240844216163288403" name="ctrlPropertySpecifications" index="3ScKZR" />
      </concept>
    </language>
    <language id="58df54a5-af81-48d5-bb5c-9db2412768a4" name="de.ppme.expressions">
      <concept id="4476466017894647997" name="de.ppme.expressions.structure.AbstractContainerType" flags="ng" index="3E5at">
        <child id="4476466017894648000" name="componentType" index="3E5bw" />
      </concept>
      <concept id="4476466017894647291" name="de.ppme.expressions.structure.VectorType" flags="ng" index="3E5Zr">
        <child id="6185001050544693460" name="ndim" index="3cXY_V" />
      </concept>
      <concept id="4476466017894626055" name="de.ppme.expressions.structure.MatrixType" flags="ng" index="3Ee$B" />
      <concept id="4476466017895421930" name="de.ppme.expressions.structure.VectorElementAccess" flags="ng" index="3R0Ra">
        <child id="4476466017895421931" name="vector" index="3R0Rb" />
        <child id="4476466017895421933" name="index" index="3R0Rd" />
      </concept>
      <concept id="8802928458142836436" name="de.ppme.expressions.structure.LessExpression" flags="ng" index="28VqSu" />
      <concept id="396783600242163430" name="de.ppme.expressions.structure.IntegerLiteral" flags="ng" index="cCXpN">
        <property id="396783600242257878" name="value" index="cC$t3" />
      </concept>
      <concept id="9083317248185289090" name="de.ppme.expressions.structure.VectorLiteral" flags="ng" index="mjilF">
        <child id="9083317248185300113" name="values" index="mjhxS" />
      </concept>
      <concept id="6145176214748482670" name="de.ppme.expressions.structure.PlusExpression" flags="ng" index="B2O5r" />
      <concept id="6145176214748483203" name="de.ppme.expressions.structure.MinusExpression" flags="ng" index="B2OeQ" />
      <concept id="6145176214748513808" name="de.ppme.expressions.structure.ParenthesizedExpression" flags="ng" index="B2WG_" />
      <concept id="6145176214748154964" name="de.ppme.expressions.structure.BinaryExpression" flags="ng" index="Bt$5x">
        <child id="6145176214748155369" name="left" index="Bt$3s" />
        <child id="6145176214748155371" name="right" index="Bt$3u" />
      </concept>
      <concept id="6145176214748154974" name="de.ppme.expressions.structure.AssignmentExpression" flags="ng" index="Bt$5F" />
      <concept id="6145176214748176626" name="de.ppme.expressions.structure.UnaryExpression" flags="ng" index="BtER7">
        <child id="6145176214748176627" name="expression" index="BtER6" />
      </concept>
      <concept id="6145176214748221724" name="de.ppme.expressions.structure.TrueLiteral" flags="ng" index="BtPKD" />
      <concept id="6145176214748221706" name="de.ppme.expressions.structure.BooleanLiteral" flags="ng" index="BtPKZ">
        <property id="7757862620602945941" name="value" index="YYatA" />
      </concept>
      <concept id="6145176214748259879" name="de.ppme.expressions.structure.DecimalLiteral" flags="ng" index="BtYGi">
        <property id="6145176214748259880" name="value" index="BtYGt" />
      </concept>
      <concept id="3234668756578971106" name="de.ppme.expressions.structure.MulExpression" flags="ng" index="2P4BvU" />
      <concept id="3234668756578971110" name="de.ppme.expressions.structure.DivExpression" flags="ng" index="2P4BvY" />
      <concept id="7192466915357234866" name="de.ppme.expressions.structure.RealType" flags="ng" index="ZjH0Z" />
      <concept id="7192466915357238192" name="de.ppme.expressions.structure.ITyped" flags="ng" index="ZjIkX">
        <child id="7192466915357238193" name="type" index="ZjIkW" />
      </concept>
      <concept id="7192466915357648203" name="de.ppme.expressions.structure.StringLiteral" flags="ng" index="Ztaf6">
        <property id="7192466915357648213" name="value" index="Ztafo" />
      </concept>
      <concept id="2547387476991160656" name="de.ppme.expressions.structure.ScientificNumberLiteral" flags="ng" index="34318T">
        <property id="2547387476991173723" name="prefix" index="3434sM" />
        <property id="2547387476991173726" name="postfix" index="3434sR" />
      </concept>
    </language>
    <language id="92d2ea16-5a42-4fdf-a676-c7604efe3504" name="de.slisson.mps.richtext">
      <concept id="2557074442922380897" name="de.slisson.mps.richtext.structure.Text" flags="ng" index="19SGf9">
        <child id="2557074442922392302" name="words" index="19SJt6" />
      </concept>
      <concept id="2557074442922438156" name="de.slisson.mps.richtext.structure.Word" flags="ng" index="19SUe$">
        <property id="2557074442922438158" name="escapedValue" index="19SUeA" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="f7af8208-82a7-4aab-9829-b32680223c52" name="de.ppme.ctrl">
      <concept id="6166975901158549475" name="de.ppme.ctrl.structure.CtrlPropertyReference" flags="ng" index="zp_wb">
        <reference id="6166975901158549476" name="ctrlProperty" index="zp_wc" />
      </concept>
      <concept id="6397211168290209327" name="de.ppme.ctrl.structure.CtrlProperty" flags="ng" index="2ZRwI6">
        <child id="6397211168290234457" name="value" index="2ZRDnK" />
      </concept>
      <concept id="6397211168290209299" name="de.ppme.ctrl.structure.Ctrl" flags="ng" index="2ZRwIU">
        <child id="6397211168290249098" name="properties" index="2ZRmKz" />
      </concept>
      <concept id="3240844216163201259" name="de.ppme.ctrl.structure.CtrlPropertySpecification" flags="ng" index="3ScbDf">
        <reference id="3240844216163202378" name="property" index="3ScbZI" />
        <child id="3240844216163202446" name="help_txt" index="3ScbWE" />
      </concept>
    </language>
    <language id="63e1a497-af15-4608-bf36-c0aa0aaf901b" name="de.ppme.core">
      <concept id="396783600243789055" name="de.ppme.core.structure.ArrowExpression" flags="ng" index="cIIhE">
        <child id="396783600243789433" name="operand" index="cIIrG" />
        <child id="396783600243789435" name="operation" index="cIIrI" />
      </concept>
      <concept id="396783600244191634" name="de.ppme.core.structure.PositionMemberAccess" flags="ng" index="cKd$7" />
      <concept id="943869364551957516" name="de.ppme.core.structure.CreateTopologyStatement" flags="ng" index="2xpR0z">
        <child id="943869364552020805" name="boundary_condition" index="2xpBXE" />
        <child id="943869364552020882" name="ghost_size" index="2xpBYX" />
      </concept>
      <concept id="943869364553727788" name="de.ppme.core.structure.CreateParticlesStatement" flags="ng" index="2xw7c3">
        <child id="943869364553728028" name="ghost_size" index="2xw0KN" />
        <child id="943869364553727941" name="topology" index="2xw7fE" />
        <child id="708996761100070897" name="body" index="P$RcX" />
        <child id="708996761100070720" name="displacementBody" index="P$Rec" />
      </concept>
      <concept id="929032500828069707" name="de.ppme.core.structure.PowerExpression" flags="ng" index="CgG_C" />
      <concept id="8007716293499730002" name="de.ppme.core.structure.ParticleListType" flags="ng" index="2Z2qvo">
        <reference id="8007716293502450881" name="plist" index="2ZsMdb" />
      </concept>
      <concept id="8007716293503019137" name="de.ppme.core.structure.ParticleMemberAccess" flags="ng" index="2ZuZsb">
        <reference id="8007716293503019139" name="decl" index="2ZuZs9" />
      </concept>
      <concept id="3413548270338329844" name="de.ppme.core.structure.PropertyType" flags="ng" index="32th3$">
        <child id="3413548270339400481" name="descr" index="32pvWL" />
        <child id="3413548270338330394" name="ndim" index="32thca" />
        <child id="3413548270338330401" name="zero" index="32thcL" />
      </concept>
      <concept id="6810947015889353876" name="de.ppme.core.structure.ComputeNeighlistStatment" flags="ng" index="34$yiH">
        <child id="6810947015889355265" name="parts" index="34$HCS" />
      </concept>
      <concept id="6810947015890677431" name="de.ppme.core.structure.ApplyBCMemberAccess" flags="ng" index="34FJqe" />
      <concept id="2945754496565952264" name="de.ppme.core.structure.TimeloopStatement" flags="ng" index="1jGXcj">
        <child id="2945754496565952266" name="body" index="1jGXch" />
      </concept>
      <concept id="2430378650381774185" name="de.ppme.core.structure.NeighborsExpression" flags="ng" index="1s29RC">
        <child id="2430378650381776079" name="parts" index="1s28he" />
        <child id="2430378650381776081" name="p" index="1s28hg" />
      </concept>
      <concept id="7988859962165330229" name="de.ppme.core.structure.ParticleLoopStatment" flags="ng" index="1zUE3U">
        <child id="7988859962166182297" name="variable" index="1zBU1m" />
        <child id="7988859962165330230" name="iterable" index="1zUE3T" />
      </concept>
      <concept id="5811480436907517931" name="de.ppme.core.structure.ParticleListMemberAccess" flags="ng" index="3_DU51">
        <reference id="5811480436907519203" name="decl" index="3_DUD9" />
      </concept>
      <concept id="6068049259195233127" name="de.ppme.core.structure.InlineCodeStatement" flags="ng" index="3DOnB1">
        <child id="6068049259195233128" name="inlineCode" index="3DOnBe" />
      </concept>
      <concept id="6068049259195233141" name="de.ppme.core.structure.CreateNeighborListStatement" flags="ng" index="3DOnBj">
        <child id="6068049259195233143" name="particles" index="3DOnBh" />
      </concept>
      <concept id="6068049259195440621" name="de.ppme.core.structure.MappingStatement" flags="ng" index="3DR9tb">
        <child id="6810947015889562546" name="parts" index="34BZeb" />
        <child id="6068049259195440622" name="mappingType" index="3DR9t8" />
      </concept>
      <concept id="6068049259195440626" name="de.ppme.core.structure.PartialMappingType" flags="ng" index="3DR9tk" />
      <concept id="6068049259195440624" name="de.ppme.core.structure.GhostMappingType" flags="ng" index="3DR9tm" />
      <concept id="6068049259194405279" name="de.ppme.core.structure.ParticleType" flags="ng" index="3DVdGT" />
    </language>
  </registry>
  <node concept="32DcKF" id="4ic06ho7b2z">
    <property role="TrG5h" value="nbody" />
    <ref role="HcygO" node="Opj2YGupV0" resolve="default" />
    <node concept="Bt1Mc" id="4ic06ho7n6R" role="Bt9BK">
      <property role="TrG5h" value="main" />
      <node concept="B3gvr" id="4ic06ho7n6S" role="BtKE6">
        <node concept="2xpR0z" id="4ic06ho7HHu" role="B3gvq">
          <property role="TrG5h" value="topo" />
          <node concept="Ztaf6" id="5mt6372TJVS" role="2xpBXE">
            <property role="Ztafo" value="ppm_param_bcdef_freespace" />
          </node>
          <node concept="zp_wb" id="4Zj96Mozsc8" role="2xpBYX">
            <ref role="zp_wc" node="Opj2YGut86" resolve="ghost_size" />
          </node>
        </node>
        <node concept="B3gsC" id="4ic06ho7KUh" role="B3gvq" />
        <node concept="2xw7c3" id="4ic06ho7KUq" role="B3gvq">
          <property role="TrG5h" value="pl" />
          <node concept="2Z2qvo" id="4ic06ho7KUr" role="ZjIkW">
            <ref role="2ZsMdb" node="4ic06ho7KUq" resolve="pl" />
            <node concept="3DVdGT" id="4ic06ho7KUs" role="3E5bw" />
          </node>
          <node concept="B0vAg" id="4ic06ho7Myx" role="2xw7fE">
            <ref role="B0vBL" node="4ic06ho7HHu" resolve="topo" />
          </node>
          <node concept="B3gvr" id="4ic06ho7PXO" role="P$RcX">
            <node concept="$eJ5c" id="4ic06ho8JAQ" role="B3gvq">
              <node concept="B3B8_" id="4ic06ho8JAR" role="$eJ59">
                <property role="TrG5h" value="v" />
                <node concept="32th3$" id="4ic06ho8JAW" role="ZjIkW">
                  <node concept="Ztaf6" id="4ic06ho8Lek" role="32pvWL">
                    <property role="Ztafo" value="velocity" />
                  </node>
                  <node concept="BtPKD" id="4ic06ho8MQW" role="32thcL">
                    <property role="YYatA" value="true" />
                  </node>
                  <node concept="cCXpN" id="4ic06ho8OMk" role="32thca">
                    <property role="cC$t3" value="3" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="$eJ5c" id="BkKQgi2cxA" role="B3gvq">
              <node concept="B3B8_" id="BkKQgi2cxC" role="$eJ59">
                <property role="TrG5h" value="m" />
                <node concept="32th3$" id="BkKQgi2cxO" role="ZjIkW">
                  <node concept="Ztaf6" id="BkKQgi2cyT" role="32pvWL">
                    <property role="Ztafo" value="mass" />
                  </node>
                  <node concept="cCXpN" id="BkKQgi2cyd" role="32thca">
                    <property role="cC$t3" value="1" />
                  </node>
                  <node concept="BtPKD" id="BkKQgi2cBt" role="32thcL">
                    <property role="YYatA" value="true" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="B3gvr" id="BkKQgi1QFe" role="P$Rec" />
          <node concept="zp_wb" id="4Zj96Mozt0s" role="2xw0KN">
            <ref role="zp_wc" node="Opj2YGut86" resolve="ghost_size" />
          </node>
        </node>
        <node concept="B3gsC" id="X$JffDi1vU" role="B3gvq" />
        <node concept="$eJ5c" id="X$JffDi9$6" role="B3gvq">
          <node concept="B3B8_" id="X$JffDi9$8" role="$eJ59">
            <property role="TrG5h" value="parts_data" />
            <node concept="3Ee$B" id="gLk$EQO3VA" role="ZjIkW">
              <node concept="ZjH0Z" id="gLk$EQO3W6" role="3E5bw" />
            </node>
          </node>
        </node>
        <node concept="3DOnB1" id="BkKQgi1lfV" role="B3gvq">
          <node concept="19SGf9" id="BkKQgi1lfX" role="3DOnBe">
            <node concept="19SUe$" id="BkKQgi1lg4" role="19SJt6">
              <property role="19SUeA" value="! - initialize particles ----------------------------------------------------&#10;  allocate(parts_data(pl%Npart, 7))&#10;  open(10, file='data.tab', action='read')&#10;  do i=1,pl%Npart&#10;    read(10, *) parts_data(i,:)&#10;  end do&#10;  close(10)&#10;&#10;  print *, &quot;== Initializing particle set ===========================================&quot;&#10;&#10;  foreach p in particles(pl) with positions(x, writex=true) vec_props(v,a) sca_props(m)&#10;    m_p = parts_data(p,1) &#10;    x_p(:) = parts_data(p,2:4)&#10;    v_p(:) = parts_data(p,5:7)&#10;    a_p(:) = 0&#10;  end foreach&#10;&#10;! - end initialize particles ------------------------------------------------&#10;" />
            </node>
          </node>
        </node>
        <node concept="B3gsC" id="4ic06ho8PN4" role="B3gvq" />
        <node concept="3DOnBj" id="4ic06ho8PN_" role="B3gvq">
          <property role="TrG5h" value="nl" />
          <node concept="B0vAg" id="4ic06ho8PPN" role="3DOnBh">
            <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
          </node>
        </node>
        <node concept="B3gsC" id="4ic06ho8Q$l" role="B3gvq" />
        <node concept="B3hOv" id="X$JffDjpn8" role="B3gvq">
          <node concept="cIIhE" id="X$JffDjpqM" role="B3hOu">
            <node concept="B0vAg" id="X$JffDjpqv" role="cIIrG">
              <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
            </node>
            <node concept="34FJqe" id="X$JffDjpOd" role="cIIrI" />
          </node>
        </node>
        <node concept="B3gsC" id="X$JffDjpdr" role="B3gvq" />
        <node concept="$eJ5c" id="BkKQgi2fdZ" role="B3gvq">
          <node concept="B3B8_" id="BkKQgi2fe1" role="$eJ59">
            <property role="TrG5h" value="dist" />
            <node concept="3E5Zr" id="BkKQgi2fl9" role="ZjIkW">
              <node concept="ZjH0Z" id="BkKQgi2fl_" role="3E5bw" />
              <node concept="cCXpN" id="22KE0GQpkSG" role="3cXY_V">
                <property role="cC$t3" value="3" />
              </node>
            </node>
          </node>
        </node>
        <node concept="$eJ5c" id="BkKQgi2h4Y" role="B3gvq">
          <node concept="B3B8_" id="BkKQgi2h50" role="$eJ59">
            <property role="TrG5h" value="r" />
            <node concept="ZjH0Z" id="BkKQgi2ieY" role="ZjIkW" />
          </node>
        </node>
        <node concept="$eJ5c" id="BkKQgi2uND" role="B3gvq">
          <node concept="B3B8_" id="BkKQgi2uNF" role="$eJ59">
            <property role="TrG5h" value="da" />
            <node concept="3E5Zr" id="BkKQgi2v0R" role="ZjIkW">
              <node concept="ZjH0Z" id="BkKQgi2v1j" role="3E5bw" />
              <node concept="cCXpN" id="22KE0GQplAf" role="3cXY_V">
                <property role="cC$t3" value="3" />
              </node>
            </node>
          </node>
        </node>
        <node concept="$eJ5c" id="gLk$EQOtkh" role="B3gvq">
          <node concept="B3B8_" id="gLk$EQOtki" role="$eJ59">
            <property role="TrG5h" value="sum_a" />
            <node concept="3E5Zr" id="gLk$EQOtkj" role="ZjIkW">
              <node concept="ZjH0Z" id="gLk$EQOtkk" role="3E5bw" />
              <node concept="cCXpN" id="gLk$EQOtkl" role="3cXY_V">
                <property role="cC$t3" value="3" />
              </node>
            </node>
          </node>
        </node>
        <node concept="B3gsC" id="gLk$EQOtgB" role="B3gvq" />
        <node concept="B3gsC" id="BkKQgi2f6V" role="B3gvq" />
        <node concept="3EFUyv" id="4Zj96MozIR8" role="B3gvq">
          <node concept="cIIhE" id="4Zj96MozIR9" role="3EFUNR">
            <node concept="B0vAg" id="4Zj96MozIRa" role="cIIrG">
              <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
            </node>
            <node concept="3_DU51" id="4Zj96MozIRb" role="cIIrI">
              <ref role="3_DUD9" node="4ic06ho8JAR" resolve="v" />
            </node>
          </node>
          <node concept="cIIhE" id="4Zj96MozIRc" role="3EFUNR">
            <node concept="B0vAg" id="4Zj96MozIRd" role="cIIrG">
              <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
            </node>
            <node concept="3_DU51" id="4Zj96MozIRe" role="cIIrI">
              <ref role="3_DUD9" node="BkKQgi2cxC" resolve="m" />
            </node>
          </node>
          <node concept="cCXpN" id="4Zj96MozIRf" role="w7HHv">
            <property role="cC$t3" value="1" />
          </node>
        </node>
        <node concept="1jGXcj" id="4ic06ho8Q$W" role="B3gvq">
          <property role="TrG5h" value="t" />
          <node concept="B3gvr" id="4ic06ho8Q$Y" role="1jGXch">
            <node concept="B3gsC" id="4Zj96MozM8$" role="B3gvq" />
            <node concept="3DR9tb" id="4Zj96MozNzt" role="B3gvq">
              <node concept="3DR9tk" id="4Zj96MozO29" role="3DR9t8" />
              <node concept="B0vAg" id="4Zj96MozO1X" role="34BZeb">
                <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
              </node>
            </node>
            <node concept="3DR9tb" id="4Zj96MozOWQ" role="B3gvq">
              <node concept="3DR9tm" id="4Zj96MozPrI" role="3DR9t8" />
              <node concept="B0vAg" id="4Zj96MozPry" role="34BZeb">
                <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
              </node>
            </node>
            <node concept="34$yiH" id="4Zj96MozQmK" role="B3gvq">
              <node concept="B0vAg" id="4Zj96MozQPy" role="34$HCS">
                <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
              </node>
            </node>
            <node concept="B3gsC" id="4Zj96MozMAQ" role="B3gvq" />
            <node concept="1zUE3U" id="BkKQgi2cMo" role="B3gvq">
              <node concept="B0vAg" id="BkKQgi2cMR" role="1zUE3T">
                <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
              </node>
              <node concept="B3B8_" id="BkKQgi2cMq" role="1zBU1m">
                <property role="TrG5h" value="p" />
                <node concept="3DVdGT" id="BkKQgi2cMz" role="ZjIkW" />
              </node>
              <node concept="B3gvr" id="BkKQgi2cMr" role="1wZMir">
                <node concept="B3hOv" id="gLk$EQOlJ2" role="B3gvq">
                  <node concept="Bt$5F" id="gLk$EQOmLp" role="B3hOu">
                    <node concept="cCXpN" id="gLk$EQOmLs" role="Bt$3u">
                      <property role="cC$t3" value="0" />
                    </node>
                    <node concept="B0vAg" id="gLk$EQOtqI" role="Bt$3s">
                      <ref role="B0vBL" node="gLk$EQOtki" resolve="sum_a" />
                    </node>
                  </node>
                </node>
                <node concept="1zUE3U" id="BkKQgi2cN8" role="B3gvq">
                  <node concept="1s29RC" id="BkKQgi2cNB" role="1zUE3T">
                    <node concept="B0vAg" id="BkKQgi2cNZ" role="1s28hg">
                      <ref role="B0vBL" node="BkKQgi2cMq" resolve="p" />
                    </node>
                    <node concept="B0vAg" id="BkKQgi2cOc" role="1s28he">
                      <ref role="B0vBL" node="4ic06ho8PN_" resolve="nl" />
                    </node>
                  </node>
                  <node concept="B3B8_" id="BkKQgi2cNa" role="1zBU1m">
                    <property role="TrG5h" value="q" />
                    <node concept="3DVdGT" id="BkKQgi2cNj" role="ZjIkW" />
                  </node>
                  <node concept="B3gvr" id="BkKQgi2cNb" role="1wZMir">
                    <node concept="B3hOv" id="BkKQgi2fXI" role="B3gvq">
                      <node concept="Bt$5F" id="BkKQgi2g49" role="B3hOu">
                        <node concept="B2OeQ" id="BkKQgi2glG" role="Bt$3u">
                          <node concept="cIIhE" id="BkKQgi2g$4" role="Bt$3u">
                            <node concept="B0vAg" id="BkKQgi2gsN" role="cIIrG">
                              <ref role="B0vBL" node="BkKQgi2cMq" resolve="p" />
                            </node>
                            <node concept="cKd$7" id="BkKQgi2gFO" role="cIIrI" />
                          </node>
                          <node concept="cIIhE" id="BkKQgi2g5I" role="Bt$3s">
                            <node concept="B0vAg" id="BkKQgi2g4N" role="cIIrG">
                              <ref role="B0vBL" node="BkKQgi2cNa" resolve="q" />
                            </node>
                            <node concept="cKd$7" id="BkKQgi2gdx" role="cIIrI" />
                          </node>
                        </node>
                        <node concept="B0vAg" id="BkKQgi2g4o" role="Bt$3s">
                          <ref role="B0vBL" node="BkKQgi2fe1" resolve="dist" />
                        </node>
                      </node>
                    </node>
                    <node concept="B3hOv" id="BkKQgi2inx" role="B3gvq">
                      <node concept="Bt$5F" id="BkKQgi2ivU" role="B3hOu">
                        <node concept="B0vAg" id="BkKQgi2iw9" role="Bt$3s">
                          <ref role="B0vBL" node="BkKQgi2h50" resolve="r" />
                        </node>
                        <node concept="CgG_C" id="BkKQgi2tDk" role="Bt$3u">
                          <node concept="B2WG_" id="BkKQgi2tix" role="Bt$3s">
                            <node concept="B2O5r" id="BkKQgi2jlB" role="BtER6">
                              <node concept="CgG_C" id="BkKQgi2jLY" role="Bt$3u">
                                <node concept="3R0Ra" id="BkKQgi2jCk" role="Bt$3s">
                                  <node concept="cCXpN" id="BkKQgi2jCr" role="3R0Rd">
                                    <property role="cC$t3" value="3" />
                                  </node>
                                  <node concept="B0vAg" id="BkKQgi2juX" role="3R0Rb">
                                    <ref role="B0vBL" node="BkKQgi2fe1" resolve="dist" />
                                  </node>
                                </node>
                                <node concept="cCXpN" id="BkKQgi2jV_" role="Bt$3u">
                                  <property role="cC$t3" value="2" />
                                </node>
                              </node>
                              <node concept="B2O5r" id="BkKQgi2iM3" role="Bt$3s">
                                <node concept="CgG_C" id="BkKQgi2kpo" role="Bt$3s">
                                  <node concept="3R0Ra" id="BkKQgi2iwR" role="Bt$3s">
                                    <node concept="cCXpN" id="BkKQgi2iDj" role="3R0Rd">
                                      <property role="cC$t3" value="1" />
                                    </node>
                                    <node concept="B0vAg" id="BkKQgi2iwm" role="3R0Rb">
                                      <ref role="B0vBL" node="BkKQgi2fe1" resolve="dist" />
                                    </node>
                                  </node>
                                  <node concept="cCXpN" id="BkKQgi2kzv" role="Bt$3u">
                                    <property role="cC$t3" value="2" />
                                  </node>
                                </node>
                                <node concept="CgG_C" id="BkKQgi2k5r" role="Bt$3u">
                                  <node concept="3R0Ra" id="BkKQgi2j3B" role="Bt$3s">
                                    <node concept="B0vAg" id="BkKQgi2iUJ" role="3R0Rb">
                                      <ref role="B0vBL" node="BkKQgi2fe1" resolve="dist" />
                                    </node>
                                    <node concept="cCXpN" id="22KE0GQpqyx" role="3R0Rd">
                                      <property role="cC$t3" value="2" />
                                    </node>
                                  </node>
                                  <node concept="cCXpN" id="BkKQgi2kfh" role="Bt$3u">
                                    <property role="cC$t3" value="2" />
                                  </node>
                                </node>
                              </node>
                            </node>
                          </node>
                          <node concept="B2WG_" id="4Zj96MozJQB" role="Bt$3u">
                            <node concept="2P4BvY" id="4Zj96MozJQC" role="BtER6">
                              <node concept="cCXpN" id="4Zj96MozJQD" role="Bt$3u">
                                <property role="cC$t3" value="2" />
                              </node>
                              <node concept="cCXpN" id="4Zj96MozJQE" role="Bt$3s">
                                <property role="cC$t3" value="1" />
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="3EFsE7" id="4Zj96MozKj8" role="B3gvq">
                      <node concept="B3gvr" id="4Zj96MozKja" role="3EF9vl">
                        <node concept="B3hOv" id="4Zj96MozLgd" role="B3gvq">
                          <node concept="Bt$5F" id="4Zj96MozLgw" role="B3hOu">
                            <node concept="zp_wb" id="4Zj96MozLGz" role="Bt$3u">
                              <ref role="zp_wc" node="ti97EHI3pg" resolve="epsilon" />
                            </node>
                            <node concept="B0vAg" id="4Zj96MozLgl" role="Bt$3s">
                              <ref role="B0vBL" node="BkKQgi2h50" resolve="r" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="28VqSu" id="4Zj96MozKoI" role="3EFsH6">
                        <node concept="zp_wb" id="4Zj96MozKOp" role="Bt$3u">
                          <ref role="zp_wc" node="ti97EHI3pg" resolve="epsilon" />
                        </node>
                        <node concept="B0vAg" id="4Zj96MozKkk" role="Bt$3s">
                          <ref role="B0vBL" node="BkKQgi2h50" resolve="r" />
                        </node>
                      </node>
                    </node>
                    <node concept="B3hOv" id="BkKQgi2v23" role="B3gvq">
                      <node concept="Bt$5F" id="BkKQgi2veE" role="B3hOu">
                        <node concept="B0vAg" id="BkKQgi2veq" role="Bt$3s">
                          <ref role="B0vBL" node="BkKQgi2uNF" resolve="da" />
                        </node>
                        <node concept="2P4BvU" id="BkKQgi2RR8" role="Bt$3u">
                          <node concept="zp_wb" id="BkKQgi2RRb" role="Bt$3u">
                            <ref role="zp_wc" node="BkKQgi2PgQ" resolve="G" />
                          </node>
                          <node concept="2P4BvU" id="BkKQgi2vfX" role="Bt$3s">
                            <node concept="cIIhE" id="BkKQgi2vg4" role="Bt$3s">
                              <node concept="B0vAg" id="BkKQgi2vg5" role="cIIrG">
                                <ref role="B0vBL" node="BkKQgi2cNa" resolve="q" />
                              </node>
                              <node concept="2ZuZsb" id="BkKQgi2vg6" role="cIIrI">
                                <ref role="2ZuZs9" node="BkKQgi2cxC" resolve="m" />
                              </node>
                            </node>
                            <node concept="B2WG_" id="BkKQgi2vfY" role="Bt$3u">
                              <node concept="2P4BvY" id="BkKQgi2vfZ" role="BtER6">
                                <node concept="CgG_C" id="BkKQgi2vg0" role="Bt$3u">
                                  <node concept="B0vAg" id="BkKQgi2vg1" role="Bt$3s">
                                    <ref role="B0vBL" node="BkKQgi2h50" resolve="r" />
                                  </node>
                                  <node concept="cCXpN" id="BkKQgi2vg2" role="Bt$3u">
                                    <property role="cC$t3" value="3" />
                                  </node>
                                </node>
                                <node concept="B0vAg" id="BkKQgi2vg3" role="Bt$3s">
                                  <ref role="B0vBL" node="BkKQgi2fe1" resolve="dist" />
                                </node>
                              </node>
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                    <node concept="B3hOv" id="BkKQgi2cOz" role="B3gvq">
                      <node concept="Bt$5F" id="BkKQgi2cOF" role="B3hOu">
                        <node concept="B2O5r" id="BkKQgi2w81" role="Bt$3u">
                          <node concept="B0vAg" id="BkKQgi2wlv" role="Bt$3u">
                            <ref role="B0vBL" node="BkKQgi2uNF" resolve="da" />
                          </node>
                          <node concept="B0vAg" id="gLk$EQOuGv" role="Bt$3s">
                            <ref role="B0vBL" node="gLk$EQOtki" resolve="sum_a" />
                          </node>
                        </node>
                        <node concept="B0vAg" id="gLk$EQOugH" role="Bt$3s">
                          <ref role="B0vBL" node="gLk$EQOtki" resolve="sum_a" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="B3hOv" id="BkKQgi2xtJ" role="B3gvq">
                  <node concept="Bt$5F" id="BkKQgi2xGk" role="B3hOu">
                    <node concept="B2O5r" id="BkKQgi2yFD" role="Bt$3u">
                      <node concept="2P4BvU" id="BkKQgi2zHA" role="Bt$3u">
                        <node concept="zp_wb" id="BkKQgi2$kF" role="Bt$3u">
                          <ref role="zp_wc" node="ti97EHIgqy" resolve="delta_t" />
                        </node>
                        <node concept="B0vAg" id="gLk$EQOv7W" role="Bt$3s">
                          <ref role="B0vBL" node="gLk$EQOtki" resolve="sum_a" />
                        </node>
                      </node>
                      <node concept="cIIhE" id="BkKQgi2ybf" role="Bt$3s">
                        <node concept="B0vAg" id="BkKQgi2xWB" role="cIIrG">
                          <ref role="B0vBL" node="BkKQgi2cMq" resolve="p" />
                        </node>
                        <node concept="2ZuZsb" id="BkKQgi2yqt" role="cIIrI">
                          <ref role="2ZuZs9" node="4ic06ho8JAR" resolve="v" />
                        </node>
                      </node>
                    </node>
                    <node concept="cIIhE" id="BkKQgi2xGN" role="Bt$3s">
                      <node concept="B0vAg" id="BkKQgi2xGz" role="cIIrG">
                        <ref role="B0vBL" node="BkKQgi2cMq" resolve="p" />
                      </node>
                      <node concept="2ZuZsb" id="BkKQgi2xV7" role="cIIrI">
                        <ref role="2ZuZs9" node="4ic06ho8JAR" resolve="v" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="B3hOv" id="gLk$EQOniJ" role="B3gvq">
                  <node concept="Bt$5F" id="gLk$EQOniK" role="B3hOu">
                    <node concept="B2O5r" id="gLk$EQOniL" role="Bt$3u">
                      <node concept="cIIhE" id="gLk$EQOniM" role="Bt$3u">
                        <node concept="B0vAg" id="gLk$EQOniN" role="cIIrG">
                          <ref role="B0vBL" node="BkKQgi2cMq" resolve="p" />
                        </node>
                        <node concept="cKd$7" id="gLk$EQOniO" role="cIIrI" />
                      </node>
                      <node concept="2P4BvU" id="gLk$EQOniZ" role="Bt$3s">
                        <node concept="cIIhE" id="gLk$EQOnj0" role="Bt$3s">
                          <node concept="B0vAg" id="gLk$EQOnj1" role="cIIrG">
                            <ref role="B0vBL" node="BkKQgi2cMq" resolve="p" />
                          </node>
                          <node concept="2ZuZsb" id="gLk$EQOnj2" role="cIIrI">
                            <ref role="2ZuZs9" node="4ic06ho8JAR" resolve="v" />
                          </node>
                        </node>
                        <node concept="zp_wb" id="gLk$EQOnj3" role="Bt$3u">
                          <ref role="zp_wc" node="ti97EHIgqy" resolve="delta_t" />
                        </node>
                      </node>
                    </node>
                    <node concept="cIIhE" id="gLk$EQOnj4" role="Bt$3s">
                      <node concept="B0vAg" id="gLk$EQOnj5" role="cIIrG">
                        <ref role="B0vBL" node="BkKQgi2cMq" resolve="p" />
                      </node>
                      <node concept="cKd$7" id="gLk$EQOnj6" role="cIIrI" />
                    </node>
                  </node>
                </node>
                <node concept="B3gsC" id="gLk$EQOnfG" role="B3gvq" />
              </node>
            </node>
            <node concept="B3gsC" id="X$JffDjqgH" role="B3gvq" />
            <node concept="B3hOv" id="X$JffDjqn5" role="B3gvq">
              <node concept="cIIhE" id="X$JffDjqn6" role="B3hOu">
                <node concept="B0vAg" id="X$JffDjqn7" role="cIIrG">
                  <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
                </node>
                <node concept="34FJqe" id="X$JffDjqn8" role="cIIrI" />
              </node>
            </node>
            <node concept="B3gsC" id="X$JffDjsz5" role="B3gvq" />
            <node concept="B3hOv" id="BkKQgi2Hkg" role="B3gvq">
              <node concept="Bt$5F" id="BkKQgi2HEW" role="B3hOu">
                <node concept="B2O5r" id="BkKQgi2IkU" role="Bt$3u">
                  <node concept="zp_wb" id="BkKQgi2IDX" role="Bt$3u">
                    <ref role="zp_wc" node="ti97EHIgqy" resolve="delta_t" />
                  </node>
                  <node concept="B0vAg" id="BkKQgi2HZV" role="Bt$3s">
                    <ref role="B0vBL" node="4ic06ho8Q$W" resolve="t" />
                  </node>
                </node>
                <node concept="B0vAg" id="BkKQgi2HEK" role="Bt$3s">
                  <ref role="B0vBL" node="4ic06ho8Q$W" resolve="t" />
                </node>
              </node>
            </node>
            <node concept="B3gsC" id="X$JffDjqeF" role="B3gvq" />
            <node concept="3EFUyv" id="BkKQgi2L7n" role="B3gvq">
              <node concept="cIIhE" id="BkKQgi2LuJ" role="3EFUNR">
                <node concept="B0vAg" id="BkKQgi2Luv" role="cIIrG">
                  <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
                </node>
                <node concept="3_DU51" id="BkKQgi2LOI" role="cIIrI">
                  <ref role="3_DUD9" node="4ic06ho8JAR" resolve="v" />
                </node>
              </node>
              <node concept="cIIhE" id="BkKQgi2LR4" role="3EFUNR">
                <node concept="B0vAg" id="BkKQgi2LQC" role="cIIrG">
                  <ref role="B0vBL" node="4ic06ho7KUq" resolve="pl" />
                </node>
                <node concept="3_DU51" id="BkKQgi2MdM" role="cIIrI">
                  <ref role="3_DUD9" node="BkKQgi2cxC" resolve="m" />
                </node>
              </node>
              <node concept="cCXpN" id="BkKQgi2LPI" role="w7HHv">
                <property role="cC$t3" value="1" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3ScbDf" id="1J0zIof1ePR" role="3ScKZR">
      <ref role="3ScbZI" node="ti97EHI3pg" resolve="epsilon" />
      <node concept="Ztaf6" id="1J0zIof1eQ0" role="3ScbWE">
        <property role="Ztafo" value="cut-off distance for close particles" />
      </node>
    </node>
  </node>
  <node concept="2ZRwIU" id="Opj2YGupV0">
    <property role="TrG5h" value="default" />
    <node concept="2ZRwI6" id="ti97EHI3pg" role="2ZRmKz">
      <property role="TrG5h" value="epsilon" />
      <node concept="BtYGi" id="1ryP$t5Fkkn" role="2ZRDnK">
        <property role="BtYGt" value="0.1" />
      </node>
    </node>
    <node concept="2ZRwI6" id="ti97EHIgqy" role="2ZRmKz">
      <property role="TrG5h" value="delta_t" />
      <node concept="BtYGi" id="BkKQgi2Xjc" role="2ZRDnK">
        <property role="BtYGt" value="0.1" />
      </node>
    </node>
    <node concept="2ZRwI6" id="ON9Gjnksgh" role="2ZRmKz">
      <property role="TrG5h" value="min_phys" />
      <node concept="mjilF" id="ON9GjnksyY" role="2ZRDnK">
        <node concept="34318T" id="1ryP$t5Fku$" role="mjhxS">
          <property role="3434sM" value="-10" />
          <property role="3434sR" value="6" />
        </node>
        <node concept="34318T" id="1ryP$t5Fsd4" role="mjhxS">
          <property role="3434sM" value="-10" />
          <property role="3434sR" value="6" />
        </node>
        <node concept="34318T" id="1ryP$t5FsdH" role="mjhxS">
          <property role="3434sM" value="-10" />
          <property role="3434sR" value="6" />
        </node>
      </node>
    </node>
    <node concept="2ZRwI6" id="ON9GjnksGD" role="2ZRmKz">
      <property role="TrG5h" value="max_phys" />
      <node concept="mjilF" id="ON9GjnksGE" role="2ZRDnK">
        <node concept="34318T" id="1ryP$t5Fsfi" role="mjhxS">
          <property role="3434sM" value="10" />
          <property role="3434sR" value="6" />
        </node>
        <node concept="34318T" id="1ryP$t5Fsgm" role="mjhxS">
          <property role="3434sM" value="10" />
          <property role="3434sR" value="6" />
        </node>
        <node concept="34318T" id="1ryP$t5FsgZ" role="mjhxS">
          <property role="3434sM" value="10" />
          <property role="3434sR" value="6" />
        </node>
      </node>
    </node>
    <node concept="2ZRwI6" id="Opj2YGusV2" role="2ZRmKz">
      <property role="TrG5h" value="domain_decomposition" />
      <node concept="cCXpN" id="m1E9k9cirt" role="2ZRDnK">
        <property role="cC$t3" value="7" />
      </node>
    </node>
    <node concept="2ZRwI6" id="Opj2YGut7G" role="2ZRmKz">
      <property role="TrG5h" value="processor_assignment" />
      <node concept="cCXpN" id="m1E9k9citJ" role="2ZRDnK">
        <property role="cC$t3" value="1" />
      </node>
    </node>
    <node concept="2ZRwI6" id="Opj2YGut86" role="2ZRmKz">
      <property role="TrG5h" value="ghost_size" />
      <node concept="BtYGi" id="Opj2YGutgh" role="2ZRDnK">
        <property role="BtYGt" value="0.1" />
      </node>
    </node>
    <node concept="2ZRwI6" id="Opj2YGut8y" role="2ZRmKz">
      <property role="TrG5h" value="Npart" />
      <node concept="cCXpN" id="m1E9k9ciw1" role="2ZRDnK">
        <property role="cC$t3" value="8192" />
      </node>
    </node>
    <node concept="2ZRwI6" id="Opj2YGut90" role="2ZRmKz">
      <property role="TrG5h" value="start_time" />
      <node concept="BtYGi" id="Opj2YGuthi" role="2ZRDnK">
        <property role="BtYGt" value="0.0" />
      </node>
    </node>
    <node concept="2ZRwI6" id="Opj2YGut9w" role="2ZRmKz">
      <property role="TrG5h" value="stop_time" />
      <node concept="BtYGi" id="Opj2YGuthS" role="2ZRDnK">
        <property role="BtYGt" value="80.0" />
      </node>
    </node>
    <node concept="2ZRwI6" id="BkKQgi2PgQ" role="2ZRmKz">
      <property role="TrG5h" value="G" />
      <node concept="BtYGi" id="1ryP$t5Fkli" role="2ZRDnK">
        <property role="BtYGt" value="1.0" />
      </node>
    </node>
  </node>
</model>

