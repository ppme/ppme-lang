<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b4b8b7dd-6e2e-4b00-82a2-415d6c16bcc2(de.ppme.expressions.actions)">
  <persistence version="9" />
  <languages>
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="-1" />
    <use id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc" version="-1" />
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="0" />
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="as9o" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/f:java_stub#3f233e7f-b8a6-46d2-a57f-795d56775243#org.jetbrains.annotations(Annotations/org.jetbrains.annotations@java_stub)" />
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" />
    <import index="bkkh" ref="r:0249b848-8a19-49ef-a019-de89bf2328cf(de.ppme.expressions.behavior)" />
    <import index="e2lb" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" implicit="true" />
  </imports>
  <registry>
    <language id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts">
      <concept id="1194033889146" name="jetbrains.mps.lang.sharedConcepts.structure.ConceptFunctionParameter_editorContext" flags="nn" index="1XNTG" />
      <concept id="1161622665029" name="jetbrains.mps.lang.sharedConcepts.structure.ConceptFunctionParameter_model" flags="nn" index="1Q6Npb" />
    </language>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="4510086454722552739" name="jetbrains.mps.lang.editor.structure.PropertyDeclarationCellSelector" flags="ng" index="eBIwv">
        <reference id="4510086454740628767" name="propertyDeclaration" index="fyFUz" />
      </concept>
      <concept id="3547227755871693971" name="jetbrains.mps.lang.editor.structure.PredefinedSelector" flags="ng" index="2B6iha">
        <property id="2162403111523065396" name="cellId" index="1lyBwo" />
      </concept>
      <concept id="3647146066980922272" name="jetbrains.mps.lang.editor.structure.SelectInEditorOperation" flags="nn" index="1OKiuA">
        <child id="1948540814633499358" name="editorContext" index="lBI5i" />
        <child id="1948540814635895774" name="cellSelector" index="lGT1i" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="1082485599095" name="jetbrains.mps.baseLanguage.structure.BlockStatement" flags="nn" index="9aQIb">
        <child id="1082485599096" name="statements" index="9aQI4" />
      </concept>
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1076505808687" name="jetbrains.mps.baseLanguage.structure.WhileStatement" flags="nn" index="2$JKZl">
        <child id="1076505808688" name="condition" index="2$JKZa" />
      </concept>
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1083245097125" name="jetbrains.mps.baseLanguage.structure.EnumClass" flags="ig" index="Qs71p">
        <child id="1083245396908" name="enumConstant" index="Qtgdg" />
      </concept>
      <concept id="1083245299891" name="jetbrains.mps.baseLanguage.structure.EnumConstantDeclaration" flags="ig" index="QsSxf" />
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1082485599094" name="ifFalseStatement" index="9aQIa" />
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081506773034" name="jetbrains.mps.baseLanguage.structure.LessThanExpression" flags="nn" index="3eOVzh" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1081855346303" name="jetbrains.mps.baseLanguage.structure.BreakStatement" flags="nn" index="3zACq4" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1144230876926" name="jetbrains.mps.baseLanguage.structure.AbstractForStatement" flags="nn" index="1DupvO">
        <child id="1144230900587" name="variable" index="1Duv9x" />
      </concept>
      <concept id="1144231330558" name="jetbrains.mps.baseLanguage.structure.ForStatement" flags="nn" index="1Dw8fO">
        <child id="1144231399730" name="condition" index="1Dwp0S" />
        <child id="1144231408325" name="iteration" index="1Dwrff" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="f2801650-65d5-424e-bb1b-463a8781b786" name="jetbrains.mps.baseLanguage.javadoc">
      <concept id="6832197706140518104" name="jetbrains.mps.baseLanguage.javadoc.structure.DocMethodParameterReference" flags="ng" index="zr_55" />
      <concept id="6832197706140518103" name="jetbrains.mps.baseLanguage.javadoc.structure.BaseParameterReference" flags="ng" index="zr_5a">
        <reference id="6832197706140518108" name="param" index="zr_51" />
      </concept>
      <concept id="5349172909345501395" name="jetbrains.mps.baseLanguage.javadoc.structure.BaseDocComment" flags="ng" index="P$AiS">
        <child id="8465538089690331502" name="body" index="TZ5H$" />
      </concept>
      <concept id="5349172909345532724" name="jetbrains.mps.baseLanguage.javadoc.structure.MethodDocComment" flags="ng" index="P$JXv">
        <child id="8465538089690917625" name="param" index="TUOzN" />
      </concept>
      <concept id="8465538089690881930" name="jetbrains.mps.baseLanguage.javadoc.structure.ParameterBlockDocTag" flags="ng" index="TUZQ0">
        <property id="8465538089690881934" name="text" index="TUZQ4" />
        <child id="6832197706140518123" name="parameter" index="zr_5Q" />
      </concept>
      <concept id="8465538089690331500" name="jetbrains.mps.baseLanguage.javadoc.structure.CommentLine" flags="ng" index="TZ5HA">
        <child id="8970989240999019149" name="part" index="1dT_Ay" />
      </concept>
      <concept id="8970989240999019143" name="jetbrains.mps.baseLanguage.javadoc.structure.TextCommentLinePart" flags="ng" index="1dT_AC">
        <property id="8970989240999019144" name="text" index="1dT_AB" />
      </concept>
      <concept id="2068944020170241612" name="jetbrains.mps.baseLanguage.javadoc.structure.ClassifierDocComment" flags="ng" index="3UR2Jj" />
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1176544042499" name="jetbrains.mps.lang.typesystem.structure.Node_TypeOperation" flags="nn" index="3JvlWi" />
      <concept id="1178870617262" name="jetbrains.mps.lang.typesystem.structure.CoerceExpression" flags="nn" index="1UaxmW">
        <child id="1178870894644" name="pattern" index="1Ub_4A" />
        <child id="1178870894645" name="nodeToCoerce" index="1Ub_4B" />
      </concept>
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="1196433923911" name="jetbrains.mps.lang.actions.structure.SideTransform_SimpleString" flags="nn" index="2h1dTh">
        <property id="1196433942569" name="text" index="2h1i$Z" />
      </concept>
      <concept id="1177323996388" name="jetbrains.mps.lang.actions.structure.AddMenuPart" flags="ng" index="tYCnQ" />
      <concept id="1177327161126" name="jetbrains.mps.lang.actions.structure.QueryFunction_CanSubstitute" flags="in" index="uaGSO" />
      <concept id="1177327274449" name="jetbrains.mps.lang.actions.structure.ConceptFunctionParameter_pattern" flags="nn" index="ub8z3" />
      <concept id="1177327570013" name="jetbrains.mps.lang.actions.structure.QueryFunction_Substitute_Handler" flags="in" index="ucgPf" />
      <concept id="1177327666243" name="jetbrains.mps.lang.actions.structure.SimpleItemSubstitutePart" flags="ng" index="ucClh">
        <child id="1177327698839" name="canSubstitute" index="ucKa5" />
        <child id="1177327709106" name="handler" index="ucMEw" />
        <child id="1177336013307" name="matchingText" index="uGu3D" />
      </concept>
      <concept id="1177333529597" name="jetbrains.mps.lang.actions.structure.ConceptPart" flags="ng" index="uyZFJ">
        <reference id="1177333551023" name="concept" index="uz4UX" />
        <child id="1177333559040" name="part" index="uz6Si" />
      </concept>
      <concept id="1177335944525" name="jetbrains.mps.lang.actions.structure.QueryFunction_SubstituteString" flags="in" index="uGdhv" />
      <concept id="1177497140107" name="jetbrains.mps.lang.actions.structure.ConceptFunctionParameter_sourceNode" flags="nn" index="Cj7Ep" />
      <concept id="1177498013932" name="jetbrains.mps.lang.actions.structure.SimpleSideTransformMenuPart" flags="ng" index="Cmt7Y">
        <child id="1177498166690" name="matchingText" index="Cn2iK" />
        <child id="1177498182537" name="descriptionText" index="Cn6ar" />
        <child id="1177498207384" name="handler" index="Cncma" />
      </concept>
      <concept id="1177498227294" name="jetbrains.mps.lang.actions.structure.QueryFunction_SideTransform_Handler" flags="in" index="Cnhdc" />
      <concept id="767145758118872828" name="jetbrains.mps.lang.actions.structure.NF_Node_ReplaceWithNewOperation" flags="nn" index="2DeJnW" />
      <concept id="767145758118872830" name="jetbrains.mps.lang.actions.structure.NF_Link_SetNewChildOperation" flags="nn" index="2DeJnY" />
      <concept id="1177526535706" name="jetbrains.mps.lang.actions.structure.ConceptFunctionParameter_result" flags="nn" index="E3gs8" />
      <concept id="1177526592562" name="jetbrains.mps.lang.actions.structure.QueryFunction_SideTransform_ConceptHandler" flags="in" index="E3ukw" />
      <concept id="1177568407352" name="jetbrains.mps.lang.actions.structure.ConceptFunctionParameter_currentTargetNode" flags="nn" index="GyYSE" />
      <concept id="5480835971642155304" name="jetbrains.mps.lang.actions.structure.NF_Model_CreateNewNodeOperation" flags="nn" index="15TzpJ" />
      <concept id="1158700664498" name="jetbrains.mps.lang.actions.structure.NodeFactories" flags="ng" index="37WguZ">
        <child id="1158700779049" name="nodeFactory" index="37WGs$" />
      </concept>
      <concept id="1158700725281" name="jetbrains.mps.lang.actions.structure.NodeFactory" flags="ig" index="37WvkG">
        <reference id="1158700943156" name="applicableConcept" index="37XkoT" />
        <child id="1158701448518" name="setupFunction" index="37ZfLb" />
      </concept>
      <concept id="1158701162220" name="jetbrains.mps.lang.actions.structure.NodeSetupFunction" flags="in" index="37Y9Zx" />
      <concept id="1154465102724" name="jetbrains.mps.lang.actions.structure.NodeSubstitutePreconditionFunction" flags="in" index="3buRE8" />
      <concept id="1154622616118" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstitutePreconditionFunction" flags="in" index="3kRJcU" />
      <concept id="5584396657084912703" name="jetbrains.mps.lang.actions.structure.NodeSetupFunction_NewNode" flags="nn" index="1r4Lsj" />
      <concept id="5584396657084920413" name="jetbrains.mps.lang.actions.structure.NodeSetupFunction_SampleNode" flags="nn" index="1r4N5L" />
      <concept id="5425882385312046132" name="jetbrains.mps.lang.actions.structure.ConceptFunctionParameter_targetNode" flags="nn" index="1yR$tW" />
      <concept id="1182819125053" name="jetbrains.mps.lang.actions.structure.ConceptRightTransformPart" flags="ng" index="1_wSoI">
        <reference id="1182819234902" name="concept" index="1_xjl5" />
        <child id="1074357240595407594" name="nodeQuery" index="3ppw3t" />
        <child id="1182819210322" name="handler" index="1_xdl1" />
      </concept>
      <concept id="1178537049112" name="jetbrains.mps.lang.actions.structure.QueryFunction_SideTransform_NodeQuery" flags="in" index="1Ai3Oa" />
      <concept id="1112056943463" name="jetbrains.mps.lang.actions.structure.NodeSubstituteActions" flags="ng" index="3FK_9_">
        <child id="1112058057696" name="actionsBuilder" index="3FOPby" />
      </concept>
      <concept id="1112058030570" name="jetbrains.mps.lang.actions.structure.NodeSubstituteActionsBuilder" flags="ig" index="3FOIzC">
        <reference id="1112058088712" name="applicableConcept" index="3FOWKa" />
        <child id="1177324142645" name="part" index="tZc4B" />
        <child id="1154465386371" name="precondition" index="3bvWUf" />
      </concept>
      <concept id="1138079221458" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstituteActionsBuilder" flags="ig" index="3UNGvq">
        <property id="1215605257730" name="side" index="7I3sp" />
        <property id="1140829165817" name="transformTag" index="2uHTBK" />
        <reference id="1138079221462" name="applicableConcept" index="3UNGvu" />
        <child id="1177442283389" name="part" index="_1QTJ" />
        <child id="1154622757656" name="precondition" index="3kShCk" />
      </concept>
      <concept id="1138079416598" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstituteActions" flags="ng" index="3UOs0u">
        <child id="1138079416599" name="actionsBuilder" index="3UOs0v" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="5820409030208923287" name="jetbrains.mps.lang.smodel.structure.Node_GetContainingLinkOperation" flags="nn" index="25OxAV" />
      <concept id="1226359078165" name="jetbrains.mps.lang.smodel.structure.LinkRefExpression" flags="nn" index="28GBK8">
        <reference id="1226359078166" name="conceptDeclaration" index="28GBKb" />
        <reference id="1226359192215" name="linkDeclaration" index="28H3Ia" />
      </concept>
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1140725362528" name="jetbrains.mps.lang.smodel.structure.Link_SetTargetOperation" flags="nn" index="2oxUTD">
        <child id="1140725362529" name="linkTarget" index="2oxUTC" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1138661924179" name="jetbrains.mps.lang.smodel.structure.Property_SetOperation" flags="nn" index="tyxLq">
        <child id="1138662048170" name="value" index="tz02z" />
      </concept>
      <concept id="7453996997717780434" name="jetbrains.mps.lang.smodel.structure.Node_GetSConceptOperation" flags="nn" index="2yIwOk" />
      <concept id="1138757581985" name="jetbrains.mps.lang.smodel.structure.Link_SetNewChildOperation" flags="nn" index="zfrQC">
        <reference id="1139880128956" name="concept" index="1A9B2P" />
      </concept>
      <concept id="1143235216708" name="jetbrains.mps.lang.smodel.structure.Model_CreateNewNodeOperation" flags="nn" index="I8ghe">
        <reference id="1143235391024" name="concept" index="I8UWU" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1139621453865" name="jetbrains.mps.lang.smodel.structure.Node_IsInstanceOfOperation" flags="nn" index="1mIQ4w">
        <child id="1177027386292" name="conceptArgument" index="cj9EA" />
      </concept>
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1139867745658" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithNewOperation" flags="nn" index="1_qnLN">
        <reference id="1139867957129" name="concept" index="1_rbq0" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <reference id="1140138128738" name="concept" index="1PxNhF" />
        <child id="1140138123956" name="leftExpression" index="1PxMeX" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1228341669568" name="jetbrains.mps.lang.smodel.structure.Node_DetachOperation" flags="nn" index="3YRAZt" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435808" name="initValue" index="HW$Y0" />
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1227026082377" name="jetbrains.mps.baseLanguage.collections.structure.RemoveFirstElementOperation" flags="nn" index="2Kt2Hk" />
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1165525191778" name="jetbrains.mps.baseLanguage.collections.structure.GetFirstOperation" flags="nn" index="1uHKPH" />
      <concept id="1176501494711" name="jetbrains.mps.baseLanguage.collections.structure.IsNotEmptyOperation" flags="nn" index="3GX2aA" />
    </language>
  </registry>
  <node concept="3FK_9_" id="5l83jlMgju_">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="makeRealLiteral" />
    <node concept="3FOIzC" id="5l83jlMgjuA" role="3FOPby">
      <ref role="3FOWKa" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      <node concept="tYCnQ" id="m1E9k9c5JV" role="tZc4B">
        <ref role="uz4UX" to="pfd6:m1E9k98BVA" resolve="IntegerLiteral" />
        <node concept="ucClh" id="m1E9k9c5X3" role="uz6Si">
          <node concept="ucgPf" id="m1E9k9c5X4" role="ucMEw">
            <node concept="3clFbS" id="m1E9k9c5X5" role="2VODD2">
              <node concept="3cpWs8" id="m1E9k9c8Dt" role="3cqZAp">
                <node concept="3cpWsn" id="m1E9k9c8Dw" role="3cpWs9">
                  <property role="TrG5h" value="literal" />
                  <node concept="3Tqbb2" id="m1E9k9c8Ds" role="1tU5fm">
                    <ref role="ehGHo" to="pfd6:m1E9k98BVA" resolve="IntegerLiteral" />
                  </node>
                  <node concept="2OqwBi" id="m1E9k9c8Lr" role="33vP2m">
                    <node concept="1Q6Npb" id="m1E9k9c8JH" role="2Oq$k0" />
                    <node concept="15TzpJ" id="m1E9k9c8UI" role="2OqNvi">
                      <ref role="I8UWU" to="pfd6:m1E9k98BVA" resolve="IntegerLiteral" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="m1E9k9c9eB" role="3cqZAp">
                <node concept="2OqwBi" id="m1E9k9cXMM" role="3clFbG">
                  <node concept="2OqwBi" id="m1E9k9c9hr" role="2Oq$k0">
                    <node concept="37vLTw" id="m1E9k9c9e_" role="2Oq$k0">
                      <ref role="3cqZAo" node="m1E9k9c8Dw" resolve="literal" />
                    </node>
                    <node concept="3TrcHB" id="m1E9k9c9DB" role="2OqNvi">
                      <ref role="3TsBF5" to="pfd6:m1E9k98YZm" resolve="value" />
                    </node>
                  </node>
                  <node concept="tyxLq" id="m1E9k9cYwP" role="2OqNvi">
                    <node concept="2YIFZM" id="m1E9k9cc51" role="tz02z">
                      <ref role="37wK5l" to="e2lb:~Integer.parseInt(java.lang.String):int" resolve="parseInt" />
                      <ref role="1Pybhc" to="e2lb:~Integer" resolve="Integer" />
                      <node concept="ub8z3" id="m1E9k9cccm" role="37wK5m" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="m1E9k9ccFa" role="3cqZAp">
                <node concept="37vLTw" id="m1E9k9ccFA" role="3cqZAk">
                  <ref role="3cqZAo" node="m1E9k9c8Dw" resolve="literal" />
                </node>
              </node>
            </node>
          </node>
          <node concept="uaGSO" id="m1E9k9c6fE" role="ucKa5">
            <node concept="3clFbS" id="m1E9k9c6fF" role="2VODD2">
              <node concept="3clFbF" id="m1E9k9c6lD" role="3cqZAp">
                <node concept="2OqwBi" id="m1E9k9c6yz" role="3clFbG">
                  <node concept="ub8z3" id="m1E9k9c6lC" role="2Oq$k0" />
                  <node concept="liA8E" id="m1E9k9c6ZU" role="2OqNvi">
                    <ref role="37wK5l" to="e2lb:~String.matches(java.lang.String):boolean" resolve="matches" />
                    <node concept="Xl_RD" id="m1E9k9c75x" role="37wK5m">
                      <property role="Xl_RC" value="(-?)\\d+" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="uGdhv" id="m1E9k9c89f" role="uGu3D">
            <node concept="3clFbS" id="m1E9k9c89h" role="2VODD2">
              <node concept="3clFbF" id="m1E9k9c8kF" role="3cqZAp">
                <node concept="ub8z3" id="m1E9k9c8kE" role="3clFbG" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3buRE8" id="5l83jlMgjA_" role="3bvWUf">
        <node concept="3clFbS" id="5l83jlMgjAA" role="2VODD2">
          <node concept="3clFbF" id="5l83jlMgjFx" role="3cqZAp">
            <node concept="3fqX7Q" id="5l83jlMguSN" role="3clFbG">
              <node concept="2OqwBi" id="5l83jlMguSP" role="3fr31v">
                <node concept="GyYSE" id="5l83jlMguSQ" role="2Oq$k0" />
                <node concept="1mIQ4w" id="5l83jlMguSR" role="2OqNvi">
                  <node concept="chp4Y" id="5l83jlMguSS" role="cj9EA">
                    <ref role="cht4Q" to="pfd6:5l83jlMfP2B" resolve="Literal" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="tYCnQ" id="5l83jlMgkkz" role="tZc4B">
        <ref role="uz4UX" to="pfd6:5l83jlMfYoB" resolve="DecimalLiteral" />
        <node concept="ucClh" id="5l83jlMgkw$" role="uz6Si">
          <node concept="ucgPf" id="5l83jlMgkw_" role="ucMEw">
            <node concept="3clFbS" id="5l83jlMgkwA" role="2VODD2">
              <node concept="3cpWs8" id="5l83jlMgn9V" role="3cqZAp">
                <node concept="3cpWsn" id="5l83jlMgn9Y" role="3cpWs9">
                  <property role="TrG5h" value="literal" />
                  <node concept="3Tqbb2" id="5l83jlMgn9U" role="1tU5fm">
                    <ref role="ehGHo" to="pfd6:5l83jlMfYoB" resolve="DecimalLiteral" />
                  </node>
                  <node concept="2OqwBi" id="5l83jlMgnpi" role="33vP2m">
                    <node concept="1Q6Npb" id="5l83jlMgnn$" role="2Oq$k0" />
                    <node concept="15TzpJ" id="5l83jlMgnxH" role="2OqNvi">
                      <ref role="I8UWU" to="pfd6:5l83jlMfYoB" resolve="DecimalLiteral" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="5l83jlMgn_W" role="3cqZAp">
                <node concept="2OqwBi" id="m1E9k9cZeH" role="3clFbG">
                  <node concept="2OqwBi" id="5l83jlMgnCK" role="2Oq$k0">
                    <node concept="37vLTw" id="5l83jlMgn_U" role="2Oq$k0">
                      <ref role="3cqZAo" node="5l83jlMgn9Y" resolve="literal" />
                    </node>
                    <node concept="3TrcHB" id="5l83jlMgots" role="2OqNvi">
                      <ref role="3TsBF5" to="pfd6:5l83jlMfYoC" resolve="value" />
                    </node>
                  </node>
                  <node concept="tyxLq" id="m1E9k9cZEN" role="2OqNvi">
                    <node concept="ub8z3" id="m1E9k9cZHd" role="tz02z" />
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="5l83jlMgpek" role="3cqZAp">
                <node concept="37vLTw" id="5l83jlMgpei" role="3clFbG">
                  <ref role="3cqZAo" node="5l83jlMgn9Y" resolve="literal" />
                </node>
              </node>
            </node>
          </node>
          <node concept="uaGSO" id="5l83jlMgkyh" role="ucKa5">
            <node concept="3clFbS" id="5l83jlMgkyi" role="2VODD2">
              <node concept="3clFbF" id="5l83jlMgCh6" role="3cqZAp">
                <node concept="2OqwBi" id="5l83jlMgC_p" role="3clFbG">
                  <node concept="ub8z3" id="5l83jlMgCox" role="2Oq$k0" />
                  <node concept="liA8E" id="5l83jlMgGLI" role="2OqNvi">
                    <ref role="37wK5l" to="e2lb:~String.matches(java.lang.String):boolean" resolve="matches" />
                    <node concept="Xl_RD" id="5l83jlMgGRq" role="37wK5m">
                      <property role="Xl_RC" value="(-?)(\\d*.\\d)" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="uGdhv" id="5l83jlMgmTi" role="uGu3D">
            <node concept="3clFbS" id="5l83jlMgmTk" role="2VODD2">
              <node concept="3clFbF" id="5l83jlMgn4I" role="3cqZAp">
                <node concept="ub8z3" id="5l83jlMgn4H" role="3clFbG" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="1AGkceJmEvk">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="makeBinaryExpression" />
    <node concept="3UNGvq" id="1AGkceJofC1" role="3UOs0v">
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      <node concept="3kRJcU" id="1AGkceJofHP" role="3kShCk">
        <node concept="3clFbS" id="1AGkceJofHQ" role="2VODD2">
          <node concept="3clFbF" id="1AGkceJofMO" role="3cqZAp">
            <node concept="3fqX7Q" id="1AGkceJofMM" role="3clFbG">
              <node concept="2OqwBi" id="1AGkceJofYU" role="3fr31v">
                <node concept="Cj7Ep" id="1AGkceJofS7" role="2Oq$k0" />
                <node concept="1mIQ4w" id="1AGkceJog95" role="2OqNvi">
                  <node concept="chp4Y" id="1AGkceJogeT" role="cj9EA">
                    <ref role="cht4Q" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1_wSoI" id="1AGkceJoglQ" role="_1QTJ">
        <ref role="1_xjl5" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        <node concept="E3ukw" id="1AGkceJoglR" role="1_xdl1">
          <node concept="3clFbS" id="1AGkceJoglS" role="2VODD2">
            <node concept="3cpWs6" id="1AGkceJog$l" role="3cqZAp">
              <node concept="2YIFZM" id="1AGkceJogBS" role="3cqZAk">
                <ref role="37wK5l" node="1AGkceJmQq2" resolve="processRightTransform" />
                <ref role="1Pybhc" node="1AGkceJmIYA" resolve="PrecedenceUtil" />
                <node concept="Cj7Ep" id="1AGkceJogDH" role="37wK5m" />
                <node concept="E3gs8" id="1AGkceJogGa" role="37wK5m" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3UNGvq" id="1AGkceJoh5Y" role="3UOs0v">
      <property role="7I3sp" value="left" />
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      <node concept="3kRJcU" id="1AGkceJohp1" role="3kShCk">
        <node concept="3clFbS" id="1AGkceJohp2" role="2VODD2">
          <node concept="3clFbF" id="1AGkceJohu0" role="3cqZAp">
            <node concept="3fqX7Q" id="1AGkceJohtY" role="3clFbG">
              <node concept="2OqwBi" id="1AGkceJoh$X" role="3fr31v">
                <node concept="Cj7Ep" id="1AGkceJohwR" role="2Oq$k0" />
                <node concept="1mIQ4w" id="1AGkceJohL_" role="2OqNvi">
                  <node concept="chp4Y" id="1AGkceJohRp" role="cj9EA">
                    <ref role="cht4Q" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="1_wSoI" id="1AGkceJohYq" role="_1QTJ">
        <ref role="1_xjl5" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        <node concept="E3ukw" id="1AGkceJohYr" role="1_xdl1">
          <node concept="3clFbS" id="1AGkceJohYs" role="2VODD2">
            <node concept="3cpWs6" id="1AGkceJoicX" role="3cqZAp">
              <node concept="2YIFZM" id="1AGkceJoih1" role="3cqZAk">
                <ref role="37wK5l" node="1AGkceJmOxS" resolve="processLeftTransform" />
                <ref role="1Pybhc" node="1AGkceJmIYA" resolve="PrecedenceUtil" />
                <node concept="Cj7Ep" id="1AGkceJoile" role="37wK5m" />
                <node concept="E3gs8" id="1AGkceJoinF" role="37wK5m" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3UNGvq" id="1AGkceJmEvl" role="3UOs0v">
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
      <node concept="1_wSoI" id="1AGkceJnTtJ" role="_1QTJ">
        <ref role="1_xjl5" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        <node concept="E3ukw" id="1AGkceJnTtL" role="1_xdl1">
          <node concept="3clFbS" id="1AGkceJnTtN" role="2VODD2">
            <node concept="3cpWs6" id="1AGkceJnTZU" role="3cqZAp">
              <node concept="2YIFZM" id="1AGkceJnU2Q" role="3cqZAk">
                <ref role="37wK5l" node="1AGkceJmOxS" resolve="processLeftTransform" />
                <ref role="1Pybhc" node="1AGkceJmIYA" resolve="PrecedenceUtil" />
                <node concept="1PxgMI" id="1AGkceJnU7b" role="37wK5m">
                  <ref role="1PxNhF" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
                  <node concept="1yR$tW" id="1AGkceJnU4p" role="1PxMeX" />
                </node>
                <node concept="E3gs8" id="1AGkceJnUcC" role="37wK5m" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1Ai3Oa" id="1AGkceJnTvz" role="3ppw3t">
          <node concept="3clFbS" id="1AGkceJnTv$" role="2VODD2">
            <node concept="3clFbF" id="1AGkceJnTxi" role="3cqZAp">
              <node concept="2OqwBi" id="1AGkceJnTMZ" role="3clFbG">
                <node concept="Cj7Ep" id="1AGkceJnTxh" role="2Oq$k0" />
                <node concept="3TrEf2" id="1AGkceJnTYh" role="2OqNvi">
                  <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3kRJcU" id="1AGkceJpDkQ" role="3kShCk">
        <node concept="3clFbS" id="1AGkceJpDkR" role="2VODD2">
          <node concept="3clFbF" id="1AGkceJpDsZ" role="3cqZAp">
            <node concept="2OqwBi" id="1AGkceJpDxR" role="3clFbG">
              <node concept="Cj7Ep" id="1AGkceJpDsY" role="2Oq$k0" />
              <node concept="1mIQ4w" id="1AGkceJpDWi" role="2OqNvi">
                <node concept="chp4Y" id="1AGkceJpE2a" role="cj9EA">
                  <ref role="cht4Q" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3UNGvq" id="1AGkceJnUkD" role="3UOs0v">
      <property role="7I3sp" value="left" />
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
      <node concept="1_wSoI" id="1AGkceJnUpb" role="_1QTJ">
        <ref role="1_xjl5" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        <node concept="E3ukw" id="1AGkceJnUpc" role="1_xdl1">
          <node concept="3clFbS" id="1AGkceJnUpd" role="2VODD2">
            <node concept="3cpWs6" id="1AGkceJnUEp" role="3cqZAp">
              <node concept="2YIFZM" id="1AGkceJnUGO" role="3cqZAk">
                <ref role="37wK5l" node="1AGkceJmQq2" resolve="processRightTransform" />
                <ref role="1Pybhc" node="1AGkceJmIYA" resolve="PrecedenceUtil" />
                <node concept="1PxgMI" id="1AGkceJnUKl" role="37wK5m">
                  <ref role="1PxNhF" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
                  <node concept="1yR$tW" id="1AGkceJnUIb" role="1PxMeX" />
                </node>
                <node concept="E3gs8" id="1AGkceJnUP8" role="37wK5m" />
              </node>
            </node>
          </node>
        </node>
        <node concept="1Ai3Oa" id="1AGkceJnUpT" role="3ppw3t">
          <node concept="3clFbS" id="1AGkceJnUpU" role="2VODD2">
            <node concept="3clFbF" id="1AGkceJnUqM" role="3cqZAp">
              <node concept="2OqwBi" id="1AGkceJnUtc" role="3clFbG">
                <node concept="Cj7Ep" id="1AGkceJnUqL" role="2Oq$k0" />
                <node concept="3TrEf2" id="1AGkceJnUCK" role="2OqNvi">
                  <ref role="3Tt5mk" to="pfd6:5l83jlMf$RD" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3kRJcU" id="1AGkceJpEgw" role="3kShCk">
        <node concept="3clFbS" id="1AGkceJpEgx" role="2VODD2">
          <node concept="3clFbF" id="1AGkceJpEl8" role="3cqZAp">
            <node concept="2OqwBi" id="1AGkceJpEla" role="3clFbG">
              <node concept="Cj7Ep" id="1AGkceJpElb" role="2Oq$k0" />
              <node concept="1mIQ4w" id="1AGkceJpElc" role="2OqNvi">
                <node concept="chp4Y" id="1AGkceJpEld" role="cj9EA">
                  <ref role="cht4Q" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="1AGkceJmIYA">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="PrecedenceUtil" />
    <node concept="2tJIrI" id="4_KIr3pY4Uu" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJnmYu" role="jymVt">
      <property role="TrG5h" value="isLowerPriority" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1AGkceJnmYx" role="3clF47">
        <node concept="3cpWs6" id="1AGkceJnnbQ" role="3cqZAp">
          <node concept="3eOVzh" id="1AGkceJnpkw" role="3cqZAk">
            <node concept="2OqwBi" id="1AGkceJnqCU" role="3uHU7w">
              <node concept="2OqwBi" id="1AGkceJnpZy" role="2Oq$k0">
                <node concept="2OqwBi" id="1AGkceJnpww" role="2Oq$k0">
                  <node concept="37vLTw" id="55TOEi0pgXb" role="2Oq$k0">
                    <ref role="3cqZAo" node="1AGkceJnnaW" resolve="second" />
                  </node>
                  <node concept="2yIwOk" id="1AGkceJnpJK" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="1AGkceJnqlI" role="2OqNvi">
                  <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                </node>
              </node>
              <node concept="liA8E" id="1AGkceJnrzG" role="2OqNvi">
                <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
              </node>
            </node>
            <node concept="2OqwBi" id="1AGkceJnnPA" role="3uHU7B">
              <node concept="2OqwBi" id="1AGkceJnnnk" role="2Oq$k0">
                <node concept="2OqwBi" id="1AGkceJnne_" role="2Oq$k0">
                  <node concept="37vLTw" id="55TOEi0pgVV" role="2Oq$k0">
                    <ref role="3cqZAo" node="1AGkceJnnaE" resolve="first" />
                  </node>
                  <node concept="2yIwOk" id="1AGkceJnnlJ" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="1AGkceJnnGh" role="2OqNvi">
                  <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                </node>
              </node>
              <node concept="liA8E" id="1AGkceJnoJA" role="2OqNvi">
                <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1AGkceJnxar" role="1B3o_S" />
      <node concept="10P_77" id="1AGkceJnmYp" role="3clF45" />
      <node concept="37vLTG" id="1AGkceJnnaE" role="3clF46">
        <property role="TrG5h" value="first" />
        <node concept="3Tqbb2" id="1AGkceJnnaD" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
      </node>
      <node concept="37vLTG" id="1AGkceJnnaW" role="3clF46">
        <property role="TrG5h" value="second" />
        <node concept="3Tqbb2" id="1AGkceJnnba" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1AGkceJnsty" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJnsdI" role="jymVt">
      <property role="TrG5h" value="isLowerPriority" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1AGkceJnsdJ" role="3clF47">
        <node concept="3cpWs6" id="1AGkceJnsdK" role="3cqZAp">
          <node concept="3eOVzh" id="1AGkceJnsdL" role="3cqZAk">
            <node concept="37vLTw" id="1AGkceJnsJ4" role="3uHU7w">
              <ref role="3cqZAo" node="1AGkceJnse4" resolve="level" />
            </node>
            <node concept="2OqwBi" id="1AGkceJnsdT" role="3uHU7B">
              <node concept="2OqwBi" id="1AGkceJnsdU" role="2Oq$k0">
                <node concept="2OqwBi" id="1AGkceJnsdV" role="2Oq$k0">
                  <node concept="37vLTw" id="1AGkceJnsdW" role="2Oq$k0">
                    <ref role="3cqZAo" node="1AGkceJnse2" resolve="first" />
                  </node>
                  <node concept="2yIwOk" id="1AGkceJnsdX" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="1AGkceJnsdY" role="2OqNvi">
                  <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                </node>
              </node>
              <node concept="liA8E" id="1AGkceJnsdZ" role="2OqNvi">
                <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1AGkceJnxfg" role="1B3o_S" />
      <node concept="10P_77" id="1AGkceJnse1" role="3clF45" />
      <node concept="37vLTG" id="1AGkceJnse2" role="3clF46">
        <property role="TrG5h" value="first" />
        <node concept="3Tqbb2" id="1AGkceJnse3" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
      </node>
      <node concept="37vLTG" id="1AGkceJnse4" role="3clF46">
        <property role="TrG5h" value="level" />
        <node concept="10Oyi0" id="1AGkceJnsG4" role="1tU5fm" />
      </node>
    </node>
    <node concept="2tJIrI" id="1AGkceJnYg5" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJnYWw" role="jymVt">
      <property role="TrG5h" value="isHigherPriority" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1AGkceJnYWx" role="3clF47">
        <node concept="3cpWs6" id="1AGkceJnYWy" role="3cqZAp">
          <node concept="3eOSWO" id="55TOEi0pgYu" role="3cqZAk">
            <node concept="2OqwBi" id="1AGkceJnYWF" role="3uHU7B">
              <node concept="2OqwBi" id="1AGkceJnYWG" role="2Oq$k0">
                <node concept="2OqwBi" id="1AGkceJnYWH" role="2Oq$k0">
                  <node concept="37vLTw" id="1AGkceJnYWI" role="2Oq$k0">
                    <ref role="3cqZAo" node="1AGkceJnYWO" resolve="first" />
                  </node>
                  <node concept="2yIwOk" id="1AGkceJnYWJ" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="1AGkceJnYWK" role="2OqNvi">
                  <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                </node>
              </node>
              <node concept="liA8E" id="1AGkceJnYWL" role="2OqNvi">
                <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
              </node>
            </node>
            <node concept="2OqwBi" id="1AGkceJnYW$" role="3uHU7w">
              <node concept="2OqwBi" id="1AGkceJnYW_" role="2Oq$k0">
                <node concept="2OqwBi" id="1AGkceJnYWA" role="2Oq$k0">
                  <node concept="37vLTw" id="55TOEi0pgYr" role="2Oq$k0">
                    <ref role="3cqZAo" node="1AGkceJnYWQ" resolve="second" />
                  </node>
                  <node concept="2yIwOk" id="1AGkceJnYWC" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="1AGkceJnYWD" role="2OqNvi">
                  <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                </node>
              </node>
              <node concept="liA8E" id="1AGkceJnYWE" role="2OqNvi">
                <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1AGkceJnYWM" role="1B3o_S" />
      <node concept="10P_77" id="1AGkceJnYWN" role="3clF45" />
      <node concept="37vLTG" id="1AGkceJnYWO" role="3clF46">
        <property role="TrG5h" value="first" />
        <node concept="3Tqbb2" id="1AGkceJnYWP" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
      </node>
      <node concept="37vLTG" id="1AGkceJnYWQ" role="3clF46">
        <property role="TrG5h" value="second" />
        <node concept="3Tqbb2" id="1AGkceJnYWR" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1AGkceJnYAV" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJnxRy" role="jymVt">
      <property role="TrG5h" value="isSamePriority" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="37vLTG" id="1AGkceJny9P" role="3clF46">
        <property role="TrG5h" value="first" />
        <node concept="3Tqbb2" id="1AGkceJny9Q" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
      </node>
      <node concept="37vLTG" id="1AGkceJny9R" role="3clF46">
        <property role="TrG5h" value="second" />
        <node concept="3Tqbb2" id="1AGkceJny9S" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
      </node>
      <node concept="3clFbS" id="1AGkceJnxR_" role="3clF47">
        <node concept="3cpWs6" id="1AGkceJnybd" role="3cqZAp">
          <node concept="3clFbC" id="1AGkceJnyfO" role="3cqZAk">
            <node concept="2OqwBi" id="1AGkceJnybm" role="3uHU7B">
              <node concept="2OqwBi" id="1AGkceJnybn" role="2Oq$k0">
                <node concept="2OqwBi" id="1AGkceJnybo" role="2Oq$k0">
                  <node concept="37vLTw" id="1AGkceJnybp" role="2Oq$k0">
                    <ref role="3cqZAo" node="1AGkceJny9P" resolve="first" />
                  </node>
                  <node concept="2yIwOk" id="1AGkceJnybq" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="1AGkceJnybr" role="2OqNvi">
                  <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                </node>
              </node>
              <node concept="liA8E" id="1AGkceJnybs" role="2OqNvi">
                <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
              </node>
            </node>
            <node concept="2OqwBi" id="1AGkceJnybf" role="3uHU7w">
              <node concept="2OqwBi" id="1AGkceJnybg" role="2Oq$k0">
                <node concept="2OqwBi" id="1AGkceJnybh" role="2Oq$k0">
                  <node concept="37vLTw" id="55TOEi0ph0o" role="2Oq$k0">
                    <ref role="3cqZAo" node="1AGkceJny9R" resolve="second" />
                  </node>
                  <node concept="2yIwOk" id="1AGkceJnybj" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="1AGkceJnybk" role="2OqNvi">
                  <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                </node>
              </node>
              <node concept="liA8E" id="1AGkceJnybl" role="2OqNvi">
                <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1AGkceJnx_p" role="1B3o_S" />
      <node concept="10P_77" id="1AGkceJnxRn" role="3clF45" />
    </node>
    <node concept="2tJIrI" id="1AGkceJnwSz" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJmOFr" role="jymVt">
      <property role="TrG5h" value="getTargetForLeftTransform" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="37vLTG" id="1AGkceJmOGx" role="3clF46">
        <property role="TrG5h" value="contextNode" />
        <node concept="3Tqbb2" id="1AGkceJmOGy" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
        <node concept="2AHcQZ" id="1AGkceJmT4O" role="2AJF6D">
          <ref role="2AI5Lk" to="as9o:~NotNull" resolve="NotNull" />
        </node>
      </node>
      <node concept="37vLTG" id="1AGkceJmOGz" role="3clF46">
        <property role="TrG5h" value="resultNode" />
        <node concept="3Tqbb2" id="1AGkceJmOG$" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        </node>
        <node concept="2AHcQZ" id="1AGkceJmT7l" role="2AJF6D">
          <ref role="2AI5Lk" to="as9o:~NotNull" resolve="NotNull" />
        </node>
      </node>
      <node concept="3clFbS" id="1AGkceJmOFu" role="3clF47">
        <node concept="3cpWs8" id="1AGkceJmTaj" role="3cqZAp">
          <node concept="3cpWsn" id="1AGkceJmTam" role="3cpWs9">
            <property role="TrG5h" value="resultingPriority" />
            <node concept="10Oyi0" id="1AGkceJmTai" role="1tU5fm" />
            <node concept="2OqwBi" id="1AGkceJmU8u" role="33vP2m">
              <node concept="2OqwBi" id="1AGkceJmTKx" role="2Oq$k0">
                <node concept="2OqwBi" id="1AGkceJmTeV" role="2Oq$k0">
                  <node concept="37vLTw" id="1AGkceJmTc5" role="2Oq$k0">
                    <ref role="3cqZAo" node="1AGkceJmOGz" resolve="resultNode" />
                  </node>
                  <node concept="2yIwOk" id="1AGkceJmTzT" role="2OqNvi" />
                </node>
                <node concept="2qgKlT" id="1AGkceJmTYh" role="2OqNvi">
                  <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                </node>
              </node>
              <node concept="liA8E" id="1AGkceJmV2v" role="2OqNvi">
                <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="1AGkceJmVcP" role="3cqZAp">
          <node concept="3cpWsn" id="1AGkceJmVcS" role="3cpWs9">
            <property role="TrG5h" value="targetNode" />
            <node concept="3Tqbb2" id="1AGkceJmVcN" role="1tU5fm">
              <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
            </node>
            <node concept="37vLTw" id="1AGkceJmVvQ" role="33vP2m">
              <ref role="3cqZAo" node="1AGkceJmOGx" resolve="contextNode" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1AGkceJmV$E" role="3cqZAp" />
        <node concept="1Dw8fO" id="1AGkceJmVCn" role="3cqZAp">
          <node concept="3clFbS" id="1AGkceJmVCp" role="2LFqv$">
            <node concept="3SKdUt" id="1AGkceJnvb0" role="3cqZAp">
              <node concept="3SKdUq" id="1AGkceJnvdR" role="3SKWNk">
                <property role="3SKdUp" value="if the parent expression is parenthesized we terminate the search upwards" />
              </node>
            </node>
            <node concept="3clFbJ" id="1AGkceJntxD" role="3cqZAp">
              <node concept="3clFbS" id="1AGkceJntxF" role="3clFbx">
                <node concept="3zACq4" id="1AGkceJntFC" role="3cqZAp" />
              </node>
              <node concept="2OqwBi" id="1AGkceJnt$9" role="3clFbw">
                <node concept="37vLTw" id="1AGkceJntyz" role="2Oq$k0">
                  <ref role="3cqZAo" node="1AGkceJmVCq" resolve="parentNode" />
                </node>
                <node concept="1mIQ4w" id="1AGkceJntDY" role="2OqNvi">
                  <node concept="chp4Y" id="1AGkceJntEl" role="cj9EA">
                    <ref role="cht4Q" to="pfd6:5l83jlMgWog" resolve="ParenthesizedExpression" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3SKdUt" id="1AGkceJnv0e" role="3cqZAp">
              <node concept="3SKdUq" id="1AGkceJnv31" role="3SKWNk">
                <property role="3SKdUp" value="if the parent expression is a BinaryExpression with a higher priority and the target node is the right child then transform current target and add additional parenthesis" />
              </node>
            </node>
            <node concept="3clFbJ" id="1AGkceJntHS" role="3cqZAp">
              <node concept="3clFbS" id="1AGkceJntHU" role="3clFbx">
                <node concept="3zACq4" id="1AGkceJnuJ$" role="3cqZAp" />
              </node>
              <node concept="1Wc70l" id="1AGkceJnu5o" role="3clFbw">
                <node concept="3clFbC" id="1AGkceJnuym" role="3uHU7w">
                  <node concept="28GBK8" id="1AGkceJnu$M" role="3uHU7w">
                    <ref role="28GBKb" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                    <ref role="28H3Ia" to="pfd6:5l83jlMf$RF" />
                  </node>
                  <node concept="2OqwBi" id="1AGkceJnueR" role="3uHU7B">
                    <node concept="37vLTw" id="1AGkceJnuc2" role="2Oq$k0">
                      <ref role="3cqZAo" node="1AGkceJmVcS" resolve="targetNode" />
                    </node>
                    <node concept="25OxAV" id="1AGkceJnunh" role="2OqNvi" />
                  </node>
                </node>
                <node concept="2OqwBi" id="1AGkceJntLu" role="3uHU7B">
                  <node concept="37vLTw" id="1AGkceJntJS" role="2Oq$k0">
                    <ref role="3cqZAo" node="1AGkceJmVCq" resolve="parentNode" />
                  </node>
                  <node concept="1mIQ4w" id="1AGkceJntWQ" role="2OqNvi">
                    <node concept="chp4Y" id="1AGkceJntXr" role="cj9EA">
                      <ref role="cht4Q" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="1AGkceJnvh8" role="3cqZAp">
              <node concept="37vLTI" id="1AGkceJnvld" role="3clFbG">
                <node concept="1PxgMI" id="1AGkceJnvoN" role="37vLTx">
                  <ref role="1PxNhF" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
                  <node concept="37vLTw" id="1AGkceJnvmW" role="1PxMeX">
                    <ref role="3cqZAo" node="1AGkceJmVCq" resolve="parentNode" />
                  </node>
                </node>
                <node concept="37vLTw" id="1AGkceJnvh6" role="37vLTJ">
                  <ref role="3cqZAo" node="1AGkceJmVcS" resolve="targetNode" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3cpWsn" id="1AGkceJmVCq" role="1Duv9x">
            <property role="TrG5h" value="parentNode" />
            <node concept="3Tqbb2" id="1AGkceJmVES" role="1tU5fm" />
            <node concept="2OqwBi" id="1AGkceJmVHX" role="33vP2m">
              <node concept="37vLTw" id="1AGkceJmVGf" role="2Oq$k0">
                <ref role="3cqZAo" node="1AGkceJmVcS" resolve="targetNode" />
              </node>
              <node concept="1mfA1w" id="1AGkceJmVPG" role="2OqNvi" />
            </node>
          </node>
          <node concept="1Wc70l" id="1AGkceJngZn" role="1Dwp0S">
            <node concept="1Wc70l" id="1AGkceJmVW$" role="3uHU7B">
              <node concept="3y3z36" id="1AGkceJmVTw" role="3uHU7B">
                <node concept="37vLTw" id="1AGkceJmVQz" role="3uHU7B">
                  <ref role="3cqZAo" node="1AGkceJmVCq" resolve="parentNode" />
                </node>
                <node concept="10Nm6u" id="1AGkceJmVUU" role="3uHU7w" />
              </node>
              <node concept="2OqwBi" id="1AGkceJmW0X" role="3uHU7w">
                <node concept="37vLTw" id="1AGkceJmVZf" role="2Oq$k0">
                  <ref role="3cqZAo" node="1AGkceJmVCq" resolve="parentNode" />
                </node>
                <node concept="1mIQ4w" id="1AGkceJmW7b" role="2OqNvi">
                  <node concept="chp4Y" id="1AGkceJmW83" role="cj9EA">
                    <ref role="cht4Q" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="1rXfSq" id="1AGkceJnrHB" role="3uHU7w">
              <ref role="37wK5l" node="1AGkceJnsdI" resolve="isLowerPriority" />
              <node concept="1PxgMI" id="1AGkceJnrVx" role="37wK5m">
                <ref role="1PxNhF" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
                <node concept="37vLTw" id="1AGkceJnrKV" role="1PxMeX">
                  <ref role="3cqZAo" node="1AGkceJmVCq" resolve="parentNode" />
                </node>
              </node>
              <node concept="37vLTw" id="1AGkceJnsQ5" role="37wK5m">
                <ref role="3cqZAo" node="1AGkceJmTam" resolve="resultingPriority" />
              </node>
            </node>
          </node>
          <node concept="37vLTI" id="1AGkceJntj2" role="1Dwrff">
            <node concept="2OqwBi" id="1AGkceJntpf" role="37vLTx">
              <node concept="37vLTw" id="1AGkceJntni" role="2Oq$k0">
                <ref role="3cqZAo" node="1AGkceJmVcS" resolve="targetNode" />
              </node>
              <node concept="1mfA1w" id="1AGkceJntwD" role="2OqNvi" />
            </node>
            <node concept="37vLTw" id="1AGkceJnten" role="37vLTJ">
              <ref role="3cqZAo" node="1AGkceJmVCq" resolve="parentNode" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1AGkceJmXG5" role="3cqZAp">
          <node concept="37vLTw" id="1AGkceJnvqL" role="3cqZAk">
            <ref role="3cqZAo" node="1AGkceJmVcS" resolve="targetNode" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1AGkceJnVVb" role="1B3o_S" />
      <node concept="3Tqbb2" id="1AGkceJmOFo" role="3clF45">
        <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      </node>
      <node concept="2AHcQZ" id="1AGkceJmREv" role="2AJF6D">
        <ref role="2AI5Lk" to="as9o:~NotNull" resolve="NotNull" />
      </node>
    </node>
    <node concept="2tJIrI" id="1AGkceJnVur" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJnWvH" role="jymVt">
      <property role="TrG5h" value="parenthesizeIfNecessary" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1AGkceJnWvK" role="3clF47">
        <node concept="3clFbJ" id="1AGkceJnWPH" role="3cqZAp">
          <node concept="3clFbS" id="1AGkceJnWPI" role="3clFbx">
            <node concept="3cpWs8" id="1AGkceJnXo1" role="3cqZAp">
              <node concept="3cpWsn" id="1AGkceJnXo4" role="3cpWs9">
                <property role="TrG5h" value="parentNode" />
                <node concept="3Tqbb2" id="1AGkceJnXo0" role="1tU5fm">
                  <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                </node>
                <node concept="1PxgMI" id="1AGkceJnX$T" role="33vP2m">
                  <ref role="1PxNhF" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                  <node concept="2OqwBi" id="1AGkceJnXso" role="1PxMeX">
                    <node concept="37vLTw" id="1AGkceJnXqa" role="2Oq$k0">
                      <ref role="3cqZAo" node="1AGkceJnWOo" resolve="contextNode" />
                    </node>
                    <node concept="1mfA1w" id="1AGkceJnXzN" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="1AGkceJnXCD" role="3cqZAp">
              <node concept="3clFbS" id="1AGkceJnXCF" role="3clFbx">
                <node concept="3cpWs8" id="1AGkceJnZXo" role="3cqZAp">
                  <node concept="3cpWsn" id="1AGkceJnZXr" role="3cpWs9">
                    <property role="TrG5h" value="result" />
                    <node concept="3Tqbb2" id="1AGkceJnZXm" role="1tU5fm">
                      <ref role="ehGHo" to="pfd6:5l83jlMgWog" resolve="ParenthesizedExpression" />
                    </node>
                    <node concept="2OqwBi" id="1AGkceJo038" role="33vP2m">
                      <node concept="37vLTw" id="1AGkceJo00O" role="2Oq$k0">
                        <ref role="3cqZAo" node="1AGkceJnWOo" resolve="contextNode" />
                      </node>
                      <node concept="2DeJnW" id="1AGkceJo0ql" role="2OqNvi">
                        <ref role="1_rbq0" to="pfd6:5l83jlMgWog" resolve="ParenthesizedExpression" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="1AGkceJo0Jg" role="3cqZAp">
                  <node concept="37vLTI" id="1AGkceJo1bC" role="3clFbG">
                    <node concept="37vLTw" id="1AGkceJo1eU" role="37vLTx">
                      <ref role="3cqZAo" node="1AGkceJnWOo" resolve="contextNode" />
                    </node>
                    <node concept="2OqwBi" id="1AGkceJo0Mh" role="37vLTJ">
                      <node concept="37vLTw" id="1AGkceJo0Je" role="2Oq$k0">
                        <ref role="3cqZAo" node="1AGkceJnZXr" resolve="result" />
                      </node>
                      <node concept="3TrEf2" id="1AGkceJo0WR" role="2OqNvi">
                        <ref role="3Tt5mk" to="pfd6:5l83jlMfE3N" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs6" id="1AGkceJo1iL" role="3cqZAp">
                  <node concept="37vLTw" id="1AGkceJo1kD" role="3cqZAk">
                    <ref role="3cqZAo" node="1AGkceJnZXr" resolve="result" />
                  </node>
                </node>
              </node>
              <node concept="1Wc70l" id="1AGkceJnYdJ" role="3clFbw">
                <node concept="1rXfSq" id="1AGkceJnZND" role="3uHU7w">
                  <ref role="37wK5l" node="1AGkceJnYWw" resolve="isHigherPriority" />
                  <node concept="37vLTw" id="1AGkceJnZQj" role="37wK5m">
                    <ref role="3cqZAo" node="1AGkceJnXo4" resolve="parentNode" />
                  </node>
                  <node concept="37vLTw" id="1AGkceJnZUq" role="37wK5m">
                    <ref role="3cqZAo" node="1AGkceJnWOo" resolve="contextNode" />
                  </node>
                </node>
                <node concept="3clFbC" id="1AGkceJnY5G" role="3uHU7B">
                  <node concept="2OqwBi" id="1AGkceJnXHy" role="3uHU7B">
                    <node concept="37vLTw" id="1AGkceJnXFo" role="2Oq$k0">
                      <ref role="3cqZAo" node="1AGkceJnWOo" resolve="contextNode" />
                    </node>
                    <node concept="25OxAV" id="1AGkceJnXVF" role="2OqNvi" />
                  </node>
                  <node concept="28GBK8" id="1AGkceJnY7U" role="3uHU7w">
                    <ref role="28GBKb" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                    <ref role="28H3Ia" to="pfd6:5l83jlMf$RF" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="1AGkceJnX8r" role="3clFbw">
            <node concept="2OqwBi" id="1AGkceJnWTw" role="2Oq$k0">
              <node concept="37vLTw" id="1AGkceJnWRo" role="2Oq$k0">
                <ref role="3cqZAo" node="1AGkceJnWOo" resolve="contextNode" />
              </node>
              <node concept="1mfA1w" id="1AGkceJnX0E" role="2OqNvi" />
            </node>
            <node concept="1mIQ4w" id="1AGkceJnXeA" role="2OqNvi">
              <node concept="chp4Y" id="1AGkceJnXeV" role="cj9EA">
                <ref role="cht4Q" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1AGkceJnXi5" role="3cqZAp">
          <node concept="37vLTw" id="1AGkceJnXla" role="3cqZAk">
            <ref role="3cqZAo" node="1AGkceJnWOo" resolve="contextNode" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1AGkceJnW4u" role="1B3o_S" />
      <node concept="3Tqbb2" id="1AGkceJnWvq" role="3clF45">
        <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      </node>
      <node concept="37vLTG" id="1AGkceJnWOo" role="3clF46">
        <property role="TrG5h" value="contextNode" />
        <node concept="3Tqbb2" id="1AGkceJnWOn" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
        <node concept="2AHcQZ" id="1AGkceJnWOM" role="2AJF6D">
          <ref role="2AI5Lk" to="as9o:~NotNull" resolve="NotNull" />
        </node>
      </node>
      <node concept="2AHcQZ" id="4_KIr3q0v1W" role="2AJF6D">
        <ref role="2AI5Lk" to="as9o:~NotNull" resolve="NotNull" />
      </node>
    </node>
    <node concept="2tJIrI" id="1AGkceJmIZu" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJmOxS" role="jymVt">
      <property role="TrG5h" value="processLeftTransform" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1AGkceJmOxV" role="3clF47">
        <node concept="3SKdUt" id="1AGkceJmO__" role="3cqZAp">
          <node concept="3SKdUq" id="1AGkceJmO_C" role="3SKWNk">
            <property role="3SKdUp" value="get the node to process" />
          </node>
        </node>
        <node concept="3cpWs8" id="1AGkceJmOAk" role="3cqZAp">
          <node concept="3cpWsn" id="1AGkceJmOAn" role="3cpWs9">
            <property role="TrG5h" value="nodeToProcess" />
            <node concept="3Tqbb2" id="1AGkceJmOAi" role="1tU5fm">
              <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
            </node>
            <node concept="2YIFZM" id="1AGkceJmOH4" role="33vP2m">
              <ref role="37wK5l" node="1AGkceJmOFr" resolve="getTargetForLeftTransform" />
              <ref role="1Pybhc" node="1AGkceJmIYA" resolve="PrecedenceUtil" />
              <node concept="37vLTw" id="1AGkceJmOIk" role="37wK5m">
                <ref role="3cqZAo" node="1AGkceJmOyb" resolve="sourceNode" />
              </node>
              <node concept="37vLTw" id="1AGkceJmOJU" role="37wK5m">
                <ref role="3cqZAo" node="1AGkceJmOyr" resolve="result" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3SKdUt" id="1AGkceJmOMM" role="3cqZAp">
          <node concept="3SKdUq" id="1AGkceJmONJ" role="3SKWNk">
            <property role="3SKdUp" value="since binary operations are left-associative we should perform a complex left transform" />
          </node>
        </node>
        <node concept="3SKdUt" id="1AGkceJmOP8" role="3cqZAp">
          <node concept="3SKdUq" id="1AGkceJmOQ9" role="3SKWNk">
            <property role="3SKdUp" value="BinaryExpression is &quot;left&quot; child of another BinaryExpression with the same priority level" />
          </node>
        </node>
        <node concept="3clFbJ" id="1AGkceJnvDW" role="3cqZAp">
          <node concept="3clFbS" id="1AGkceJnvDY" role="3clFbx">
            <node concept="3cpWs8" id="1AGkceJnwCb" role="3cqZAp">
              <node concept="3cpWsn" id="1AGkceJnwCe" role="3cpWs9">
                <property role="TrG5h" value="parentBinaryExpression" />
                <node concept="3Tqbb2" id="1AGkceJnwC9" role="1tU5fm">
                  <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                </node>
                <node concept="1PxgMI" id="1AGkceJnwPE" role="33vP2m">
                  <ref role="1PxNhF" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                  <node concept="2OqwBi" id="1AGkceJnwHn" role="1PxMeX">
                    <node concept="37vLTw" id="1AGkceJnwF6" role="2Oq$k0">
                      <ref role="3cqZAo" node="1AGkceJmOAn" resolve="nodeToProcess" />
                    </node>
                    <node concept="1mfA1w" id="1AGkceJnwO$" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="1AGkceJnypE" role="3cqZAp">
              <node concept="3clFbS" id="1AGkceJnypG" role="3clFbx">
                <node concept="3clFbF" id="1AGkceJnyyi" role="3cqZAp">
                  <node concept="2OqwBi" id="1AGkceJny_t" role="3clFbG">
                    <node concept="37vLTw" id="1AGkceJnyyg" role="2Oq$k0">
                      <ref role="3cqZAo" node="1AGkceJnwCe" resolve="parentBinaryExpression" />
                    </node>
                    <node concept="1P9Npp" id="1AGkceJnyK3" role="2OqNvi">
                      <node concept="37vLTw" id="1AGkceJnyLe" role="1P9ThW">
                        <ref role="3cqZAo" node="1AGkceJmOyr" resolve="result" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="1AGkceJnyNg" role="3cqZAp">
                  <node concept="37vLTI" id="1AGkceJnz1X" role="3clFbG">
                    <node concept="37vLTw" id="1AGkceJnz3u" role="37vLTx">
                      <ref role="3cqZAo" node="1AGkceJmOAn" resolve="nodeToProcess" />
                    </node>
                    <node concept="2OqwBi" id="1AGkceJnyPX" role="37vLTJ">
                      <node concept="37vLTw" id="1AGkceJnyNe" role="2Oq$k0">
                        <ref role="3cqZAo" node="1AGkceJmOyr" resolve="result" />
                      </node>
                      <node concept="3TrEf2" id="1AGkceJnz0L" role="2OqNvi">
                        <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="1AGkceJnz6E" role="3cqZAp">
                  <node concept="37vLTI" id="1AGkceJnzwj" role="3clFbG">
                    <node concept="37vLTw" id="1AGkceJnzzi" role="37vLTx">
                      <ref role="3cqZAo" node="1AGkceJnwCe" resolve="parentBinaryExpression" />
                    </node>
                    <node concept="2OqwBi" id="1AGkceJnz9z" role="37vLTJ">
                      <node concept="37vLTw" id="1AGkceJnz6C" role="2Oq$k0">
                        <ref role="3cqZAo" node="1AGkceJmOyr" resolve="result" />
                      </node>
                      <node concept="3TrEf2" id="1AGkceJnzk9" role="2OqNvi">
                        <ref role="3Tt5mk" to="pfd6:5l83jlMf$RD" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs6" id="1AGkceJnz_t" role="3cqZAp">
                  <node concept="37vLTw" id="1AGkceJnzAW" role="3cqZAk">
                    <ref role="3cqZAo" node="1AGkceJmOyr" resolve="result" />
                  </node>
                </node>
              </node>
              <node concept="1rXfSq" id="1AGkceJnyrB" role="3clFbw">
                <ref role="37wK5l" node="1AGkceJnxRy" resolve="isSamePriority" />
                <node concept="37vLTw" id="1AGkceJnysU" role="37wK5m">
                  <ref role="3cqZAo" node="1AGkceJnwCe" resolve="parentBinaryExpression" />
                </node>
                <node concept="37vLTw" id="1AGkceJnyvT" role="37wK5m">
                  <ref role="3cqZAo" node="1AGkceJmOyr" resolve="result" />
                </node>
              </node>
            </node>
          </node>
          <node concept="1Wc70l" id="1AGkceJnwdH" role="3clFbw">
            <node concept="3clFbC" id="1AGkceJnwvA" role="3uHU7w">
              <node concept="28GBK8" id="1AGkceJnwyB" role="3uHU7w">
                <ref role="28GBKb" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                <ref role="28H3Ia" to="pfd6:5l83jlMf$RF" />
              </node>
              <node concept="2OqwBi" id="1AGkceJnwj5" role="3uHU7B">
                <node concept="37vLTw" id="1AGkceJnwgg" role="2Oq$k0">
                  <ref role="3cqZAo" node="1AGkceJmOAn" resolve="nodeToProcess" />
                </node>
                <node concept="25OxAV" id="1AGkceJnwr5" role="2OqNvi" />
              </node>
            </node>
            <node concept="2OqwBi" id="1AGkceJnvYW" role="3uHU7B">
              <node concept="2OqwBi" id="1AGkceJnvJX" role="2Oq$k0">
                <node concept="37vLTw" id="1AGkceJnvHL" role="2Oq$k0">
                  <ref role="3cqZAo" node="1AGkceJmOAn" resolve="nodeToProcess" />
                </node>
                <node concept="1mfA1w" id="1AGkceJnvR9" role="2OqNvi" />
              </node>
              <node concept="1mIQ4w" id="1AGkceJnw59" role="2OqNvi">
                <node concept="chp4Y" id="1AGkceJnw5w" role="cj9EA">
                  <ref role="cht4Q" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1AGkceJnvAw" role="3cqZAp" />
        <node concept="3clFbF" id="1AGkceJmOSH" role="3cqZAp">
          <node concept="2OqwBi" id="1AGkceJmOVw" role="3clFbG">
            <node concept="37vLTw" id="1AGkceJmOSF" role="2Oq$k0">
              <ref role="3cqZAo" node="1AGkceJmOAn" resolve="nodeToProcess" />
            </node>
            <node concept="1P9Npp" id="1AGkceJmP39" role="2OqNvi">
              <node concept="37vLTw" id="1AGkceJmP44" role="1P9ThW">
                <ref role="3cqZAo" node="1AGkceJmOyr" resolve="result" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1AGkceJmP6n" role="3cqZAp">
          <node concept="2OqwBi" id="1AGkceJmPFm" role="3clFbG">
            <node concept="2OqwBi" id="1AGkceJmP9G" role="2Oq$k0">
              <node concept="37vLTw" id="1AGkceJmP6l" role="2Oq$k0">
                <ref role="3cqZAo" node="1AGkceJmOyr" resolve="result" />
              </node>
              <node concept="3TrEf2" id="1AGkceJmPvH" role="2OqNvi">
                <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
              </node>
            </node>
            <node concept="2oxUTD" id="1AGkceJmPMT" role="2OqNvi">
              <node concept="37vLTw" id="1AGkceJmPO4" role="2oxUTC">
                <ref role="3cqZAo" node="1AGkceJmOAn" resolve="nodeToProcess" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1AGkceJo1IA" role="3cqZAp" />
        <node concept="3clFbF" id="1AGkceJo1Xn" role="3cqZAp">
          <node concept="1rXfSq" id="1AGkceJo1Xl" role="3clFbG">
            <ref role="37wK5l" node="1AGkceJnWvH" resolve="parenthesizeIfNecessary" />
            <node concept="37vLTw" id="1AGkceJo25D" role="37wK5m">
              <ref role="3cqZAo" node="1AGkceJmOyr" resolve="result" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1AGkceJmPPs" role="3cqZAp" />
        <node concept="3cpWs6" id="1AGkceJmPV0" role="3cqZAp">
          <node concept="37vLTw" id="1AGkceJmPZY" role="3cqZAk">
            <ref role="3cqZAo" node="1AGkceJmOyr" resolve="result" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="1AGkceJmOxy" role="1B3o_S" />
      <node concept="3Tqbb2" id="1AGkceJmOxL" role="3clF45">
        <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
      </node>
      <node concept="37vLTG" id="1AGkceJmOyb" role="3clF46">
        <property role="TrG5h" value="sourceNode" />
        <node concept="3Tqbb2" id="1AGkceJmOya" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
      </node>
      <node concept="37vLTG" id="1AGkceJmOyr" role="3clF46">
        <property role="TrG5h" value="result" />
        <node concept="3Tqbb2" id="1AGkceJmOyD" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1AGkceJmQh8" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJmQq2" role="jymVt">
      <property role="TrG5h" value="processRightTransform" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="37vLTG" id="1AGkceJmQtL" role="3clF46">
        <property role="TrG5h" value="sourceNode" />
        <node concept="3Tqbb2" id="1AGkceJmQtM" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
      </node>
      <node concept="37vLTG" id="1AGkceJmQtN" role="3clF46">
        <property role="TrG5h" value="result" />
        <node concept="3Tqbb2" id="1AGkceJmQtO" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        </node>
      </node>
      <node concept="3clFbS" id="1AGkceJmQq5" role="3clF47">
        <node concept="3clFbF" id="1AGkceJmQuw" role="3cqZAp">
          <node concept="2OqwBi" id="1AGkceJmQwn" role="3clFbG">
            <node concept="37vLTw" id="1AGkceJmQuv" role="2Oq$k0">
              <ref role="3cqZAo" node="1AGkceJmQtL" resolve="sourceNode" />
            </node>
            <node concept="1P9Npp" id="1AGkceJmQBt" role="2OqNvi">
              <node concept="37vLTw" id="1AGkceJmQCu" role="1P9ThW">
                <ref role="3cqZAo" node="1AGkceJmQtN" resolve="result" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="1AGkceJmQDI" role="3cqZAp">
          <node concept="2OqwBi" id="1AGkceJmR2w" role="3clFbG">
            <node concept="2OqwBi" id="1AGkceJmQGe" role="2Oq$k0">
              <node concept="37vLTw" id="1AGkceJmQDG" role="2Oq$k0">
                <ref role="3cqZAo" node="1AGkceJmQtN" resolve="result" />
              </node>
              <node concept="3TrEf2" id="1AGkceJmQQR" role="2OqNvi">
                <ref role="3Tt5mk" to="pfd6:5l83jlMf$RD" />
              </node>
            </node>
            <node concept="2oxUTD" id="1AGkceJmRa_" role="2OqNvi">
              <node concept="37vLTw" id="1AGkceJmRbQ" role="2oxUTC">
                <ref role="3cqZAo" node="1AGkceJmQtL" resolve="sourceNode" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5SLXE_2I5g0" role="3cqZAp" />
        <node concept="3clFbF" id="5SLXE_2I5is" role="3cqZAp">
          <node concept="2YIFZM" id="5SLXE_2I6ng" role="3clFbG">
            <ref role="37wK5l" node="1AGkceJrt2y" resolve="checkBinaryExpressionForPriority" />
            <ref role="1Pybhc" node="1AGkceJrt1m" resolve="ParenthesisUtil" />
            <node concept="37vLTw" id="5SLXE_2I6pm" role="37wK5m">
              <ref role="3cqZAo" node="1AGkceJmQtN" resolve="result" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="1AGkceJmRcA" role="3cqZAp" />
        <node concept="3cpWs6" id="1AGkceJmRgM" role="3cqZAp">
          <node concept="37vLTw" id="1AGkceJmRj1" role="3cqZAk">
            <ref role="3cqZAo" node="1AGkceJmQtN" resolve="result" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="1AGkceJmQmv" role="1B3o_S" />
      <node concept="3Tqbb2" id="1AGkceJmQpe" role="3clF45">
        <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
      </node>
    </node>
    <node concept="3Tm1VV" id="1AGkceJmIYB" role="1B3o_S" />
    <node concept="3UR2Jj" id="1AGkceJn$86" role="lGtFl">
      <node concept="TZ5HA" id="1AGkceJn$87" role="TZ5H$">
        <node concept="1dT_AC" id="1AGkceJn$88" role="1dT_Ay">
          <property role="1dT_AB" value="This class is based on jetbrains.mps.baseLanguage.actions.PrecedenceUtil." />
        </node>
        <node concept="1dT_AC" id="1AGkceJn$om" role="1dT_Ay">
          <property role="1dT_AB" value="" />
        </node>
      </node>
    </node>
  </node>
  <node concept="Qs71p" id="1AGkceJmNAd">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="PrecedenceLevel" />
    <node concept="2tJIrI" id="1AGkceJmNVu" role="jymVt" />
    <node concept="3clFbW" id="1AGkceJmO8o" role="jymVt">
      <node concept="3cqZAl" id="1AGkceJmO8p" role="3clF45" />
      <node concept="3clFbS" id="1AGkceJmO8r" role="3clF47">
        <node concept="3SKdUt" id="1AGkceJmOdR" role="3cqZAp">
          <node concept="3SKdUq" id="1AGkceJmOdU" role="3SKWNk">
            <property role="3SKdUp" value="The precedence order of expressions corresponds to the levels &quot;defined&quot; in Java" />
          </node>
        </node>
        <node concept="3SKdUt" id="1AGkceJmOgk" role="3cqZAp">
          <node concept="3SKdUq" id="1AGkceJmOgs" role="3SKWNk">
            <property role="3SKdUp" value="see - http://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html" />
          </node>
        </node>
        <node concept="3clFbH" id="1AGkceJmOgv" role="3cqZAp" />
        <node concept="3SKdUt" id="1AGkceJmOja" role="3cqZAp">
          <node concept="3SKdUq" id="1AGkceJmOjo" role="3SKWNk">
            <property role="3SKdUp" value="The actual priority of Expressions is determined by ordinal of the corresponding" />
          </node>
        </node>
        <node concept="3SKdUt" id="1AGkceJmOkf" role="3cqZAp">
          <node concept="3SKdUq" id="1AGkceJmOkx" role="3SKWNk">
            <property role="3SKdUp" value="enumeration constant; upper constants have higher priority than lower ones" />
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="1AGkceJmO7q" role="1B3o_S" />
    </node>
    <node concept="3Tm1VV" id="1AGkceJmNAe" role="1B3o_S" />
    <node concept="QsSxf" id="1AGkceJmNB8" role="Qtgdg">
      <property role="TrG5h" value="PARENTHESIS" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNBw" role="Qtgdg">
      <property role="TrG5h" value="POSTFIX" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNC7" role="Qtgdg">
      <property role="TrG5h" value="UNARY" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNE9" role="Qtgdg">
      <property role="TrG5h" value="MULTIPLICATIVE" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNF0" role="Qtgdg">
      <property role="TrG5h" value="ADDITIVE" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNFU" role="Qtgdg">
      <property role="TrG5h" value="SHIFT" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNGS" role="Qtgdg">
      <property role="TrG5h" value="RELATIONAL" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNHY" role="Qtgdg">
      <property role="TrG5h" value="EQUALITY" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNJf" role="Qtgdg">
      <property role="TrG5h" value="BITWISE_AND" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNKL" role="Qtgdg">
      <property role="TrG5h" value="BITWISE_XOR" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNLE" role="Qtgdg">
      <property role="TrG5h" value="BITWISE_OR" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNMO" role="Qtgdg">
      <property role="TrG5h" value="LOGICAL_AND" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNO0" role="Qtgdg">
      <property role="TrG5h" value="LOGICAL_OR" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNP8" role="Qtgdg">
      <property role="TrG5h" value="TERNARY" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNQi" role="Qtgdg">
      <property role="TrG5h" value="ASSIGNMENT" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
    <node concept="QsSxf" id="1AGkceJmNSx" role="Qtgdg">
      <property role="TrG5h" value="DEFAULT" />
      <ref role="37wK5l" node="1AGkceJmO8o" resolve="PrecedenceLevel" />
    </node>
  </node>
  <node concept="37WguZ" id="1AGkceJqK3z">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="binaryExpression_factory" />
    <node concept="37WvkG" id="1AGkceJqK3$" role="37WGs$">
      <ref role="37XkoT" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
      <node concept="37Y9Zx" id="1AGkceJqKbk" role="37ZfLb">
        <node concept="3clFbS" id="1AGkceJqKbl" role="2VODD2">
          <node concept="3clFbJ" id="1AGkceJqKbq" role="3cqZAp">
            <node concept="3clFbS" id="1AGkceJqKbr" role="3clFbx">
              <node concept="3clFbF" id="1AGkceJqKpn" role="3cqZAp">
                <node concept="2OqwBi" id="1AGkceJqKMK" role="3clFbG">
                  <node concept="2OqwBi" id="1AGkceJqKs2" role="2Oq$k0">
                    <node concept="1r4Lsj" id="1AGkceJqKpm" role="2Oq$k0" />
                    <node concept="3TrEf2" id="1AGkceJqKAZ" role="2OqNvi">
                      <ref role="3Tt5mk" to="pfd6:5l83jlMf$RD" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="1AGkceJqL1L" role="2OqNvi">
                    <node concept="2OqwBi" id="1AGkceJqL9T" role="2oxUTC">
                      <node concept="1PxgMI" id="1AGkceJqL5d" role="2Oq$k0">
                        <ref role="1PxNhF" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                        <node concept="1r4N5L" id="1AGkceJqL3b" role="1PxMeX" />
                      </node>
                      <node concept="3TrEf2" id="1AGkceJqLnc" role="2OqNvi">
                        <ref role="3Tt5mk" to="pfd6:5l83jlMf$RD" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="1AGkceJqLrd" role="3cqZAp">
                <node concept="2OqwBi" id="1AGkceJqLrf" role="3clFbG">
                  <node concept="2OqwBi" id="1AGkceJqLrg" role="2Oq$k0">
                    <node concept="1r4Lsj" id="1AGkceJqLrh" role="2Oq$k0" />
                    <node concept="3TrEf2" id="1AGkceJqLQg" role="2OqNvi">
                      <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="1AGkceJqLrj" role="2OqNvi">
                    <node concept="2OqwBi" id="1AGkceJqLrk" role="2oxUTC">
                      <node concept="1PxgMI" id="1AGkceJqLrl" role="2Oq$k0">
                        <ref role="1PxNhF" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                        <node concept="1r4N5L" id="1AGkceJqLrm" role="1PxMeX" />
                      </node>
                      <node concept="3TrEf2" id="1AGkceJqLE0" role="2OqNvi">
                        <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="1AGkceJqKcD" role="3clFbw">
              <node concept="1r4N5L" id="1AGkceJqKbD" role="2Oq$k0" />
              <node concept="1mIQ4w" id="1AGkceJqKnP" role="2OqNvi">
                <node concept="chp4Y" id="1AGkceJqKoo" role="cj9EA">
                  <ref role="cht4Q" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="1AGkceJrt1m">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="ParenthesisUtil" />
    <node concept="2tJIrI" id="4_KIr3q0JLO" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJrt2y" role="jymVt">
      <property role="TrG5h" value="checkBinaryExpressionForPriority" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1AGkceJrt2_" role="3clF47">
        <node concept="3clFbF" id="1AGkceJrt6I" role="3cqZAp">
          <node concept="1rXfSq" id="1AGkceJrt6H" role="3clFbG">
            <ref role="37wK5l" node="1AGkceJrt4l" resolve="checkCandidateExpressionsPriorities" />
            <node concept="2ShNRf" id="1AGkceJrt9C" role="37wK5m">
              <node concept="Tc6Ow" id="1AGkceJrtkA" role="2ShVmc">
                <node concept="3Tqbb2" id="1AGkceJrtwA" role="HW$YZ">
                  <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                </node>
                <node concept="37vLTw" id="1AGkceJrtEk" role="HW$Y0">
                  <ref role="3cqZAo" node="1AGkceJrt2P" resolve="binaryExpression" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="1AGkceJrt2g" role="1B3o_S" />
      <node concept="3cqZAl" id="1AGkceJrt2t" role="3clF45" />
      <node concept="37vLTG" id="1AGkceJrt2P" role="3clF46">
        <property role="TrG5h" value="binaryExpression" />
        <node concept="3Tqbb2" id="1AGkceJrt2O" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        </node>
      </node>
      <node concept="P$JXv" id="4_KIr3q13Ko" role="lGtFl">
        <node concept="TZ5HA" id="4_KIr3q13Kp" role="TZ5H$">
          <node concept="1dT_AC" id="4_KIr3q13Kq" role="1dT_Ay">
            <property role="1dT_AB" value="Checks the given binary operation w.r.t. priority. Reorders the expression tree if necessary." />
          </node>
        </node>
        <node concept="TUZQ0" id="4_KIr3q13Kr" role="TUOzN">
          <property role="TUZQ4" value="the expression to check" />
          <node concept="zr_55" id="4_KIr3q13Kt" role="zr_5Q">
            <ref role="zr_51" node="1AGkceJrt2P" resolve="binaryExpression" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1AGkceJrt3b" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJrt4l" role="jymVt">
      <property role="TrG5h" value="checkCandidateExpressionsPriorities" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1AGkceJrt4o" role="3clF47">
        <node concept="2$JKZl" id="1AGkceJrtG4" role="3cqZAp">
          <node concept="3clFbS" id="1AGkceJrtG5" role="2LFqv$">
            <node concept="3cpWs8" id="1AGkceJrwpp" role="3cqZAp">
              <node concept="3cpWsn" id="1AGkceJrwps" role="3cpWs9">
                <property role="TrG5h" value="candidate" />
                <node concept="3Tqbb2" id="1AGkceJrwpo" role="1tU5fm">
                  <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                </node>
                <node concept="2OqwBi" id="1AGkceJrxgg" role="33vP2m">
                  <node concept="37vLTw" id="1AGkceJrwtF" role="2Oq$k0">
                    <ref role="3cqZAo" node="1AGkceJrt4X" resolve="candidates" />
                  </node>
                  <node concept="1uHKPH" id="1AGkceJrz5Y" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="1AGkceJrzbA" role="3cqZAp">
              <node concept="3clFbS" id="1AGkceJrzbC" role="3clFbx">
                <node concept="3clFbF" id="1AGkceJrzh2" role="3cqZAp">
                  <node concept="2OqwBi" id="1AGkceJrzX7" role="3clFbG">
                    <node concept="37vLTw" id="1AGkceJrzml" role="2Oq$k0">
                      <ref role="3cqZAo" node="1AGkceJrt4X" resolve="candidates" />
                    </node>
                    <node concept="2Kt2Hk" id="1AGkceJrBCm" role="2OqNvi" />
                  </node>
                </node>
              </node>
              <node concept="1Wc70l" id="1AGkceJrE_c" role="3clFbw">
                <node concept="1rXfSq" id="1AGkceJrEK8" role="3uHU7w">
                  <ref role="37wK5l" node="1AGkceJrCfG" resolve="checkExpressionParentForPriority" />
                  <node concept="37vLTw" id="1AGkceJrETI" role="37wK5m">
                    <ref role="3cqZAo" node="1AGkceJrwps" resolve="candidate" />
                  </node>
                  <node concept="37vLTw" id="1AGkceJrF7e" role="37wK5m">
                    <ref role="3cqZAo" node="1AGkceJrt4X" resolve="candidates" />
                  </node>
                </node>
                <node concept="1Wc70l" id="1AGkceJrDsV" role="3uHU7B">
                  <node concept="1rXfSq" id="1AGkceJrC$E" role="3uHU7B">
                    <ref role="37wK5l" node="1AGkceJrBXy" resolve="checkExpressionChildForPriority" />
                    <node concept="37vLTw" id="1AGkceJrCGj" role="37wK5m">
                      <ref role="3cqZAo" node="1AGkceJrwps" resolve="candidate" />
                    </node>
                    <node concept="2OqwBi" id="1AGkceJrCW0" role="37wK5m">
                      <node concept="37vLTw" id="1AGkceJrCPs" role="2Oq$k0">
                        <ref role="3cqZAo" node="1AGkceJrwps" resolve="candidate" />
                      </node>
                      <node concept="3TrEf2" id="1AGkceJrD8W" role="2OqNvi">
                        <ref role="3Tt5mk" to="pfd6:5l83jlMf$RD" />
                      </node>
                    </node>
                    <node concept="37vLTw" id="1AGkceJrDjy" role="37wK5m">
                      <ref role="3cqZAo" node="1AGkceJrt4X" resolve="candidates" />
                    </node>
                  </node>
                  <node concept="1rXfSq" id="1AGkceJrDAj" role="3uHU7w">
                    <ref role="37wK5l" node="1AGkceJrBXy" resolve="checkExpressionChildForPriority" />
                    <node concept="37vLTw" id="1AGkceJrDJa" role="37wK5m">
                      <ref role="3cqZAo" node="1AGkceJrwps" resolve="candidate" />
                    </node>
                    <node concept="2OqwBi" id="1AGkceJrE1q" role="37wK5m">
                      <node concept="37vLTw" id="1AGkceJrDUb" role="2Oq$k0">
                        <ref role="3cqZAo" node="1AGkceJrwps" resolve="candidate" />
                      </node>
                      <node concept="3TrEf2" id="1AGkceJrEdZ" role="2OqNvi">
                        <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                      </node>
                    </node>
                    <node concept="37vLTw" id="1AGkceJrEq0" role="37wK5m">
                      <ref role="3cqZAo" node="1AGkceJrt4X" resolve="candidates" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="1AGkceJrux6" role="2$JKZa">
            <node concept="37vLTw" id="1AGkceJrtI_" role="2Oq$k0">
              <ref role="3cqZAo" node="1AGkceJrt4X" resolve="candidates" />
            </node>
            <node concept="3GX2aA" id="1AGkceJrwmN" role="2OqNvi" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1AGkceJrt3N" role="1B3o_S" />
      <node concept="3cqZAl" id="1AGkceJrt4g" role="3clF45" />
      <node concept="37vLTG" id="1AGkceJrt4X" role="3clF46">
        <property role="TrG5h" value="candidates" />
        <node concept="_YKpA" id="1AGkceJrt4V" role="1tU5fm">
          <node concept="3Tqbb2" id="1AGkceJrt5c" role="_ZDj9">
            <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1AGkceJrBOW" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJrBXy" role="jymVt">
      <property role="TrG5h" value="checkExpressionChildForPriority" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1AGkceJrBX_" role="3clF47">
        <node concept="3clFbJ" id="1AGkceJrFtk" role="3cqZAp">
          <node concept="3clFbS" id="1AGkceJrFtm" role="3clFbx">
            <node concept="3cpWs8" id="1AGkceJrFRd" role="3cqZAp">
              <node concept="3cpWsn" id="1AGkceJrFRg" role="3cpWs9">
                <property role="TrG5h" value="childBinaryExprNode" />
                <node concept="3Tqbb2" id="1AGkceJrFRb" role="1tU5fm">
                  <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                </node>
                <node concept="1PxgMI" id="1AGkceJrGRa" role="33vP2m">
                  <ref role="1PxNhF" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                  <node concept="37vLTw" id="1AGkceJrG2A" role="1PxMeX">
                    <ref role="3cqZAo" node="1AGkceJrC4j" resolve="childNode" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="5SLXE_2FZY3" role="3cqZAp">
              <node concept="3clFbS" id="5SLXE_2FZY5" role="3clFbx">
                <node concept="3clFbF" id="5SLXE_2Hehv" role="3cqZAp">
                  <node concept="1rXfSq" id="5SLXE_2Heht" role="3clFbG">
                    <ref role="37wK5l" node="5SLXE_2G3y3" resolve="rotateTree" />
                    <node concept="37vLTw" id="5SLXE_2HesM" role="37wK5m">
                      <ref role="3cqZAo" node="1AGkceJrFRg" resolve="childBinaryExprNode" />
                    </node>
                    <node concept="37vLTw" id="5SLXE_2HeAv" role="37wK5m">
                      <ref role="3cqZAo" node="1AGkceJrC3U" resolve="parentNode" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="5SLXE_2G3Wd" role="3cqZAp">
                  <node concept="2OqwBi" id="5SLXE_2G4Bx" role="3clFbG">
                    <node concept="37vLTw" id="5SLXE_2G3Wb" role="2Oq$k0">
                      <ref role="3cqZAo" node="1AGkceJrC4O" resolve="candidates" />
                    </node>
                    <node concept="TSZUe" id="5SLXE_2G8ue" role="2OqNvi">
                      <node concept="1PxgMI" id="5SLXE_2G8BC" role="25WWJ7">
                        <ref role="1PxNhF" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                        <node concept="37vLTw" id="5SLXE_2G8zk" role="1PxMeX">
                          <ref role="3cqZAo" node="1AGkceJrC4j" resolve="childNode" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs6" id="5SLXE_2G3g9" role="3cqZAp">
                  <node concept="3clFbT" id="5SLXE_2G3jD" role="3cqZAk">
                    <property role="3clFbU" value="false" />
                  </node>
                </node>
              </node>
              <node concept="1rXfSq" id="5SLXE_2G1Np" role="3clFbw">
                <ref role="37wK5l" node="5SLXE_2FMpe" resolve="isBadPriority" />
                <node concept="37vLTw" id="5SLXE_2G1Vh" role="37wK5m">
                  <ref role="3cqZAo" node="1AGkceJrFRg" resolve="childBinaryExprNode" />
                </node>
                <node concept="37vLTw" id="5SLXE_2G24F" role="37wK5m">
                  <ref role="3cqZAo" node="1AGkceJrC3U" resolve="parentNode" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="1AGkceJrFBZ" role="3clFbw">
            <node concept="37vLTw" id="1AGkceJrFzp" role="2Oq$k0">
              <ref role="3cqZAo" node="1AGkceJrC4j" resolve="childNode" />
            </node>
            <node concept="1mIQ4w" id="1AGkceJrFJb" role="2OqNvi">
              <node concept="chp4Y" id="1AGkceJrFJy" role="cj9EA">
                <ref role="cht4Q" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1AGkceJrC2t" role="3cqZAp">
          <node concept="3clFbT" id="1AGkceJrC3H" role="3cqZAk">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1AGkceJrBTQ" role="1B3o_S" />
      <node concept="10P_77" id="1AGkceJrBXv" role="3clF45" />
      <node concept="37vLTG" id="1AGkceJrC3U" role="3clF46">
        <property role="TrG5h" value="parentNode" />
        <node concept="3Tqbb2" id="1AGkceJrC3T" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        </node>
      </node>
      <node concept="37vLTG" id="1AGkceJrC4j" role="3clF46">
        <property role="TrG5h" value="childNode" />
        <node concept="3Tqbb2" id="1AGkceJrC4E" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
        </node>
      </node>
      <node concept="37vLTG" id="1AGkceJrC4O" role="3clF46">
        <property role="TrG5h" value="candidates" />
        <node concept="_YKpA" id="1AGkceJrC5f" role="1tU5fm">
          <node concept="3Tqbb2" id="1AGkceJrC5s" role="_ZDj9">
            <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1AGkceJrC6l" role="jymVt" />
    <node concept="2YIFZL" id="1AGkceJrCfG" role="jymVt">
      <property role="TrG5h" value="checkExpressionParentForPriority" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="1AGkceJrCfH" role="3clF47">
        <node concept="3clFbJ" id="5SLXE_2HeUS" role="3cqZAp">
          <node concept="3clFbS" id="5SLXE_2HeUU" role="3clFbx">
            <node concept="3cpWs6" id="5SLXE_2Hfxe" role="3cqZAp">
              <node concept="3clFbT" id="5SLXE_2Hf$E" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="5SLXE_2HfpO" role="3clFbw">
            <node concept="10Nm6u" id="5SLXE_2HftA" role="3uHU7w" />
            <node concept="2OqwBi" id="5SLXE_2Hfec" role="3uHU7B">
              <node concept="37vLTw" id="5SLXE_2Hf8_" role="2Oq$k0">
                <ref role="3cqZAo" node="1AGkceJrCfM" resolve="node" />
              </node>
              <node concept="1mfA1w" id="5SLXE_2HfoL" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="5SLXE_2HfFx" role="3cqZAp">
          <node concept="3clFbS" id="5SLXE_2HfFz" role="3clFbx">
            <node concept="3cpWs8" id="5SLXE_2Hgpb" role="3cqZAp">
              <node concept="3cpWsn" id="5SLXE_2Hgpe" role="3cpWs9">
                <property role="TrG5h" value="parentNode" />
                <node concept="3Tqbb2" id="5SLXE_2Hgp9" role="1tU5fm">
                  <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                </node>
                <node concept="1PxgMI" id="5SLXE_2HgKw" role="33vP2m">
                  <ref role="1PxNhF" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
                  <node concept="2OqwBi" id="5SLXE_2Hgyt" role="1PxMeX">
                    <node concept="37vLTw" id="5SLXE_2Hgw_" role="2Oq$k0">
                      <ref role="3cqZAo" node="1AGkceJrCfM" resolve="node" />
                    </node>
                    <node concept="1mfA1w" id="5SLXE_2HgIb" role="2OqNvi" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="5SLXE_2HiaD" role="3cqZAp">
              <node concept="3clFbS" id="5SLXE_2HiaF" role="3clFbx">
                <node concept="3clFbF" id="5SLXE_2HiED" role="3cqZAp">
                  <node concept="1rXfSq" id="5SLXE_2HiEB" role="3clFbG">
                    <ref role="37wK5l" node="5SLXE_2G3y3" resolve="rotateTree" />
                    <node concept="37vLTw" id="5SLXE_2HiJq" role="37wK5m">
                      <ref role="3cqZAo" node="1AGkceJrCfM" resolve="node" />
                    </node>
                    <node concept="37vLTw" id="5SLXE_2HiUm" role="37wK5m">
                      <ref role="3cqZAo" node="5SLXE_2Hgpe" resolve="parentNode" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbF" id="5SLXE_2Hj0P" role="3cqZAp">
                  <node concept="2OqwBi" id="5SLXE_2HjFh" role="3clFbG">
                    <node concept="37vLTw" id="5SLXE_2Hj0N" role="2Oq$k0">
                      <ref role="3cqZAo" node="1AGkceJrCfQ" resolve="candidates" />
                    </node>
                    <node concept="TSZUe" id="5SLXE_2HnlP" role="2OqNvi">
                      <node concept="37vLTw" id="5SLXE_2HnqV" role="25WWJ7">
                        <ref role="3cqZAo" node="5SLXE_2Hgpe" resolve="parentNode" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3cpWs6" id="5SLXE_2HnyM" role="3cqZAp">
                  <node concept="3clFbT" id="5SLXE_2HnAn" role="3cqZAk">
                    <property role="3clFbU" value="false" />
                  </node>
                </node>
              </node>
              <node concept="1rXfSq" id="5SLXE_2HikO" role="3clFbw">
                <ref role="37wK5l" node="5SLXE_2FMpe" resolve="isBadPriority" />
                <node concept="37vLTw" id="5SLXE_2HipB" role="37wK5m">
                  <ref role="3cqZAo" node="1AGkceJrCfM" resolve="node" />
                </node>
                <node concept="37vLTw" id="5SLXE_2Hi_F" role="37wK5m">
                  <ref role="3cqZAo" node="5SLXE_2Hgpe" resolve="parentNode" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="5SLXE_2Hgay" role="3clFbw">
            <node concept="2OqwBi" id="5SLXE_2HfOw" role="2Oq$k0">
              <node concept="37vLTw" id="5SLXE_2HfMe" role="2Oq$k0">
                <ref role="3cqZAo" node="1AGkceJrCfM" resolve="node" />
              </node>
              <node concept="1mfA1w" id="5SLXE_2HfZ7" role="2OqNvi" />
            </node>
            <node concept="1mIQ4w" id="5SLXE_2HggC" role="2OqNvi">
              <node concept="chp4Y" id="5SLXE_2Hghm" role="cj9EA">
                <ref role="cht4Q" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1AGkceJrCfI" role="3cqZAp">
          <node concept="3clFbT" id="1AGkceJrCfJ" role="3cqZAk">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1AGkceJrCfK" role="1B3o_S" />
      <node concept="10P_77" id="1AGkceJrCfL" role="3clF45" />
      <node concept="37vLTG" id="1AGkceJrCfM" role="3clF46">
        <property role="TrG5h" value="node" />
        <node concept="3Tqbb2" id="1AGkceJrCfN" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        </node>
      </node>
      <node concept="37vLTG" id="1AGkceJrCfQ" role="3clF46">
        <property role="TrG5h" value="candidates" />
        <node concept="_YKpA" id="1AGkceJrCfR" role="1tU5fm">
          <node concept="3Tqbb2" id="1AGkceJrCfS" role="_ZDj9">
            <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5SLXE_2FM9Q" role="jymVt" />
    <node concept="2YIFZL" id="5SLXE_2FMpe" role="jymVt">
      <property role="TrG5h" value="isBadPriority" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5SLXE_2FMph" role="3clF47">
        <node concept="3cpWs6" id="5SLXE_2FMzR" role="3cqZAp">
          <node concept="22lmx$" id="5SLXE_2FRm4" role="3cqZAk">
            <node concept="1eOMI4" id="5SLXE_2FRuQ" role="3uHU7w">
              <node concept="1Wc70l" id="5SLXE_2FSOv" role="1eOMHV">
                <node concept="3clFbC" id="5SLXE_2FV4V" role="3uHU7w">
                  <node concept="2OqwBi" id="5SLXE_2FYou" role="3uHU7B">
                    <node concept="2OqwBi" id="5SLXE_2FTqW" role="2Oq$k0">
                      <node concept="2OqwBi" id="5SLXE_2FT5V" role="2Oq$k0">
                        <node concept="37vLTw" id="5SLXE_2FSX1" role="2Oq$k0">
                          <ref role="3cqZAo" node="5SLXE_2FMw4" resolve="childNode" />
                        </node>
                        <node concept="2yIwOk" id="5SLXE_2FTll" role="2OqNvi" />
                      </node>
                      <node concept="2qgKlT" id="5SLXE_2FTU0" role="2OqNvi">
                        <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                      </node>
                    </node>
                    <node concept="liA8E" id="5SLXE_2FZlB" role="2OqNvi">
                      <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="5SLXE_2HCX6" role="3uHU7w">
                    <node concept="2OqwBi" id="5SLXE_2HC9K" role="2Oq$k0">
                      <node concept="2OqwBi" id="5SLXE_2HBI_" role="2Oq$k0">
                        <node concept="37vLTw" id="5SLXE_2HBw3" role="2Oq$k0">
                          <ref role="3cqZAo" node="5SLXE_2FMwg" resolve="parentNode" />
                        </node>
                        <node concept="2yIwOk" id="5SLXE_2HBYB" role="2OqNvi" />
                      </node>
                      <node concept="2qgKlT" id="5SLXE_2HCDs" role="2OqNvi">
                        <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                      </node>
                    </node>
                    <node concept="liA8E" id="5SLXE_2HDTS" role="2OqNvi">
                      <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="5SLXE_2FS3c" role="3uHU7B">
                  <node concept="37vLTw" id="5SLXE_2FRA9" role="3uHU7B">
                    <ref role="3cqZAo" node="5SLXE_2FMw4" resolve="childNode" />
                  </node>
                  <node concept="2OqwBi" id="5SLXE_2FSlV" role="3uHU7w">
                    <node concept="37vLTw" id="5SLXE_2FSdQ" role="2Oq$k0">
                      <ref role="3cqZAo" node="5SLXE_2FMwg" resolve="parentNode" />
                    </node>
                    <node concept="3TrEf2" id="5SLXE_2FSIC" role="2OqNvi">
                      <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3eOSWO" id="3G_WKhklYr0" role="3uHU7B">
              <node concept="2OqwBi" id="5SLXE_2FNuu" role="3uHU7B">
                <node concept="2OqwBi" id="5SLXE_2FMTD" role="2Oq$k0">
                  <node concept="2OqwBi" id="5SLXE_2FMGP" role="2Oq$k0">
                    <node concept="37vLTw" id="5SLXE_2FMBi" role="2Oq$k0">
                      <ref role="3cqZAo" node="5SLXE_2FMw4" resolve="childNode" />
                    </node>
                    <node concept="2yIwOk" id="5SLXE_2FMRq" role="2OqNvi" />
                  </node>
                  <node concept="2qgKlT" id="5SLXE_2FNk0" role="2OqNvi">
                    <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                  </node>
                </node>
                <node concept="liA8E" id="5SLXE_2FNWr" role="2OqNvi">
                  <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
                </node>
              </node>
              <node concept="2OqwBi" id="5SLXE_2FPTM" role="3uHU7w">
                <node concept="2OqwBi" id="5SLXE_2FPb9" role="2Oq$k0">
                  <node concept="2OqwBi" id="5SLXE_2FOPi" role="2Oq$k0">
                    <node concept="37vLTw" id="5SLXE_2FOBl" role="2Oq$k0">
                      <ref role="3cqZAo" node="5SLXE_2FMwg" resolve="parentNode" />
                    </node>
                    <node concept="2yIwOk" id="5SLXE_2FP1y" role="2OqNvi" />
                  </node>
                  <node concept="2qgKlT" id="5SLXE_2FPB3" role="2OqNvi">
                    <ref role="37wK5l" to="bkkh:1AGkceJmO3R" resolve="getPriority" />
                  </node>
                </node>
                <node concept="liA8E" id="5SLXE_2FQNz" role="2OqNvi">
                  <ref role="37wK5l" to="e2lb:~Enum.ordinal():int" resolve="ordinal" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="5SLXE_2FMim" role="1B3o_S" />
      <node concept="10P_77" id="5SLXE_2FMpa" role="3clF45" />
      <node concept="37vLTG" id="5SLXE_2FMw4" role="3clF46">
        <property role="TrG5h" value="childNode" />
        <node concept="3Tqbb2" id="5SLXE_2FMw3" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        </node>
      </node>
      <node concept="37vLTG" id="5SLXE_2FMwg" role="3clF46">
        <property role="TrG5h" value="parentNode" />
        <node concept="3Tqbb2" id="5SLXE_2FMwq" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="5SLXE_2G3nD" role="jymVt" />
    <node concept="2YIFZL" id="5SLXE_2G3y3" role="jymVt">
      <property role="TrG5h" value="rotateTree" />
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <node concept="3clFbS" id="5SLXE_2G3y6" role="3clF47">
        <node concept="3cpWs8" id="5SLXE_2H8cH" role="3cqZAp">
          <node concept="3cpWsn" id="5SLXE_2H8cK" role="3cpWs9">
            <property role="TrG5h" value="childIsRightExpr" />
            <node concept="10P_77" id="5SLXE_2H8cG" role="1tU5fm" />
            <node concept="3clFbC" id="5SLXE_2H8RO" role="33vP2m">
              <node concept="37vLTw" id="5SLXE_2H8Xg" role="3uHU7w">
                <ref role="3cqZAo" node="5SLXE_2G3A6" resolve="childNode" />
              </node>
              <node concept="2OqwBi" id="5SLXE_2H8oc" role="3uHU7B">
                <node concept="37vLTw" id="5SLXE_2H8jQ" role="2Oq$k0">
                  <ref role="3cqZAo" node="5SLXE_2G3As" resolve="parentNode" />
                </node>
                <node concept="3TrEf2" id="5SLXE_2H8Eu" role="2OqNvi">
                  <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs8" id="5SLXE_2H9r3" role="3cqZAp">
          <node concept="3cpWsn" id="5SLXE_2H9r6" role="3cpWs9">
            <property role="TrG5h" value="backsideExpr" />
            <node concept="3Tqbb2" id="5SLXE_2H9r1" role="1tU5fm">
              <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
            </node>
            <node concept="3K4zz7" id="5SLXE_2H9Q0" role="33vP2m">
              <node concept="2OqwBi" id="5SLXE_2Ha08" role="3K4E3e">
                <node concept="37vLTw" id="5SLXE_2H9UO" role="2Oq$k0">
                  <ref role="3cqZAo" node="5SLXE_2G3A6" resolve="childNode" />
                </node>
                <node concept="3TrEf2" id="5SLXE_2HaaN" role="2OqNvi">
                  <ref role="3Tt5mk" to="pfd6:5l83jlMf$RD" />
                </node>
              </node>
              <node concept="2OqwBi" id="5SLXE_2Ham0" role="3K4GZi">
                <node concept="37vLTw" id="5SLXE_2Hag1" role="2Oq$k0">
                  <ref role="3cqZAo" node="5SLXE_2G3A6" resolve="childNode" />
                </node>
                <node concept="3TrEf2" id="5SLXE_2HawM" role="2OqNvi">
                  <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                </node>
              </node>
              <node concept="37vLTw" id="5SLXE_2H9C7" role="3K4Cdx">
                <ref role="3cqZAo" node="5SLXE_2H8cK" resolve="childIsRightExpr" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5SLXE_2Haz4" role="3cqZAp" />
        <node concept="3clFbF" id="5SLXE_2HaDQ" role="3cqZAp">
          <node concept="2OqwBi" id="5SLXE_2HaKw" role="3clFbG">
            <node concept="37vLTw" id="5SLXE_2HaDO" role="2Oq$k0">
              <ref role="3cqZAo" node="5SLXE_2G3A6" resolve="childNode" />
            </node>
            <node concept="3YRAZt" id="5SLXE_2HaUZ" role="2OqNvi" />
          </node>
        </node>
        <node concept="3clFbF" id="5SLXE_2Hb2n" role="3cqZAp">
          <node concept="2OqwBi" id="5SLXE_2Hb95" role="3clFbG">
            <node concept="37vLTw" id="5SLXE_2Hb2l" role="2Oq$k0">
              <ref role="3cqZAo" node="5SLXE_2G3As" resolve="parentNode" />
            </node>
            <node concept="1P9Npp" id="5SLXE_2Hblk" role="2OqNvi">
              <node concept="37vLTw" id="5SLXE_2Hbpf" role="1P9ThW">
                <ref role="3cqZAo" node="5SLXE_2G3A6" resolve="childNode" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5SLXE_2Hbwr" role="3cqZAp">
          <node concept="2OqwBi" id="5SLXE_2HbDx" role="3clFbG">
            <node concept="37vLTw" id="5SLXE_2Hbwp" role="2Oq$k0">
              <ref role="3cqZAo" node="5SLXE_2H9r6" resolve="backsideExpr" />
            </node>
            <node concept="1P9Npp" id="5SLXE_2HbKy" role="2OqNvi">
              <node concept="37vLTw" id="5SLXE_2HbOk" role="1P9ThW">
                <ref role="3cqZAo" node="5SLXE_2G3As" resolve="parentNode" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="5SLXE_2HbQk" role="3cqZAp" />
        <node concept="3clFbJ" id="5SLXE_2Hc4o" role="3cqZAp">
          <node concept="3clFbS" id="5SLXE_2Hc4q" role="3clFbx">
            <node concept="3clFbF" id="5SLXE_2Hcj$" role="3cqZAp">
              <node concept="2OqwBi" id="5SLXE_2HcKt" role="3clFbG">
                <node concept="2OqwBi" id="5SLXE_2HcoG" role="2Oq$k0">
                  <node concept="37vLTw" id="5SLXE_2Hcjy" role="2Oq$k0">
                    <ref role="3cqZAo" node="5SLXE_2G3As" resolve="parentNode" />
                  </node>
                  <node concept="3TrEf2" id="5SLXE_2Hczb" role="2OqNvi">
                    <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                  </node>
                </node>
                <node concept="2oxUTD" id="5SLXE_2HcRR" role="2OqNvi">
                  <node concept="37vLTw" id="5SLXE_2HcVY" role="2oxUTC">
                    <ref role="3cqZAo" node="5SLXE_2H9r6" resolve="backsideExpr" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="37vLTw" id="5SLXE_2HceX" role="3clFbw">
            <ref role="3cqZAo" node="5SLXE_2H8cK" resolve="childIsRightExpr" />
          </node>
          <node concept="9aQIb" id="5SLXE_2HcY8" role="9aQIa">
            <node concept="3clFbS" id="5SLXE_2HcY9" role="9aQI4">
              <node concept="3clFbF" id="5SLXE_2Hd2J" role="3cqZAp">
                <node concept="2OqwBi" id="5SLXE_2HdE3" role="3clFbG">
                  <node concept="2OqwBi" id="5SLXE_2Hd7R" role="2Oq$k0">
                    <node concept="37vLTw" id="5SLXE_2Hd2I" role="2Oq$k0">
                      <ref role="3cqZAo" node="5SLXE_2G3As" resolve="parentNode" />
                    </node>
                    <node concept="3TrEf2" id="5SLXE_2HduI" role="2OqNvi">
                      <ref role="3Tt5mk" to="pfd6:5l83jlMf$RD" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="5SLXE_2HdN9" role="2OqNvi">
                    <node concept="37vLTw" id="5SLXE_2HdRg" role="2oxUTC">
                      <ref role="3cqZAo" node="5SLXE_2H9r6" resolve="backsideExpr" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="5SLXE_2G3tY" role="1B3o_S" />
      <node concept="3cqZAl" id="5SLXE_2G3xZ" role="3clF45" />
      <node concept="37vLTG" id="5SLXE_2G3A6" role="3clF46">
        <property role="TrG5h" value="childNode" />
        <node concept="3Tqbb2" id="5SLXE_2G3A5" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        </node>
      </node>
      <node concept="37vLTG" id="5SLXE_2G3As" role="3clF46">
        <property role="TrG5h" value="parentNode" />
        <node concept="3Tqbb2" id="5SLXE_2G3AA" role="1tU5fm">
          <ref role="ehGHo" to="pfd6:5l83jlMf$Lk" resolve="BinaryExpression" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="1AGkceJrCaE" role="jymVt" />
    <node concept="3Tm1VV" id="1AGkceJrt1n" role="1B3o_S" />
  </node>
  <node concept="37WguZ" id="3G_WKhkiXAa">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="parenthesizedExpression_factory" />
    <node concept="37WvkG" id="3G_WKhkiXAb" role="37WGs$">
      <ref role="37XkoT" to="pfd6:5l83jlMgWog" resolve="ParenthesizedExpression" />
      <node concept="37Y9Zx" id="3G_WKhkiXAc" role="37ZfLb">
        <node concept="3clFbS" id="3G_WKhkiXAd" role="2VODD2">
          <node concept="3clFbJ" id="3G_WKhkiXAg" role="3cqZAp">
            <node concept="3clFbS" id="3G_WKhkiXAh" role="3clFbx">
              <node concept="3clFbF" id="3G_WKhkj7Zs" role="3cqZAp">
                <node concept="2OqwBi" id="3G_WKhkj8yP" role="3clFbG">
                  <node concept="2OqwBi" id="3G_WKhkj81f" role="2Oq$k0">
                    <node concept="1r4Lsj" id="3G_WKhkj7Zr" role="2Oq$k0" />
                    <node concept="3TrEf2" id="3G_WKhkj8mG" role="2OqNvi">
                      <ref role="3Tt5mk" to="pfd6:5l83jlMfE3N" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="3G_WKhkj8Eg" role="2OqNvi">
                    <node concept="1PxgMI" id="3G_WKhkj8GB" role="2oxUTC">
                      <ref role="1PxNhF" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
                      <node concept="1r4N5L" id="3G_WKhkj8F6" role="1PxMeX" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="2OqwBi" id="3G_WKhkiXVN" role="3clFbw">
              <node concept="1r4N5L" id="3G_WKhkiXAs" role="2Oq$k0" />
              <node concept="1mIQ4w" id="3G_WKhkj7Yp" role="2OqNvi">
                <node concept="chp4Y" id="3G_WKhkj7YG" role="cj9EA">
                  <ref role="cht4Q" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="3SvAy0XMHI1">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="vectorAccess" />
    <node concept="3UNGvq" id="3SvAy0XMHI2" role="3UOs0v">
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      <node concept="3kRJcU" id="3SvAy0XMI31" role="3kShCk">
        <node concept="3clFbS" id="3SvAy0XMI32" role="2VODD2">
          <node concept="3clFbF" id="3SvAy0XMIhP" role="3cqZAp">
            <node concept="2OqwBi" id="3SvAy0XMJ_E" role="3clFbG">
              <node concept="1UaxmW" id="3SvAy0XMIhM" role="2Oq$k0">
                <node concept="2OqwBi" id="3SvAy0XMIsZ" role="1Ub_4B">
                  <node concept="Cj7Ep" id="3SvAy0XMIpm" role="2Oq$k0" />
                  <node concept="3JvlWi" id="3SvAy0XMIAT" role="2OqNvi" />
                </node>
                <node concept="1YaCAy" id="3SvAy0XMIG6" role="1Ub_4A">
                  <property role="TrG5h" value="v" />
                  <ref role="1YaFvo" to="pfd6:3SvAy0XHWnV" resolve="VectorType" />
                </node>
              </node>
              <node concept="3x8VRR" id="3SvAy0XMJTp" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
      <node concept="tYCnQ" id="3SvAy0XMKdp" role="_1QTJ">
        <ref role="uz4UX" to="pfd6:3SvAy0XKTvE" resolve="VectorElementAccess" />
        <node concept="Cmt7Y" id="3SvAy0XMKsn" role="uz6Si">
          <node concept="Cnhdc" id="3SvAy0XMKsp" role="Cncma">
            <node concept="3clFbS" id="3SvAy0XMKsr" role="2VODD2">
              <node concept="3cpWs8" id="3SvAy0XMKFK" role="3cqZAp">
                <node concept="3cpWsn" id="3SvAy0XMKFN" role="3cpWs9">
                  <property role="TrG5h" value="result" />
                  <node concept="3Tqbb2" id="3SvAy0XMKFJ" role="1tU5fm">
                    <ref role="ehGHo" to="pfd6:3SvAy0XKTvE" resolve="VectorElementAccess" />
                  </node>
                  <node concept="2OqwBi" id="3SvAy0XMKNr" role="33vP2m">
                    <node concept="1Q6Npb" id="3SvAy0XMKM8" role="2Oq$k0" />
                    <node concept="15TzpJ" id="3SvAy0XMKRS" role="2OqNvi">
                      <ref role="I8UWU" to="pfd6:3SvAy0XKTvE" resolve="VectorElementAccess" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="3SvAy0XMKTJ" role="3cqZAp" />
              <node concept="3clFbF" id="3SvAy0XMKZW" role="3cqZAp">
                <node concept="2OqwBi" id="3SvAy0XML23" role="3clFbG">
                  <node concept="Cj7Ep" id="3SvAy0XMKZU" role="2Oq$k0" />
                  <node concept="1P9Npp" id="3SvAy0XMLhC" role="2OqNvi">
                    <node concept="37vLTw" id="3SvAy0XMLjh" role="1P9ThW">
                      <ref role="3cqZAo" node="3SvAy0XMKFN" resolve="result" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="3SvAy0XMLlV" role="3cqZAp">
                <node concept="2OqwBi" id="3SvAy0XMMJO" role="3clFbG">
                  <node concept="2OqwBi" id="3SvAy0XMM0g" role="2Oq$k0">
                    <node concept="37vLTw" id="3SvAy0XMLlT" role="2Oq$k0">
                      <ref role="3cqZAo" node="3SvAy0XMKFN" resolve="result" />
                    </node>
                    <node concept="3TrEf2" id="3SvAy0XMMrw" role="2OqNvi">
                      <ref role="3Tt5mk" to="pfd6:3SvAy0XKTvF" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="3SvAy0XMMT0" role="2OqNvi">
                    <node concept="Cj7Ep" id="3SvAy0XMMVB" role="2oxUTC" />
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="3SvAy0XMKYE" role="3cqZAp" />
              <node concept="3cpWs6" id="3SvAy0XMKUR" role="3cqZAp">
                <node concept="37vLTw" id="3SvAy0XMKVd" role="3cqZAk">
                  <ref role="3cqZAo" node="3SvAy0XMKFN" resolve="result" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="3SvAy0XMKt5" role="Cn2iK">
            <property role="2h1i$Z" value="[" />
          </node>
          <node concept="2h1dTh" id="3SvAy0XMKtn" role="Cn6ar">
            <property role="2h1i$Z" value="vector element access" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="2dq8QBBnk8P">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="makeScientificNumber" />
    <node concept="3UNGvq" id="2dq8QBBnkm_" role="3UOs0v">
      <ref role="3UNGvu" to="pfd6:5l83jlMfYoB" resolve="DecimalLiteral" />
      <node concept="tYCnQ" id="2dq8QBBnkmC" role="_1QTJ">
        <ref role="uz4UX" to="pfd6:2dq8QBBlz5g" resolve="ScientificNumberLiteral" />
        <node concept="Cmt7Y" id="2dq8QBBnkmM" role="uz6Si">
          <node concept="Cnhdc" id="2dq8QBBnkmN" role="Cncma">
            <node concept="3clFbS" id="2dq8QBBnkmO" role="2VODD2">
              <node concept="34ab3g" id="2dq8QBBpH8x" role="3cqZAp">
                <property role="35gtTG" value="info" />
                <node concept="Xl_RD" id="2dq8QBBpH8z" role="34bqiv">
                  <property role="Xl_RC" value="side transform for scientific number" />
                </node>
              </node>
              <node concept="3cpWs8" id="2dq8QBBnknO" role="3cqZAp">
                <node concept="3cpWsn" id="2dq8QBBnknR" role="3cpWs9">
                  <property role="TrG5h" value="sciNum" />
                  <node concept="3Tqbb2" id="2dq8QBBnknN" role="1tU5fm">
                    <ref role="ehGHo" to="pfd6:2dq8QBBlz5g" resolve="ScientificNumberLiteral" />
                  </node>
                  <node concept="2OqwBi" id="2dq8QBBp7Bo" role="33vP2m">
                    <node concept="Cj7Ep" id="2dq8QBBp7z0" role="2Oq$k0" />
                    <node concept="1_qnLN" id="2dq8QBBpBJV" role="2OqNvi">
                      <ref role="1_rbq0" to="pfd6:2dq8QBBlz5g" resolve="ScientificNumberLiteral" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2dq8QBBnviD" role="3cqZAp">
                <node concept="37vLTI" id="2dq8QBBnw03" role="3clFbG">
                  <node concept="2OqwBi" id="2dq8QBBpwBi" role="37vLTx">
                    <node concept="1PxgMI" id="2dq8QBBpwvZ" role="2Oq$k0">
                      <ref role="1PxNhF" to="pfd6:5l83jlMfYoB" resolve="DecimalLiteral" />
                      <node concept="Cj7Ep" id="2dq8QBBpvws" role="1PxMeX" />
                    </node>
                    <node concept="3TrcHB" id="2dq8QBBpwTj" role="2OqNvi">
                      <ref role="3TsBF5" to="pfd6:5l83jlMfYoC" resolve="value" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="2dq8QBBnvmj" role="37vLTJ">
                    <node concept="37vLTw" id="2dq8QBBnviB" role="2Oq$k0">
                      <ref role="3cqZAo" node="2dq8QBBnknR" resolve="sciNum" />
                    </node>
                    <node concept="3TrcHB" id="2dq8QBBnvG8" role="2OqNvi">
                      <ref role="3TsBF5" to="pfd6:2dq8QBBlAhr" resolve="prefix" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="2dq8QBBnwvq" role="3cqZAp" />
              <node concept="3clFbF" id="1sHR4zGCZ2k" role="3cqZAp">
                <node concept="2OqwBi" id="2dq8QBBn_HH" role="3clFbG">
                  <node concept="37vLTw" id="2dq8QBBn_EP" role="2Oq$k0">
                    <ref role="3cqZAo" node="2dq8QBBnknR" resolve="sciNum" />
                  </node>
                  <node concept="1OKiuA" id="2dq8QBBnAvg" role="2OqNvi">
                    <node concept="1XNTG" id="2dq8QBBnAy_" role="lBI5i" />
                    <node concept="eBIwv" id="2dq8QBBnBe$" role="lGT1i">
                      <ref role="fyFUz" to="pfd6:2dq8QBBlAhu" resolve="postfix" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="2dq8QBBnB_X" role="3cqZAp">
                <node concept="37vLTw" id="2dq8QBBo3Pt" role="3cqZAk">
                  <ref role="3cqZAo" node="2dq8QBBnknR" resolve="sciNum" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="2dq8QBBpQdO" role="Cn2iK">
            <property role="2h1i$Z" value="e" />
          </node>
        </node>
      </node>
      <node concept="tYCnQ" id="m1E9k9ba9t" role="_1QTJ">
        <ref role="uz4UX" to="pfd6:2dq8QBBlz5g" resolve="ScientificNumberLiteral" />
        <node concept="Cmt7Y" id="m1E9k9ba9u" role="uz6Si">
          <node concept="Cnhdc" id="m1E9k9ba9v" role="Cncma">
            <node concept="3clFbS" id="m1E9k9ba9w" role="2VODD2">
              <node concept="34ab3g" id="m1E9k9ba9x" role="3cqZAp">
                <property role="35gtTG" value="info" />
                <node concept="Xl_RD" id="m1E9k9ba9y" role="34bqiv">
                  <property role="Xl_RC" value="side transform for scientific number" />
                </node>
              </node>
              <node concept="3cpWs8" id="m1E9k9ba9z" role="3cqZAp">
                <node concept="3cpWsn" id="m1E9k9ba9$" role="3cpWs9">
                  <property role="TrG5h" value="sciNum" />
                  <node concept="3Tqbb2" id="m1E9k9ba9_" role="1tU5fm">
                    <ref role="ehGHo" to="pfd6:2dq8QBBlz5g" resolve="ScientificNumberLiteral" />
                  </node>
                  <node concept="2OqwBi" id="m1E9k9ba9A" role="33vP2m">
                    <node concept="Cj7Ep" id="m1E9k9ba9B" role="2Oq$k0" />
                    <node concept="1_qnLN" id="m1E9k9ba9C" role="2OqNvi">
                      <ref role="1_rbq0" to="pfd6:2dq8QBBlz5g" resolve="ScientificNumberLiteral" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="m1E9k9ba9D" role="3cqZAp">
                <node concept="37vLTI" id="m1E9k9ba9E" role="3clFbG">
                  <node concept="2OqwBi" id="m1E9k9ba9F" role="37vLTx">
                    <node concept="1PxgMI" id="m1E9k9ba9G" role="2Oq$k0">
                      <ref role="1PxNhF" to="pfd6:5l83jlMfYoB" resolve="DecimalLiteral" />
                      <node concept="Cj7Ep" id="m1E9k9ba9H" role="1PxMeX" />
                    </node>
                    <node concept="3TrcHB" id="m1E9k9ba9I" role="2OqNvi">
                      <ref role="3TsBF5" to="pfd6:5l83jlMfYoC" resolve="value" />
                    </node>
                  </node>
                  <node concept="2OqwBi" id="m1E9k9ba9J" role="37vLTJ">
                    <node concept="37vLTw" id="m1E9k9ba9K" role="2Oq$k0">
                      <ref role="3cqZAo" node="m1E9k9ba9$" resolve="sciNum" />
                    </node>
                    <node concept="3TrcHB" id="m1E9k9ba9L" role="2OqNvi">
                      <ref role="3TsBF5" to="pfd6:2dq8QBBlAhr" resolve="prefix" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="m1E9k9ba9M" role="3cqZAp" />
              <node concept="3clFbF" id="m1E9k9ba9N" role="3cqZAp">
                <node concept="2OqwBi" id="m1E9k9ba9O" role="3clFbG">
                  <node concept="37vLTw" id="m1E9k9ba9P" role="2Oq$k0">
                    <ref role="3cqZAo" node="m1E9k9ba9$" resolve="sciNum" />
                  </node>
                  <node concept="1OKiuA" id="m1E9k9ba9Q" role="2OqNvi">
                    <node concept="1XNTG" id="m1E9k9ba9R" role="lBI5i" />
                    <node concept="eBIwv" id="m1E9k9ba9S" role="lGT1i">
                      <ref role="fyFUz" to="pfd6:2dq8QBBlAhu" resolve="postfix" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="m1E9k9ba9T" role="3cqZAp">
                <node concept="37vLTw" id="m1E9k9ba9U" role="3cqZAk">
                  <ref role="3cqZAo" node="m1E9k9ba9$" resolve="sciNum" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="m1E9k9ba9V" role="Cn2iK">
            <property role="2h1i$Z" value="E" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="2dq8QBBpPmd">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="makeUnaryMinuxExpression" />
    <node concept="3UNGvq" id="2dq8QBBpPme" role="3UOs0v">
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      <node concept="3kRJcU" id="2dq8QBBpPmh" role="3kShCk">
        <node concept="3clFbS" id="2dq8QBBpPmi" role="2VODD2">
          <node concept="3clFbF" id="2dq8QBBpPrm" role="3cqZAp">
            <node concept="3y3z36" id="2dq8QBBpPCE" role="3clFbG">
              <node concept="10Nm6u" id="2dq8QBBpPFy" role="3uHU7w" />
              <node concept="Cj7Ep" id="2dq8QBBpPrk" role="3uHU7B" />
            </node>
          </node>
        </node>
      </node>
      <node concept="tYCnQ" id="2dq8QBBpPKM" role="_1QTJ">
        <ref role="uz4UX" to="pfd6:2dq8QBBpO8s" resolve="UnaryMinuxExpression" />
        <node concept="Cmt7Y" id="2dq8QBBpPV7" role="uz6Si">
          <node concept="Cnhdc" id="2dq8QBBpPV8" role="Cncma">
            <node concept="3clFbS" id="2dq8QBBpPV9" role="2VODD2">
              <node concept="3cpWs8" id="2dq8QBBpTRa" role="3cqZAp">
                <node concept="3cpWsn" id="2dq8QBBpTRd" role="3cpWs9">
                  <property role="TrG5h" value="result" />
                  <node concept="3Tqbb2" id="2dq8QBBpTR9" role="1tU5fm">
                    <ref role="ehGHo" to="pfd6:2dq8QBBpO8s" resolve="UnaryMinuxExpression" />
                  </node>
                  <node concept="2OqwBi" id="2dq8QBBpTXo" role="33vP2m">
                    <node concept="Cj7Ep" id="2dq8QBBpTVx" role="2Oq$k0" />
                    <node concept="2DeJnW" id="2dq8QBBpUm5" role="2OqNvi">
                      <ref role="1_rbq0" to="pfd6:2dq8QBBpO8s" resolve="UnaryMinuxExpression" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbH" id="2dq8QBBpUo7" role="3cqZAp" />
              <node concept="3clFbF" id="2dq8QBBpUvr" role="3cqZAp">
                <node concept="37vLTI" id="2dq8QBBpVfi" role="3clFbG">
                  <node concept="Cj7Ep" id="2dq8QBBpVi0" role="37vLTx" />
                  <node concept="2OqwBi" id="2dq8QBBpUyL" role="37vLTJ">
                    <node concept="37vLTw" id="2dq8QBBpUvp" role="2Oq$k0">
                      <ref role="3cqZAo" node="2dq8QBBpTRd" resolve="result" />
                    </node>
                    <node concept="3TrEf2" id="2dq8QBBpUYU" role="2OqNvi">
                      <ref role="3Tt5mk" to="pfd6:5l83jlMfE3N" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2dq8QBBpVHh" role="3cqZAp">
                <node concept="2OqwBi" id="2dq8QBBpVL1" role="3clFbG">
                  <node concept="37vLTw" id="2dq8QBBpVHf" role="2Oq$k0">
                    <ref role="3cqZAo" node="2dq8QBBpTRd" resolve="result" />
                  </node>
                  <node concept="1OKiuA" id="2dq8QBBpWe2" role="2OqNvi">
                    <node concept="1XNTG" id="2dq8QBBpWgC" role="lBI5i" />
                    <node concept="2B6iha" id="2dq8QBBpWm6" role="lGT1i">
                      <property role="1lyBwo" value="firstEditable" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="2dq8QBBpUpz" role="3cqZAp">
                <node concept="37vLTw" id="2dq8QBBpUr2" role="3cqZAk">
                  <ref role="3cqZAo" node="2dq8QBBpTRd" resolve="result" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="2dq8QBBpPVP" role="Cn2iK">
            <property role="2h1i$Z" value="-" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3FK_9_" id="m1E9k9d0E8">
    <property role="3GE5qa" value="literals" />
    <property role="TrG5h" value="makeDecimalLiteral" />
    <node concept="3FOIzC" id="m1E9k9d0E9" role="3FOPby">
      <ref role="3FOWKa" to="pfd6:m1E9k98BVA" resolve="IntegerLiteral" />
      <node concept="tYCnQ" id="m1E9k9d1iN" role="tZc4B">
        <ref role="uz4UX" to="pfd6:5l83jlMfYoB" resolve="DecimalLiteral" />
        <node concept="ucClh" id="m1E9k9d1iP" role="uz6Si">
          <node concept="ucgPf" id="m1E9k9d1iQ" role="ucMEw">
            <node concept="3clFbS" id="m1E9k9d1iR" role="2VODD2">
              <node concept="3cpWs8" id="m1E9k9d4MN" role="3cqZAp">
                <node concept="3cpWsn" id="m1E9k9d4MQ" role="3cpWs9">
                  <property role="TrG5h" value="literal" />
                  <node concept="3Tqbb2" id="m1E9k9d4MM" role="1tU5fm">
                    <ref role="ehGHo" to="pfd6:5l83jlMfYoB" resolve="DecimalLiteral" />
                  </node>
                  <node concept="2OqwBi" id="m1E9k9d4Tq" role="33vP2m">
                    <node concept="1Q6Npb" id="m1E9k9d4S6" role="2Oq$k0" />
                    <node concept="15TzpJ" id="m1E9k9d52R" role="2OqNvi">
                      <ref role="I8UWU" to="pfd6:5l83jlMfYoB" resolve="DecimalLiteral" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="m1E9k9d5a3" role="3cqZAp">
                <node concept="2OqwBi" id="m1E9k9d5Kk" role="3clFbG">
                  <node concept="2OqwBi" id="m1E9k9d5do" role="2Oq$k0">
                    <node concept="37vLTw" id="m1E9k9d5a1" role="2Oq$k0">
                      <ref role="3cqZAo" node="m1E9k9d4MQ" resolve="literal" />
                    </node>
                    <node concept="3TrcHB" id="m1E9k9d5st" role="2OqNvi">
                      <ref role="3TsBF5" to="pfd6:5l83jlMfYoC" resolve="value" />
                    </node>
                  </node>
                  <node concept="tyxLq" id="m1E9k9d6cE" role="2OqNvi">
                    <node concept="ub8z3" id="m1E9k9d6fm" role="tz02z" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="m1E9k9d579" role="3cqZAp">
                <node concept="37vLTw" id="m1E9k9d58n" role="3cqZAk">
                  <ref role="3cqZAo" node="m1E9k9d4MQ" resolve="literal" />
                </node>
              </node>
            </node>
          </node>
          <node concept="uaGSO" id="m1E9k9d2dq" role="ucKa5">
            <node concept="3clFbS" id="m1E9k9d2dr" role="2VODD2">
              <node concept="3clFbF" id="m1E9k9d2jz" role="3cqZAp">
                <node concept="2OqwBi" id="m1E9k9d2u0" role="3clFbG">
                  <node concept="ub8z3" id="m1E9k9d2jy" role="2Oq$k0" />
                  <node concept="liA8E" id="m1E9k9d2Vn" role="2OqNvi">
                    <ref role="37wK5l" to="e2lb:~String.matches(java.lang.String):boolean" resolve="matches" />
                    <node concept="Xl_RD" id="m1E9k9d313" role="37wK5m">
                      <property role="Xl_RC" value="(-?)\\d*.\\d+" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="uGdhv" id="m1E9k9d4io" role="uGu3D">
            <node concept="3clFbS" id="m1E9k9d4iq" role="2VODD2">
              <node concept="3clFbF" id="m1E9k9d4tY" role="3cqZAp">
                <node concept="ub8z3" id="m1E9k9d4tX" role="3clFbG" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="5nlyqYpkFgu">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="transform_VectorType" />
    <node concept="3UNGvq" id="5nlyqYpr_JO" role="3UOs0v">
      <property role="2uHTBK" value="ext_1_RTransform" />
      <property role="7I3sp" value="both" />
      <ref role="3UNGvu" to="pfd6:3SvAy0XHWnV" resolve="VectorType" />
      <node concept="tYCnQ" id="5nlyqYpr_JP" role="_1QTJ">
        <ref role="uz4UX" to="pfd6:3SvAy0XHWnV" resolve="VectorType" />
        <node concept="Cmt7Y" id="5nlyqYpr_JQ" role="uz6Si">
          <node concept="Cnhdc" id="5nlyqYpr_JR" role="Cncma">
            <node concept="3clFbS" id="5nlyqYpr_JS" role="2VODD2">
              <node concept="3clFbF" id="5nlyqYpr_JT" role="3cqZAp">
                <node concept="2OqwBi" id="5nlyqYpr_JU" role="3clFbG">
                  <node concept="2OqwBi" id="5nlyqYpr_JV" role="2Oq$k0">
                    <node concept="Cj7Ep" id="5nlyqYpr_JW" role="2Oq$k0" />
                    <node concept="3TrEf2" id="5nlyqYpr_JX" role="2OqNvi">
                      <ref role="3Tt5mk" to="pfd6:5nlyqYp8A3k" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="5nlyqYpr_JY" role="2OqNvi">
                    <ref role="1A9B2P" to="pfd6:m1E9k98BVA" resolve="IntegerLiteral" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="5nlyqYpr_JZ" role="3cqZAp">
                <node concept="Cj7Ep" id="5nlyqYpr_K0" role="3cqZAk" />
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="5nlyqYpr_K1" role="Cn2iK">
            <property role="2h1i$Z" value="," />
          </node>
          <node concept="2h1dTh" id="5nlyqYpr_K2" role="Cn6ar">
            <property role="2h1i$Z" value="add vector dimension" />
          </node>
        </node>
      </node>
      <node concept="3kRJcU" id="5nlyqYpr_K3" role="3kShCk">
        <node concept="3clFbS" id="5nlyqYpr_K4" role="2VODD2">
          <node concept="3clFbF" id="5nlyqYpr_K5" role="3cqZAp">
            <node concept="2OqwBi" id="5nlyqYpr_K6" role="3clFbG">
              <node concept="2OqwBi" id="5nlyqYpr_K7" role="2Oq$k0">
                <node concept="Cj7Ep" id="5nlyqYpr_K8" role="2Oq$k0" />
                <node concept="3TrEf2" id="5nlyqYpr_K9" role="2OqNvi">
                  <ref role="3Tt5mk" to="pfd6:5nlyqYp8A3k" />
                </node>
              </node>
              <node concept="3w_OXm" id="5nlyqYpr_Ka" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

