<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:493484be-3d66-4fd8-b261-ed47cd8c6981(de.ppme.statements.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpee" ref="r:00000000-0000-4000-0000-011c895902ca(jetbrains.mps.baseLanguage.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="q0gb" ref="r:4efcc790-8258-4a5c-a60f-10a5b5a367a2(de.ppme.base.structure)" implicit="true" />
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="4628067390765956802" name="abstract" index="R5$K7" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ" />
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="5l83jlMhgCt">
    <property role="TrG5h" value="Statement" />
    <property role="R4oN_" value="an empty statement" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="7GORU49qKH9" role="PzmwI">
      <ref role="PrY4T" node="7GORU49p0om" resolve="GlobalVariableDeclarationProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMhgFI">
    <property role="TrG5h" value="StatementList" />
    <property role="34LRSv" value="{" />
    <property role="R4oN_" value="statement list" />
    <ref role="1TJDcQ" node="5l83jlMhgCt" resolve="Statement" />
    <node concept="1TJgyj" id="5l83jlMhgFJ" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="statement" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="5l83jlMhgCt" resolve="Statement" />
    </node>
    <node concept="PrWs8" id="3yJ4drhVm3y" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="PrWs8" id="7GORU49qKG2" role="PzmwI">
      <ref role="PrY4T" node="7GORU49p0om" resolve="GlobalVariableDeclarationProvider" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMhh0E">
    <property role="TrG5h" value="ExpressionStatement" />
    <property role="R4oN_" value="a statement made of an expression" />
    <property role="34LRSv" value="&lt;expr&gt;;" />
    <ref role="1TJDcQ" node="5l83jlMhgCt" resolve="Statement" />
    <node concept="1TJgyj" id="5l83jlMhh0F" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="expression" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMhBWg">
    <property role="TrG5h" value="VariableDeclaration" />
    <property role="3GE5qa" value="variables" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="5o9jvTM873l" role="PzmwI">
      <ref role="PrY4T" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
    </node>
    <node concept="PrWs8" id="6fgLCPsEaLn" role="PzmwI">
      <ref role="PrY4T" to="q0gb:6fgLCPsE7bT" resolve="IIdentifierNamedConcept" />
    </node>
    <node concept="PrWs8" id="6fgLCPsBygo" role="PzmwI">
      <ref role="PrY4T" to="pfd6:6fgLCPsByeK" resolve="ITyped" />
    </node>
    <node concept="PrWs8" id="5o9jvTM88OY" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyj" id="5l83jlMhFC2" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="init" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="5l83jlMivi_">
    <property role="TrG5h" value="VariableReference" />
    <property role="3GE5qa" value="variables" />
    <ref role="1TJDcQ" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    <node concept="PrWs8" id="5l83jlMivj1" role="PzmwI">
      <ref role="PrY4T" to="pfd6:5l83jlMhoVs" resolve="IVariableReference" />
    </node>
    <node concept="1TJgyj" id="5l83jlMivj4" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="variableDeclaration" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="tpee:4H$HgYMZ7sw" resolve="IVariableDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="5o9jvTMcVMM">
    <property role="TrG5h" value="VariableDeclarationStatement" />
    <property role="R4oN_" value="variable declaration statement" />
    <property role="3GE5qa" value="variables" />
    <property role="34LRSv" value="&lt;type&gt; &lt;var&gt;" />
    <ref role="1TJDcQ" node="5l83jlMhgCt" resolve="Statement" />
    <node concept="1TJgyj" id="5o9jvTMcVMR" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="variableDeclaration" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5l83jlMhBWg" resolve="VariableDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="3yJ4dri0bx9">
    <property role="TrG5h" value="PrintStatement" />
    <property role="34LRSv" value="print" />
    <ref role="1TJDcQ" node="5l83jlMhgCt" resolve="Statement" />
    <node concept="1TJgyj" id="3yJ4dri0bKx" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="expression" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="1RtLc$XKLYm" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="interval" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
  </node>
  <node concept="1TIwiD" id="3yJ4dri0HDh">
    <property role="TrG5h" value="IfStatement" />
    <property role="34LRSv" value="if" />
    <ref role="1TJDcQ" node="5l83jlMhgCt" resolve="Statement" />
    <node concept="1TJgyj" id="3yJ4dri0HIg" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="condition" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="3yJ4dri0Ss3" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="ifTrue" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5l83jlMhgFI" resolve="StatementList" />
    </node>
    <node concept="1TJgyj" id="3yJ4dri1fJ0" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="elseIfClauses" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="3yJ4dri1cHG" resolve="ElseIfClause" />
    </node>
    <node concept="1TJgyj" id="3yJ4dri0Ss0" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="ifFalse" />
      <ref role="20lvS9" node="5l83jlMhgFI" resolve="StatementList" />
    </node>
  </node>
  <node concept="1TIwiD" id="3yJ4dri1cHG">
    <property role="TrG5h" value="ElseIfClause" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="3yJ4dri1cHH" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="condition" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="3yJ4dri1cHJ" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="body" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5l83jlMhgFI" resolve="StatementList" />
    </node>
  </node>
  <node concept="1TIwiD" id="6JTxo0b0Q4k">
    <property role="TrG5h" value="ForEachStatement" />
    <property role="34LRSv" value="for" />
    <property role="R4oN_" value="for (type name : iterable)" />
    <property role="3GE5qa" value="loops" />
    <ref role="1TJDcQ" node="6JTxo0b1E1g" resolve="AbstractForStatement" />
    <node concept="1TJgyj" id="6JTxo0b0RKI" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="iterable" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
    </node>
    <node concept="1TJgyj" id="6JTxo0b37O3" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="elementProperty" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="6JTxo0b37Et" resolve="ForEachElementProperty" />
    </node>
  </node>
  <node concept="1TIwiD" id="6JTxo0b1E1g">
    <property role="TrG5h" value="AbstractForStatement" />
    <property role="R5$K7" value="true" />
    <property role="3GE5qa" value="loops" />
    <ref role="1TJDcQ" node="6JTxo0b1E4m" resolve="AbstractLoopStatement" />
    <node concept="1TJgyj" id="6JTxo0b1E1j" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="variable" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="3z8Mov8aLkc" resolve="LocalVariableDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="6JTxo0b1E4m">
    <property role="R5$K7" value="true" />
    <property role="TrG5h" value="AbstractLoopStatement" />
    <property role="3GE5qa" value="loops" />
    <ref role="1TJDcQ" node="5l83jlMhgCt" resolve="Statement" />
    <node concept="PrWs8" id="6JTxo0b1E4p" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyj" id="6JTxo0b1E4r" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="body" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5l83jlMhgFI" resolve="StatementList" />
    </node>
  </node>
  <node concept="1TIwiD" id="6JTxo0b37Et">
    <property role="3GE5qa" value="loops" />
    <property role="TrG5h" value="ForEachElementProperty" />
    <ref role="1TJDcQ" node="5l83jlMhBWg" resolve="VariableDeclaration" />
  </node>
  <node concept="1TIwiD" id="3z8Mov8aLkc">
    <property role="3GE5qa" value="variables" />
    <property role="TrG5h" value="LocalVariableDeclaration" />
    <ref role="1TJDcQ" node="5l83jlMhBWg" resolve="VariableDeclaration" />
  </node>
  <node concept="PlHQZ" id="7GORU49p0om">
    <property role="TrG5h" value="GlobalVariableDeclarationProvider" />
  </node>
  <node concept="1TIwiD" id="hRSdiNcI$6">
    <property role="3GE5qa" value="variables" />
    <property role="TrG5h" value="LocalVariableDeclarationStatement" />
    <property role="R4oN_" value="local variable declaration statement" />
    <ref role="1TJDcQ" node="5l83jlMhgCt" resolve="Statement" />
    <node concept="1TJgyj" id="hRSdiNcINU" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="decl" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="5l83jlMhBWg" resolve="VariableDeclaration" />
    </node>
  </node>
</model>

