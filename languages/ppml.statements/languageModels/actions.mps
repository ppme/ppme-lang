<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:980092ab-3747-494a-abf6-499a33184b5d(de.ppme.statements.actions)">
  <persistence version="9" />
  <languages>
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="c9eo" ref="r:493484be-3d66-4fd8-b261-ed47cd8c6981(de.ppme.statements.structure)" />
    <import index="2gyk" ref="r:be57a480-399b-470c-860e-bdfcf98e0b2c(de.ppme.core.structure)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="5979988948250981289" name="jetbrains.mps.lang.actions.structure.SNodeCreatorAndInitializer" flags="nn" index="2fJWfE" />
      <concept id="1196433923911" name="jetbrains.mps.lang.actions.structure.SideTransform_SimpleString" flags="nn" index="2h1dTh">
        <property id="1196433942569" name="text" index="2h1i$Z" />
      </concept>
      <concept id="1177323996388" name="jetbrains.mps.lang.actions.structure.AddMenuPart" flags="ng" index="tYCnQ" />
      <concept id="1177333529597" name="jetbrains.mps.lang.actions.structure.ConceptPart" flags="ng" index="uyZFJ">
        <reference id="1177333551023" name="concept" index="uz4UX" />
        <child id="1177333559040" name="part" index="uz6Si" />
      </concept>
      <concept id="1177497140107" name="jetbrains.mps.lang.actions.structure.ConceptFunctionParameter_sourceNode" flags="nn" index="Cj7Ep" />
      <concept id="1177498013932" name="jetbrains.mps.lang.actions.structure.SimpleSideTransformMenuPart" flags="ng" index="Cmt7Y">
        <child id="1177498166690" name="matchingText" index="Cn2iK" />
        <child id="1177498207384" name="handler" index="Cncma" />
      </concept>
      <concept id="1177498227294" name="jetbrains.mps.lang.actions.structure.QueryFunction_SideTransform_Handler" flags="in" index="Cnhdc" />
      <concept id="767145758118872833" name="jetbrains.mps.lang.actions.structure.NF_LinkList_AddNewChildOperation" flags="nn" index="2DeJg1" />
      <concept id="767145758118872830" name="jetbrains.mps.lang.actions.structure.NF_Link_SetNewChildOperation" flags="nn" index="2DeJnY" />
      <concept id="1154622616118" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstitutePreconditionFunction" flags="in" index="3kRJcU" />
      <concept id="1138079221458" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstituteActionsBuilder" flags="ig" index="3UNGvq">
        <property id="1140829165817" name="transformTag" index="2uHTBK" />
        <property id="1158952484319" name="description" index="3mWRNi" />
        <reference id="1138079221462" name="applicableConcept" index="3UNGvu" />
        <child id="1177442283389" name="part" index="_1QTJ" />
        <child id="1154622757656" name="precondition" index="3kShCk" />
      </concept>
      <concept id="1138079416598" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstituteActions" flags="ng" index="3UOs0u">
        <child id="1138079416599" name="actionsBuilder" index="3UOs0v" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138757581985" name="jetbrains.mps.lang.smodel.structure.Link_SetNewChildOperation" flags="nn" index="zfrQC">
        <reference id="1139880128956" name="concept" index="1A9B2P" />
      </concept>
      <concept id="1143224066846" name="jetbrains.mps.lang.smodel.structure.Node_InsertNextSiblingOperation" flags="nn" index="HtI8k">
        <child id="1143224066849" name="insertedNode" index="HtI8F" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1171999116870" name="jetbrains.mps.lang.smodel.structure.Node_IsNullOperation" flags="nn" index="3w_OXm" />
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <reference id="1140138128738" name="concept" index="1PxNhF" />
        <child id="1140138123956" name="leftExpression" index="1PxMeX" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="3UOs0u" id="5l83jlMhPgl">
    <property role="TrG5h" value="makeInitExpression" />
    <node concept="3UNGvq" id="5l83jlMhPgm" role="3UOs0v">
      <ref role="3UNGvu" to="c9eo:5l83jlMhBWg" resolve="VariableDeclaration" />
      <node concept="tYCnQ" id="5l83jlMhPj3" role="_1QTJ">
        <ref role="uz4UX" to="c9eo:5l83jlMhBWg" resolve="VariableDeclaration" />
        <node concept="Cmt7Y" id="5l83jlMhPj5" role="uz6Si">
          <node concept="Cnhdc" id="5l83jlMhPj6" role="Cncma">
            <node concept="3clFbS" id="5l83jlMhPj7" role="2VODD2">
              <node concept="3clFbF" id="5l83jlMhPjN" role="3cqZAp">
                <node concept="2OqwBi" id="5l83jlMhPYx" role="3clFbG">
                  <node concept="2OqwBi" id="5l83jlMhPmO" role="2Oq$k0">
                    <node concept="Cj7Ep" id="5l83jlMhPjM" role="2Oq$k0" />
                    <node concept="3TrEf2" id="5l83jlMhPKm" role="2OqNvi">
                      <ref role="3Tt5mk" to="c9eo:5l83jlMhFC2" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="5l83jlMhQed" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbF" id="5l83jlMhQin" role="3cqZAp">
                <node concept="2OqwBi" id="5l83jlMhQl$" role="3clFbG">
                  <node concept="Cj7Ep" id="5l83jlMhQil" role="2Oq$k0" />
                  <node concept="3TrEf2" id="5l83jlMhQK6" role="2OqNvi">
                    <ref role="3Tt5mk" to="c9eo:5l83jlMhFC2" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="5l83jlMhPjK" role="Cn2iK">
            <property role="2h1i$Z" value="=" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="3yJ4dri1wvO">
    <property role="TrG5h" value="ifStatement" />
    <node concept="3UNGvq" id="3yJ4dri1wvP" role="3UOs0v">
      <property role="2uHTBK" value="ext_1_RTransform" />
      <property role="3mWRNi" value="add else branch after if" />
      <ref role="3UNGvu" to="c9eo:3yJ4dri0HDh" resolve="IfStatement" />
      <node concept="tYCnQ" id="3yJ4dri1ygb" role="_1QTJ">
        <ref role="uz4UX" to="c9eo:3yJ4dri0HDh" resolve="IfStatement" />
        <node concept="Cmt7Y" id="3yJ4dri1BjF" role="uz6Si">
          <node concept="Cnhdc" id="3yJ4dri1BjG" role="Cncma">
            <node concept="3clFbS" id="3yJ4dri1BjH" role="2VODD2">
              <node concept="3clFbF" id="3yJ4dri1BkD" role="3cqZAp">
                <node concept="2OqwBi" id="3yJ4dri1BFs" role="3clFbG">
                  <node concept="2OqwBi" id="3yJ4dri1BmH" role="2Oq$k0">
                    <node concept="Cj7Ep" id="3yJ4dri1BkC" role="2Oq$k0" />
                    <node concept="3TrEf2" id="3yJ4dri1Bwt" role="2OqNvi">
                      <ref role="3Tt5mk" to="c9eo:3yJ4dri0Ss0" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="3yJ4dri1BYt" role="2OqNvi">
                    <ref role="1A9B2P" to="c9eo:5l83jlMhgFI" resolve="StatementList" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="3yJ4dri1C4$" role="3cqZAp">
                <node concept="2OqwBi" id="3yJ4dri1C8g" role="3cqZAk">
                  <node concept="Cj7Ep" id="3yJ4dri1C5F" role="2Oq$k0" />
                  <node concept="3TrEf2" id="3yJ4dri1CrI" role="2OqNvi">
                    <ref role="3Tt5mk" to="c9eo:3yJ4dri0Ss0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="3yJ4dri1Bkm" role="Cn2iK">
            <property role="2h1i$Z" value="else" />
          </node>
        </node>
      </node>
      <node concept="3kRJcU" id="3yJ4dri1wwR" role="3kShCk">
        <node concept="3clFbS" id="3yJ4dri1wwS" role="2VODD2">
          <node concept="3clFbF" id="3yJ4dri1w_N" role="3cqZAp">
            <node concept="2OqwBi" id="3yJ4dri1xwC" role="3clFbG">
              <node concept="2OqwBi" id="3yJ4dri1wGE" role="2Oq$k0">
                <node concept="Cj7Ep" id="3yJ4dri1w_M" role="2Oq$k0" />
                <node concept="3TrEf2" id="3yJ4dri1xgb" role="2OqNvi">
                  <ref role="3Tt5mk" to="c9eo:3yJ4dri0Ss0" />
                </node>
              </node>
              <node concept="3w_OXm" id="3yJ4dri1xRV" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="3UNGvq" id="3yJ4dri1Y0n" role="3UOs0v">
      <ref role="3UNGvu" to="c9eo:3yJ4dri1cHG" resolve="ElseIfClause" />
      <node concept="tYCnQ" id="3yJ4dri1Zav" role="_1QTJ">
        <ref role="uz4UX" to="c9eo:5l83jlMhgFI" resolve="StatementList" />
        <node concept="Cmt7Y" id="3yJ4dri1Zax" role="uz6Si">
          <node concept="Cnhdc" id="3yJ4dri1Zay" role="Cncma">
            <node concept="3clFbS" id="3yJ4dri1Zaz" role="2VODD2">
              <node concept="3clFbF" id="3yJ4dri1Zbv" role="3cqZAp">
                <node concept="2OqwBi" id="3yJ4dri204X" role="3clFbG">
                  <node concept="2OqwBi" id="3yJ4dri1Zy2" role="2Oq$k0">
                    <node concept="1PxgMI" id="3yJ4dri1Zu2" role="2Oq$k0">
                      <ref role="1PxNhF" to="c9eo:3yJ4dri0HDh" resolve="IfStatement" />
                      <node concept="2OqwBi" id="3yJ4dri1Zdd" role="1PxMeX">
                        <node concept="Cj7Ep" id="3yJ4dri1Zbu" role="2Oq$k0" />
                        <node concept="1mfA1w" id="3yJ4dri1Zle" role="2OqNvi" />
                      </node>
                    </node>
                    <node concept="3TrEf2" id="3yJ4dri1ZR7" role="2OqNvi">
                      <ref role="3Tt5mk" to="c9eo:3yJ4dri0Ss0" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="3yJ4dri20oM" role="2OqNvi">
                    <ref role="1A9B2P" to="c9eo:5l83jlMhgFI" resolve="StatementList" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="3yJ4dri20v_" role="3cqZAp">
                <node concept="2OqwBi" id="3yJ4dri20Xs" role="3cqZAk">
                  <node concept="1PxgMI" id="3yJ4dri20Rc" role="2Oq$k0">
                    <ref role="1PxNhF" to="c9eo:3yJ4dri0HDh" resolve="IfStatement" />
                    <node concept="2OqwBi" id="3yJ4dri20$x" role="1PxMeX">
                      <node concept="Cj7Ep" id="3yJ4dri20xT" role="2Oq$k0" />
                      <node concept="1mfA1w" id="3yJ4dri20H7" role="2OqNvi" />
                    </node>
                  </node>
                  <node concept="3TrEf2" id="3yJ4dri21jb" role="2OqNvi">
                    <ref role="3Tt5mk" to="c9eo:3yJ4dri0Ss0" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="3yJ4dri1Zbc" role="Cn2iK">
            <property role="2h1i$Z" value="else" />
          </node>
        </node>
      </node>
      <node concept="tYCnQ" id="3yJ4dri2zYH" role="_1QTJ">
        <ref role="uz4UX" to="c9eo:3yJ4dri1cHG" resolve="ElseIfClause" />
        <node concept="Cmt7Y" id="3yJ4dri2$41" role="uz6Si">
          <node concept="Cnhdc" id="3yJ4dri2$42" role="Cncma">
            <node concept="3clFbS" id="3yJ4dri2$43" role="2VODD2">
              <node concept="3cpWs8" id="3yJ4dri2$5a" role="3cqZAp">
                <node concept="3cpWsn" id="3yJ4dri2$5d" role="3cpWs9">
                  <property role="TrG5h" value="elseIf" />
                  <node concept="3Tqbb2" id="3yJ4dri2$5e" role="1tU5fm">
                    <ref role="ehGHo" to="c9eo:3yJ4dri1cHG" resolve="ElseIfClause" />
                  </node>
                  <node concept="2ShNRf" id="3yJ4dri2$F2" role="33vP2m">
                    <node concept="2fJWfE" id="3yJ4dri2$Jr" role="2ShVmc">
                      <node concept="3Tqbb2" id="3yJ4dri2$Jt" role="3zrR0E">
                        <ref role="ehGHo" to="c9eo:3yJ4dri1cHG" resolve="ElseIfClause" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="3yJ4dri2$8_" role="3cqZAp">
                <node concept="2OqwBi" id="3yJ4dri2$by" role="3clFbG">
                  <node concept="Cj7Ep" id="3yJ4dri2$8z" role="2Oq$k0" />
                  <node concept="HtI8k" id="3yJ4dri2$ko" role="2OqNvi">
                    <node concept="37vLTw" id="3yJ4dri2$mf" role="HtI8F">
                      <ref role="3cqZAo" node="3yJ4dri2$5d" resolve="elseIf" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="3yJ4dri2$pt" role="3cqZAp">
                <node concept="37vLTw" id="3yJ4dri2$r$" role="3cqZAk">
                  <ref role="3cqZAo" node="3yJ4dri2$5d" resolve="elseIf" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="3yJ4dri2$4G" role="Cn2iK">
            <property role="2h1i$Z" value="elif" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UNGvq" id="3yJ4dri28ZS" role="3UOs0v">
      <property role="2uHTBK" value="ext_1_RTransform" />
      <property role="3mWRNi" value="add else-if statement to if" />
      <ref role="3UNGvu" to="c9eo:3yJ4dri0HDh" resolve="IfStatement" />
      <node concept="tYCnQ" id="3yJ4dri290E" role="_1QTJ">
        <ref role="uz4UX" to="c9eo:3yJ4dri0HDh" resolve="IfStatement" />
        <node concept="Cmt7Y" id="3yJ4dri290J" role="uz6Si">
          <node concept="Cnhdc" id="3yJ4dri290L" role="Cncma">
            <node concept="3clFbS" id="3yJ4dri290N" role="2VODD2">
              <node concept="3cpWs8" id="3yJ4dri291w" role="3cqZAp">
                <node concept="3cpWsn" id="3yJ4dri291z" role="3cpWs9">
                  <property role="TrG5h" value="elseIf" />
                  <node concept="3Tqbb2" id="3yJ4dri291v" role="1tU5fm">
                    <ref role="ehGHo" to="c9eo:3yJ4dri1cHG" resolve="ElseIfClause" />
                  </node>
                  <node concept="2OqwBi" id="3yJ4dri2a8R" role="33vP2m">
                    <node concept="2OqwBi" id="3yJ4dri2978" role="2Oq$k0">
                      <node concept="Cj7Ep" id="3yJ4dri294B" role="2Oq$k0" />
                      <node concept="3Tsc0h" id="3yJ4dri29pL" role="2OqNvi">
                        <ref role="3TtcxE" to="c9eo:3yJ4dri1fJ0" />
                      </node>
                    </node>
                    <node concept="2DeJg1" id="3yJ4dri2bja" role="2OqNvi" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="3yJ4dri2blH" role="3cqZAp">
                <node concept="37vLTw" id="3yJ4dri2boo" role="3cqZAk">
                  <ref role="3cqZAo" node="3yJ4dri291z" resolve="elseIf" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="3yJ4dri291t" role="Cn2iK">
            <property role="2h1i$Z" value="elif" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="1aS1l$pmm3">
    <property role="3GE5qa" value="loops" />
    <property role="TrG5h" value="doLoopTransformations" />
    <node concept="3UNGvq" id="uPhVC8_wO4" role="3UOs0v">
      <ref role="3UNGvu" to="2gyk:2zxr1HVkFW8" resolve="TimeloopStatement" />
      <node concept="tYCnQ" id="uPhVC8_wOo" role="_1QTJ">
        <ref role="uz4UX" to="2gyk:2zxr1HVkFW8" resolve="TimeloopStatement" />
        <node concept="Cmt7Y" id="uPhVC8_wOt" role="uz6Si">
          <node concept="Cnhdc" id="uPhVC8_wOv" role="Cncma">
            <node concept="3clFbS" id="uPhVC8_wOx" role="2VODD2">
              <node concept="3clFbF" id="uPhVC8_wPu" role="3cqZAp">
                <node concept="2OqwBi" id="uPhVC8_xjb" role="3clFbG">
                  <node concept="2OqwBi" id="uPhVC8_wSc" role="2Oq$k0">
                    <node concept="Cj7Ep" id="uPhVC8_wPt" role="2Oq$k0" />
                    <node concept="3TrEf2" id="uPhVC8_x58" role="2OqNvi">
                      <ref role="3Tt5mk" to="2gyk:2zxr1HVkFW9" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="uPhVC8_xyJ" role="2OqNvi" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="uPhVC8_wPb" role="Cn2iK">
            <property role="2h1i$Z" value="by" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

