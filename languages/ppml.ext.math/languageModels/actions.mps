<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:aa75d329-dc54-4281-a035-e97ad35dd7bd(de.ppme.core.actions)">
  <persistence version="9" />
  <languages>
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="0" />
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="0" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" />
    <import index="2gyk" ref="r:be57a480-399b-470c-860e-bdfcf98e0b2c(de.ppme.core.structure)" />
    <import index="c9eo" ref="r:493484be-3d66-4fd8-b261-ed47cd8c6981(de.ppme.statements.structure)" />
  </imports>
  <registry>
    <language id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts">
      <concept id="1161622665029" name="jetbrains.mps.lang.sharedConcepts.structure.ConceptFunctionParameter_model" flags="nn" index="1Q6Npb" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1176544042499" name="jetbrains.mps.lang.typesystem.structure.Node_TypeOperation" flags="nn" index="3JvlWi" />
      <concept id="1178870617262" name="jetbrains.mps.lang.typesystem.structure.CoerceExpression" flags="nn" index="1UaxmW">
        <child id="1178870894644" name="pattern" index="1Ub_4A" />
        <child id="1178870894645" name="nodeToCoerce" index="1Ub_4B" />
      </concept>
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
    </language>
    <language id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions">
      <concept id="1196433923911" name="jetbrains.mps.lang.actions.structure.SideTransform_SimpleString" flags="nn" index="2h1dTh">
        <property id="1196433942569" name="text" index="2h1i$Z" />
      </concept>
      <concept id="1177323996388" name="jetbrains.mps.lang.actions.structure.AddMenuPart" flags="ng" index="tYCnQ" />
      <concept id="1177333529597" name="jetbrains.mps.lang.actions.structure.ConceptPart" flags="ng" index="uyZFJ">
        <reference id="1177333551023" name="concept" index="uz4UX" />
        <child id="1177333559040" name="part" index="uz6Si" />
      </concept>
      <concept id="1177497140107" name="jetbrains.mps.lang.actions.structure.ConceptFunctionParameter_sourceNode" flags="nn" index="Cj7Ep" />
      <concept id="1177498013932" name="jetbrains.mps.lang.actions.structure.SimpleSideTransformMenuPart" flags="ng" index="Cmt7Y">
        <child id="1177498166690" name="matchingText" index="Cn2iK" />
        <child id="1177498182537" name="descriptionText" index="Cn6ar" />
        <child id="1177498207384" name="handler" index="Cncma" />
      </concept>
      <concept id="1177498227294" name="jetbrains.mps.lang.actions.structure.QueryFunction_SideTransform_Handler" flags="in" index="Cnhdc" />
      <concept id="767145758118872833" name="jetbrains.mps.lang.actions.structure.NF_LinkList_AddNewChildOperation" flags="nn" index="2DeJg1" />
      <concept id="767145758118872828" name="jetbrains.mps.lang.actions.structure.NF_Node_ReplaceWithNewOperation" flags="nn" index="2DeJnW" />
      <concept id="767145758118872830" name="jetbrains.mps.lang.actions.structure.NF_Link_SetNewChildOperation" flags="nn" index="2DeJnY" />
      <concept id="1180111159572" name="jetbrains.mps.lang.actions.structure.IncludeRightTransformForNodePart" flags="ng" index="346O06">
        <child id="1180111489972" name="nodeBlock" index="3484EA" />
      </concept>
      <concept id="5480835971642155304" name="jetbrains.mps.lang.actions.structure.NF_Model_CreateNewNodeOperation" flags="nn" index="15TzpJ" />
      <concept id="1158700664498" name="jetbrains.mps.lang.actions.structure.NodeFactories" flags="ng" index="37WguZ">
        <child id="1158700779049" name="nodeFactory" index="37WGs$" />
      </concept>
      <concept id="1158700725281" name="jetbrains.mps.lang.actions.structure.NodeFactory" flags="ig" index="37WvkG">
        <reference id="1158700943156" name="applicableConcept" index="37XkoT" />
        <child id="1158701448518" name="setupFunction" index="37ZfLb" />
      </concept>
      <concept id="1158701162220" name="jetbrains.mps.lang.actions.structure.NodeSetupFunction" flags="in" index="37Y9Zx" />
      <concept id="1154622616118" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstitutePreconditionFunction" flags="in" index="3kRJcU" />
      <concept id="5584396657084912703" name="jetbrains.mps.lang.actions.structure.NodeSetupFunction_NewNode" flags="nn" index="1r4Lsj" />
      <concept id="1178537049112" name="jetbrains.mps.lang.actions.structure.QueryFunction_SideTransform_NodeQuery" flags="in" index="1Ai3Oa" />
      <concept id="1138079221458" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstituteActionsBuilder" flags="ig" index="3UNGvq">
        <property id="1158952484319" name="description" index="3mWRNi" />
        <reference id="1138079221462" name="applicableConcept" index="3UNGvu" />
        <child id="1177442283389" name="part" index="_1QTJ" />
        <child id="1154622757656" name="precondition" index="3kShCk" />
      </concept>
      <concept id="1138079416598" name="jetbrains.mps.lang.actions.structure.SideTransformHintSubstituteActions" flags="ng" index="3UOs0u">
        <child id="1138079416599" name="actionsBuilder" index="3UOs0v" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1140725362528" name="jetbrains.mps.lang.smodel.structure.Link_SetTargetOperation" flags="nn" index="2oxUTD">
        <child id="1140725362529" name="linkTarget" index="2oxUTC" />
      </concept>
      <concept id="1138757581985" name="jetbrains.mps.lang.smodel.structure.Link_SetNewChildOperation" flags="nn" index="zfrQC">
        <reference id="1139880128956" name="concept" index="1A9B2P" />
      </concept>
      <concept id="1143235216708" name="jetbrains.mps.lang.smodel.structure.Model_CreateNewNodeOperation" flags="nn" index="I8ghe">
        <reference id="1143235391024" name="concept" index="I8UWU" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1144146199828" name="jetbrains.mps.lang.smodel.structure.Node_CopyOperation" flags="nn" index="1$rogu" />
      <concept id="1139867745658" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithNewOperation" flags="nn" index="1_qnLN">
        <reference id="1139867957129" name="concept" index="1_rbq0" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <property id="1193676396447" name="virtualPackage" index="3GE5qa" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1165530316231" name="jetbrains.mps.baseLanguage.collections.structure.IsEmptyOperation" flags="nn" index="1v1jN8" />
    </language>
  </registry>
  <node concept="3UOs0u" id="2Jc2aEYgxm3">
    <property role="TrG5h" value="sta_Power" />
    <node concept="3UNGvq" id="2Jc2aEYgxm4" role="3UOs0v">
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      <node concept="tYCnQ" id="2Jc2aEYgxnv" role="_1QTJ">
        <ref role="uz4UX" to="2gyk:N$_yc$D_Xb" resolve="PowerExpression" />
        <node concept="Cmt7Y" id="2Jc2aEYgxnx" role="uz6Si">
          <node concept="Cnhdc" id="2Jc2aEYgxny" role="Cncma">
            <node concept="3clFbS" id="2Jc2aEYgxnz" role="2VODD2">
              <node concept="3cpWs8" id="2Jc2aEYgAts" role="3cqZAp">
                <node concept="3cpWsn" id="2Jc2aEYgAtv" role="3cpWs9">
                  <property role="TrG5h" value="power" />
                  <node concept="3Tqbb2" id="2Jc2aEYgAtr" role="1tU5fm">
                    <ref role="ehGHo" to="2gyk:N$_yc$D_Xb" resolve="PowerExpression" />
                  </node>
                  <node concept="2OqwBi" id="2Jc2aEYgAQg" role="33vP2m">
                    <node concept="Cj7Ep" id="2Jc2aEYgAw2" role="2Oq$k0" />
                    <node concept="1_qnLN" id="2Jc2aEYgTWd" role="2OqNvi">
                      <ref role="1_rbq0" to="2gyk:N$_yc$D_Xb" resolve="PowerExpression" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2Jc2aEYgTZh" role="3cqZAp">
                <node concept="2OqwBi" id="2Jc2aEYgUlt" role="3clFbG">
                  <node concept="2OqwBi" id="2Jc2aEYgU1C" role="2Oq$k0">
                    <node concept="37vLTw" id="2Jc2aEYgTZf" role="2Oq$k0">
                      <ref role="3cqZAo" node="2Jc2aEYgAtv" resolve="power" />
                    </node>
                    <node concept="3TrEf2" id="5J3$v5miDjQ" role="2OqNvi">
                      <ref role="3Tt5mk" to="pfd6:5l83jlMf$RD" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="2Jc2aEYgU_a" role="2OqNvi">
                    <node concept="Cj7Ep" id="2Jc2aEYgUBm" role="2oxUTC" />
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2Jc2aEYgUFP" role="3cqZAp">
                <node concept="2OqwBi" id="2Jc2aEYisCK" role="3clFbG">
                  <node concept="37vLTw" id="2Jc2aEYgUFN" role="2Oq$k0">
                    <ref role="3cqZAo" node="2Jc2aEYgAtv" resolve="power" />
                  </node>
                  <node concept="3TrEf2" id="5J3$v5miDAf" role="2OqNvi">
                    <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="2Jc2aEYgxVJ" role="Cn2iK">
            <property role="2h1i$Z" value="pow" />
          </node>
          <node concept="2h1dTh" id="2Jc2aEYgxW1" role="Cn6ar">
            <property role="2h1i$Z" value="power" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UNGvq" id="2Jc2aEYiurT" role="3UOs0v">
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      <node concept="tYCnQ" id="2Jc2aEYiux4" role="_1QTJ">
        <ref role="uz4UX" to="2gyk:N$_yc$D_Xb" resolve="PowerExpression" />
        <node concept="Cmt7Y" id="2Jc2aEYiux7" role="uz6Si">
          <node concept="2h1dTh" id="2Jc2aEYiuxP" role="Cn2iK">
            <property role="2h1i$Z" value="**" />
          </node>
          <node concept="2h1dTh" id="2Jc2aEYiuy8" role="Cn6ar">
            <property role="2h1i$Z" value="power" />
          </node>
          <node concept="Cnhdc" id="2Jc2aEYiwLb" role="Cncma">
            <node concept="3clFbS" id="2Jc2aEYiwLc" role="2VODD2">
              <node concept="3cpWs8" id="2Jc2aEYiwLd" role="3cqZAp">
                <node concept="3cpWsn" id="2Jc2aEYiwLe" role="3cpWs9">
                  <property role="TrG5h" value="power" />
                  <node concept="3Tqbb2" id="2Jc2aEYiwLf" role="1tU5fm">
                    <ref role="ehGHo" to="2gyk:N$_yc$D_Xb" resolve="PowerExpression" />
                  </node>
                  <node concept="2OqwBi" id="2Jc2aEYiwLg" role="33vP2m">
                    <node concept="Cj7Ep" id="2Jc2aEYiwLh" role="2Oq$k0" />
                    <node concept="1_qnLN" id="2Jc2aEYiwLi" role="2OqNvi">
                      <ref role="1_rbq0" to="2gyk:N$_yc$D_Xb" resolve="PowerExpression" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2Jc2aEYiwLj" role="3cqZAp">
                <node concept="2OqwBi" id="2Jc2aEYiwLk" role="3clFbG">
                  <node concept="2OqwBi" id="2Jc2aEYiwLl" role="2Oq$k0">
                    <node concept="37vLTw" id="2Jc2aEYiwLm" role="2Oq$k0">
                      <ref role="3cqZAo" node="2Jc2aEYiwLe" resolve="power" />
                    </node>
                    <node concept="3TrEf2" id="5J3$v5miE92" role="2OqNvi">
                      <ref role="3Tt5mk" to="pfd6:5l83jlMf$RD" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="2Jc2aEYiwLo" role="2OqNvi">
                    <node concept="Cj7Ep" id="2Jc2aEYiwLp" role="2oxUTC" />
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="2Jc2aEYiwLq" role="3cqZAp">
                <node concept="2OqwBi" id="2Jc2aEYiwLr" role="3clFbG">
                  <node concept="37vLTw" id="2Jc2aEYiwLs" role="2Oq$k0">
                    <ref role="3cqZAo" node="2Jc2aEYiwLe" resolve="power" />
                  </node>
                  <node concept="3TrEf2" id="5J3$v5miEne" role="2OqNvi">
                    <ref role="3Tt5mk" to="pfd6:5l83jlMf$RF" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="5gQ2EqXOKf2">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="RandomNumberExpressionTransformations" />
    <node concept="3UNGvq" id="5gQ2EqXOKf3" role="3UOs0v">
      <ref role="3UNGvu" to="2gyk:5gQ2EqXOKaO" resolve="RandomNumberExpression" />
      <node concept="tYCnQ" id="5gQ2EqXOKf4" role="_1QTJ">
        <ref role="uz4UX" to="2gyk:5gQ2EqXOKaO" resolve="RandomNumberExpression" />
        <node concept="Cmt7Y" id="5gQ2EqXOKf5" role="uz6Si">
          <node concept="Cnhdc" id="5gQ2EqXOKf6" role="Cncma">
            <node concept="3clFbS" id="5gQ2EqXOKf7" role="2VODD2">
              <node concept="3cpWs8" id="5gQ2EqXOKf8" role="3cqZAp">
                <node concept="3cpWsn" id="5gQ2EqXOKf9" role="3cpWs9">
                  <property role="TrG5h" value="result" />
                  <node concept="3Tqbb2" id="5gQ2EqXOKfa" role="1tU5fm">
                    <ref role="ehGHo" to="2gyk:5gQ2EqXOKaO" resolve="RandomNumberExpression" />
                  </node>
                  <node concept="2OqwBi" id="5gQ2EqXOKfb" role="33vP2m">
                    <node concept="1Q6Npb" id="5gQ2EqXOKfc" role="2Oq$k0" />
                    <node concept="15TzpJ" id="5gQ2EqXOKfd" role="2OqNvi">
                      <ref role="I8UWU" to="2gyk:5gQ2EqXOKaO" resolve="RandomNumberExpression" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="5gQ2EqXOKfe" role="3cqZAp">
                <node concept="2OqwBi" id="5gQ2EqXOKff" role="3clFbG">
                  <node concept="Cj7Ep" id="5gQ2EqXOKfg" role="2Oq$k0" />
                  <node concept="1P9Npp" id="5gQ2EqXOKfh" role="2OqNvi">
                    <node concept="37vLTw" id="5gQ2EqXOKfi" role="1P9ThW">
                      <ref role="3cqZAo" node="5gQ2EqXOKf9" resolve="result" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="5gQ2EqXOKfj" role="3cqZAp">
                <node concept="2OqwBi" id="5gQ2EqXOKfk" role="3clFbG">
                  <node concept="2OqwBi" id="5gQ2EqXOKfl" role="2Oq$k0">
                    <node concept="37vLTw" id="5gQ2EqXOKfm" role="2Oq$k0">
                      <ref role="3cqZAo" node="5gQ2EqXOKf9" resolve="result" />
                    </node>
                    <node concept="3TrEf2" id="5gQ2EqXOKfn" role="2OqNvi">
                      <ref role="3Tt5mk" to="2gyk:5gQ2EqXOKaP" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="5gQ2EqXOKfo" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbF" id="5gQ2EqXOKfp" role="3cqZAp">
                <node concept="2OqwBi" id="5gQ2EqXOKfq" role="3clFbG">
                  <node concept="37vLTw" id="5gQ2EqXOKfr" role="2Oq$k0">
                    <ref role="3cqZAo" node="5gQ2EqXOKf9" resolve="result" />
                  </node>
                  <node concept="3TrEf2" id="5gQ2EqXOKfs" role="2OqNvi">
                    <ref role="3Tt5mk" to="2gyk:5gQ2EqXOKaP" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="5gQ2EqXOKft" role="Cn2iK">
            <property role="2h1i$Z" value="&lt;&gt;" />
          </node>
          <node concept="2h1dTh" id="5gQ2EqXOKfu" role="Cn6ar">
            <property role="2h1i$Z" value="specify random type" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="5gQ2EqXTRiZ">
    <property role="TrG5h" value="initializeStatementTransformations" />
    <node concept="3UNGvq" id="5gQ2EqXTRj0" role="3UOs0v">
      <ref role="3UNGvu" to="2gyk:5gQ2EqXTRdD" resolve="InitializeStatement" />
      <node concept="3kRJcU" id="5gQ2EqXTRj1" role="3kShCk">
        <node concept="3clFbS" id="5gQ2EqXTRj2" role="2VODD2">
          <node concept="3clFbF" id="5gQ2EqXTRj3" role="3cqZAp">
            <node concept="2OqwBi" id="5gQ2EqXTRj4" role="3clFbG">
              <node concept="2OqwBi" id="5gQ2EqXTRj5" role="2Oq$k0">
                <node concept="2OqwBi" id="5gQ2EqXTRj6" role="2Oq$k0">
                  <node concept="Cj7Ep" id="5gQ2EqXTRj7" role="2Oq$k0" />
                  <node concept="3TrEf2" id="5gQ2EqXTRj8" role="2OqNvi">
                    <ref role="3Tt5mk" to="2gyk:5gQ2EqXTRdE" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="5gQ2EqXTRj9" role="2OqNvi">
                  <ref role="3TtcxE" to="c9eo:5l83jlMhgFJ" />
                </node>
              </node>
              <node concept="1v1jN8" id="5gQ2EqXTRja" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
      <node concept="tYCnQ" id="5gQ2EqXTRjb" role="_1QTJ">
        <ref role="uz4UX" to="2gyk:5gQ2EqXTRdD" resolve="InitializeStatement" />
        <node concept="Cmt7Y" id="5gQ2EqXTRjc" role="uz6Si">
          <node concept="Cnhdc" id="5gQ2EqXTRjd" role="Cncma">
            <node concept="3clFbS" id="5gQ2EqXTRje" role="2VODD2">
              <node concept="3clFbF" id="5gQ2EqXTRjf" role="3cqZAp">
                <node concept="2OqwBi" id="5gQ2EqXTRjg" role="3clFbG">
                  <node concept="2OqwBi" id="5gQ2EqXTRjh" role="2Oq$k0">
                    <node concept="Cj7Ep" id="5gQ2EqXTRji" role="2Oq$k0" />
                    <node concept="3TrEf2" id="5gQ2EqXTRjj" role="2OqNvi">
                      <ref role="3Tt5mk" to="2gyk:5gQ2EqXTRdE" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="5gQ2EqXTRjk" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbF" id="5gQ2EqXTRjl" role="3cqZAp">
                <node concept="2OqwBi" id="5gQ2EqXTRjm" role="3clFbG">
                  <node concept="2OqwBi" id="5gQ2EqXTRjn" role="2Oq$k0">
                    <node concept="2OqwBi" id="5gQ2EqXTRjo" role="2Oq$k0">
                      <node concept="Cj7Ep" id="5gQ2EqXTRjp" role="2Oq$k0" />
                      <node concept="3TrEf2" id="5gQ2EqXTRjq" role="2OqNvi">
                        <ref role="3Tt5mk" to="2gyk:5gQ2EqXTRdE" />
                      </node>
                    </node>
                    <node concept="3Tsc0h" id="5gQ2EqXTRjr" role="2OqNvi">
                      <ref role="3TtcxE" to="c9eo:5l83jlMhgFJ" />
                    </node>
                  </node>
                  <node concept="2DeJg1" id="5gQ2EqXTRjs" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbF" id="5gQ2EqXTRjt" role="3cqZAp">
                <node concept="2OqwBi" id="5gQ2EqXTRju" role="3clFbG">
                  <node concept="Cj7Ep" id="5gQ2EqXTRjv" role="2Oq$k0" />
                  <node concept="3TrEf2" id="5gQ2EqXTRjw" role="2OqNvi">
                    <ref role="3Tt5mk" to="2gyk:5gQ2EqXTRdE" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="5gQ2EqXTRjx" role="Cn2iK">
            <property role="2h1i$Z" value=" with" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="5gQ2EqXYl6W">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="ParticleListAttributeActions" />
    <node concept="3UNGvq" id="5gQ2EqXYl6X" role="3UOs0v">
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      <node concept="tYCnQ" id="5gQ2EqXYlmQ" role="_1QTJ">
        <ref role="uz4UX" to="2gyk:2zxr1HVi7bZ" resolve="ParticleListAttributeExpression" />
        <node concept="Cmt7Y" id="5gQ2EqXYnjX" role="uz6Si">
          <node concept="Cnhdc" id="5gQ2EqXYnjY" role="Cncma">
            <node concept="3clFbS" id="5gQ2EqXYnjZ" role="2VODD2">
              <node concept="3cpWs8" id="5gQ2EqXYDed" role="3cqZAp">
                <node concept="3cpWsn" id="5gQ2EqXYDeg" role="3cpWs9">
                  <property role="TrG5h" value="plae" />
                  <node concept="3Tqbb2" id="5gQ2EqXYDeb" role="1tU5fm">
                    <ref role="ehGHo" to="2gyk:2zxr1HVi7bZ" resolve="ParticleListAttributeExpression" />
                  </node>
                  <node concept="2ShNRf" id="5gQ2EqXYDjL" role="33vP2m">
                    <node concept="3zrR0B" id="5gQ2EqXYDjo" role="2ShVmc">
                      <node concept="3Tqbb2" id="5gQ2EqXYDjp" role="3zrR0E">
                        <ref role="ehGHo" to="2gyk:2zxr1HVi7bZ" resolve="ParticleListAttributeExpression" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="5gQ2EqXYDma" role="3cqZAp">
                <node concept="2OqwBi" id="5gQ2EqXYDM9" role="3clFbG">
                  <node concept="2OqwBi" id="5gQ2EqXYDoN" role="2Oq$k0">
                    <node concept="37vLTw" id="5gQ2EqXYDm8" role="2Oq$k0">
                      <ref role="3cqZAo" node="5gQ2EqXYDeg" resolve="plae" />
                    </node>
                    <node concept="3TrEf2" id="5gQ2EqXYD$g" role="2OqNvi">
                      <ref role="3Tt5mk" to="2gyk:2zxr1HVi7c0" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="5gQ2EqXYE0j" role="2OqNvi">
                    <node concept="2OqwBi" id="5gQ2EqXYE4Y" role="2oxUTC">
                      <node concept="Cj7Ep" id="5gQ2EqXYE2h" role="2Oq$k0" />
                      <node concept="1$rogu" id="5gQ2EqXYEdm" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="5gQ2EqXYEhl" role="3cqZAp">
                <node concept="2OqwBi" id="5gQ2EqXYEkr" role="3clFbG">
                  <node concept="Cj7Ep" id="5gQ2EqXYEhj" role="2Oq$k0" />
                  <node concept="1P9Npp" id="5gQ2EqXYE$T" role="2OqNvi">
                    <node concept="37vLTw" id="5gQ2EqXYEBm" role="1P9ThW">
                      <ref role="3cqZAo" node="5gQ2EqXYDeg" resolve="plae" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="5gQ2EqXYnkC" role="Cn2iK">
            <property role="2h1i$Z" value="%" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="67Wx_o2YSLE">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="particleAccess" />
    <node concept="3UNGvq" id="67Wx_o2YSLF" role="3UOs0v">
      <property role="3mWRNi" value="transforms a particle into a particle property access expression" />
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      <node concept="tYCnQ" id="67Wx_o2YSLG" role="_1QTJ">
        <ref role="uz4UX" to="2gyk:5gQ2EqXOKaK" resolve="ParticleAccessExpression" />
        <node concept="Cmt7Y" id="67Wx_o2YSLH" role="uz6Si">
          <node concept="Cnhdc" id="67Wx_o2YSLI" role="Cncma">
            <node concept="3clFbS" id="67Wx_o2YSLJ" role="2VODD2">
              <node concept="3cpWs8" id="67Wx_o2YSLK" role="3cqZAp">
                <node concept="3cpWsn" id="67Wx_o2YSLL" role="3cpWs9">
                  <property role="TrG5h" value="result" />
                  <node concept="3Tqbb2" id="67Wx_o2YSLM" role="1tU5fm">
                    <ref role="ehGHo" to="2gyk:5gQ2EqXOKaK" resolve="ParticleAccessExpression" />
                  </node>
                  <node concept="2OqwBi" id="67Wx_o2YSLN" role="33vP2m">
                    <node concept="1Q6Npb" id="67Wx_o2YSLO" role="2Oq$k0" />
                    <node concept="15TzpJ" id="67Wx_o2YSLP" role="2OqNvi">
                      <ref role="I8UWU" to="2gyk:5gQ2EqXOKaK" resolve="ParticleAccessExpression" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="67Wx_o2YSLQ" role="3cqZAp">
                <node concept="2OqwBi" id="67Wx_o2YSLR" role="3clFbG">
                  <node concept="Cj7Ep" id="67Wx_o2YSLS" role="2Oq$k0" />
                  <node concept="1P9Npp" id="67Wx_o2YSLT" role="2OqNvi">
                    <node concept="37vLTw" id="67Wx_o2YSLU" role="1P9ThW">
                      <ref role="3cqZAo" node="67Wx_o2YSLL" resolve="result" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="67Wx_o2YSLV" role="3cqZAp">
                <node concept="2OqwBi" id="67Wx_o2YSLW" role="3clFbG">
                  <node concept="2OqwBi" id="67Wx_o2YSLX" role="2Oq$k0">
                    <node concept="37vLTw" id="67Wx_o2YSLY" role="2Oq$k0">
                      <ref role="3cqZAo" node="67Wx_o2YSLL" resolve="result" />
                    </node>
                    <node concept="3TrEf2" id="67Wx_o2YSLZ" role="2OqNvi">
                      <ref role="3Tt5mk" to="2gyk:5gQ2EqXOKaM" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="67Wx_o2YSM0" role="2OqNvi">
                    <node concept="Cj7Ep" id="67Wx_o2YSM1" role="2oxUTC" />
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="67Wx_o2YSM2" role="3cqZAp">
                <node concept="2OqwBi" id="67Wx_o2YSM3" role="3clFbG">
                  <node concept="2OqwBi" id="67Wx_o2YSM4" role="2Oq$k0">
                    <node concept="37vLTw" id="67Wx_o2YSM5" role="2Oq$k0">
                      <ref role="3cqZAo" node="67Wx_o2YSLL" resolve="result" />
                    </node>
                    <node concept="3TrEf2" id="67Wx_o2YSM6" role="2OqNvi">
                      <ref role="3Tt5mk" to="2gyk:5gQ2EqXOKaN" />
                    </node>
                  </node>
                  <node concept="2DeJnY" id="67Wx_o2YSM7" role="2OqNvi">
                    <ref role="1A9B2P" to="pfd6:6fgLCPsD6lb" resolve="StringLiteral" />
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="67Wx_o2YSM8" role="3cqZAp">
                <node concept="2OqwBi" id="67Wx_o2YSM9" role="3clFbG">
                  <node concept="37vLTw" id="67Wx_o2YSMa" role="2Oq$k0">
                    <ref role="3cqZAo" node="67Wx_o2YSLL" resolve="result" />
                  </node>
                  <node concept="3TrEf2" id="67Wx_o2YSMb" role="2OqNvi">
                    <ref role="3Tt5mk" to="2gyk:5gQ2EqXOKaN" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="67Wx_o2YSMc" role="Cn2iK">
            <property role="2h1i$Z" value="." />
          </node>
          <node concept="2h1dTh" id="67Wx_o2YSMd" role="Cn6ar">
            <property role="2h1i$Z" value="particle property access" />
          </node>
        </node>
      </node>
      <node concept="tYCnQ" id="67Wx_o2YSMe" role="_1QTJ">
        <ref role="uz4UX" to="2gyk:5gQ2EqXOKaK" resolve="ParticleAccessExpression" />
        <node concept="Cmt7Y" id="67Wx_o2YSMf" role="uz6Si">
          <node concept="Cnhdc" id="67Wx_o2YSMg" role="Cncma">
            <node concept="3clFbS" id="67Wx_o2YSMh" role="2VODD2">
              <node concept="3cpWs8" id="67Wx_o2YSMi" role="3cqZAp">
                <node concept="3cpWsn" id="67Wx_o2YSMj" role="3cpWs9">
                  <property role="TrG5h" value="result" />
                  <node concept="3Tqbb2" id="67Wx_o2YSMk" role="1tU5fm">
                    <ref role="ehGHo" to="2gyk:5gQ2EqXOKaK" resolve="ParticleAccessExpression" />
                  </node>
                  <node concept="2OqwBi" id="67Wx_o2YSMl" role="33vP2m">
                    <node concept="1Q6Npb" id="67Wx_o2YSMm" role="2Oq$k0" />
                    <node concept="15TzpJ" id="67Wx_o2YSMn" role="2OqNvi">
                      <ref role="I8UWU" to="2gyk:5gQ2EqXOKaK" resolve="ParticleAccessExpression" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="67Wx_o2YSMo" role="3cqZAp">
                <node concept="2OqwBi" id="67Wx_o2YSMp" role="3clFbG">
                  <node concept="Cj7Ep" id="67Wx_o2YSMq" role="2Oq$k0" />
                  <node concept="1P9Npp" id="67Wx_o2YSMr" role="2OqNvi">
                    <node concept="37vLTw" id="67Wx_o2YSMs" role="1P9ThW">
                      <ref role="3cqZAo" node="67Wx_o2YSMj" resolve="result" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="67Wx_o2YSMt" role="3cqZAp">
                <node concept="2OqwBi" id="67Wx_o2YSMu" role="3clFbG">
                  <node concept="2OqwBi" id="67Wx_o2YSMv" role="2Oq$k0">
                    <node concept="37vLTw" id="67Wx_o2YSMw" role="2Oq$k0">
                      <ref role="3cqZAo" node="67Wx_o2YSMj" resolve="result" />
                    </node>
                    <node concept="3TrEf2" id="67Wx_o2YSMx" role="2OqNvi">
                      <ref role="3Tt5mk" to="2gyk:5gQ2EqXOKaM" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="67Wx_o2YSMy" role="2OqNvi">
                    <node concept="Cj7Ep" id="67Wx_o2YSMz" role="2oxUTC" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="67Wx_o2YSM$" role="3cqZAp">
                <node concept="37vLTw" id="67Wx_o2YSM_" role="3cqZAk">
                  <ref role="3cqZAo" node="67Wx_o2YSMj" resolve="result" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="67Wx_o2YSMA" role="Cn2iK">
            <property role="2h1i$Z" value="[" />
          </node>
          <node concept="2h1dTh" id="67Wx_o2YSMB" role="Cn6ar">
            <property role="2h1i$Z" value="particle property access" />
          </node>
        </node>
      </node>
      <node concept="3kRJcU" id="67Wx_o2YSMC" role="3kShCk">
        <node concept="3clFbS" id="67Wx_o2YSMD" role="2VODD2">
          <node concept="3clFbF" id="67Wx_o2YSME" role="3cqZAp">
            <node concept="2OqwBi" id="67Wx_o2YSMF" role="3clFbG">
              <node concept="1UaxmW" id="67Wx_o2YSMG" role="2Oq$k0">
                <node concept="2OqwBi" id="67Wx_o2YSMH" role="1Ub_4B">
                  <node concept="Cj7Ep" id="67Wx_o2YSMI" role="2Oq$k0" />
                  <node concept="3JvlWi" id="67Wx_o2YSMJ" role="2OqNvi" />
                </node>
                <node concept="1YaCAy" id="67Wx_o2YSMK" role="1Ub_4A">
                  <property role="TrG5h" value="p" />
                  <ref role="1YaFvo" to="2gyk:5gQ2EqXQH6v" resolve="ParticleType" />
                </node>
              </node>
              <node concept="3x8VRR" id="67Wx_o2YSML" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="m1E9k9f1ae">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="transform_arrowExpression" />
    <node concept="3UNGvq" id="m1E9k9f1af" role="3UOs0v">
      <ref role="3UNGvu" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
      <node concept="3kRJcU" id="m1E9k9f1u7" role="3kShCk">
        <node concept="3clFbS" id="m1E9k9f1u8" role="2VODD2">
          <node concept="3clFbF" id="m1E9k9f1B4" role="3cqZAp">
            <node concept="2OqwBi" id="m1E9k9f1ZG" role="3clFbG">
              <node concept="2OqwBi" id="m1E9k9f1F4" role="2Oq$k0">
                <node concept="Cj7Ep" id="m1E9k9f1B3" role="2Oq$k0" />
                <node concept="3JvlWi" id="m1E9k9f1OW" role="2OqNvi" />
              </node>
              <node concept="3x8VRR" id="m1E9k9f289" role="2OqNvi" />
            </node>
          </node>
        </node>
      </node>
      <node concept="tYCnQ" id="m1E9k9f2hR" role="_1QTJ">
        <ref role="uz4UX" to="2gyk:m1E9k9eONZ" resolve="ArrowExpression" />
        <node concept="Cmt7Y" id="m1E9k9f2wV" role="uz6Si">
          <node concept="Cnhdc" id="m1E9k9f2wW" role="Cncma">
            <node concept="3clFbS" id="m1E9k9f2wX" role="2VODD2">
              <node concept="3cpWs8" id="m1E9k9f2E0" role="3cqZAp">
                <node concept="3cpWsn" id="m1E9k9f2E3" role="3cpWs9">
                  <property role="TrG5h" value="expr" />
                  <node concept="3Tqbb2" id="m1E9k9f2DZ" role="1tU5fm">
                    <ref role="ehGHo" to="2gyk:m1E9k9eONZ" resolve="ArrowExpression" />
                  </node>
                  <node concept="2OqwBi" id="m1E9k9f2RG" role="33vP2m">
                    <node concept="Cj7Ep" id="m1E9k9f2PQ" role="2Oq$k0" />
                    <node concept="2DeJnW" id="m1E9k9f3b9" role="2OqNvi">
                      <ref role="1_rbq0" to="2gyk:m1E9k9eONZ" resolve="ArrowExpression" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="m1E9k9f3mL" role="3cqZAp">
                <node concept="2OqwBi" id="m1E9k9f3GP" role="3clFbG">
                  <node concept="2OqwBi" id="m1E9k9f3p8" role="2Oq$k0">
                    <node concept="37vLTw" id="m1E9k9f3mJ" role="2Oq$k0">
                      <ref role="3cqZAo" node="m1E9k9f2E3" resolve="expr" />
                    </node>
                    <node concept="3TrEf2" id="m1E9k9f3yp" role="2OqNvi">
                      <ref role="3Tt5mk" to="2gyk:m1E9k9eOTT" />
                    </node>
                  </node>
                  <node concept="2oxUTD" id="m1E9k9f3PA" role="2OqNvi">
                    <node concept="Cj7Ep" id="m1E9k9f44a" role="2oxUTC" />
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="26Us85XSI$2" role="3cqZAp">
                <node concept="2OqwBi" id="26Us85XSIXU" role="3clFbG">
                  <node concept="2OqwBi" id="26Us85XSIB2" role="2Oq$k0">
                    <node concept="37vLTw" id="26Us85XSI$0" role="2Oq$k0">
                      <ref role="3cqZAo" node="m1E9k9f2E3" resolve="expr" />
                    </node>
                    <node concept="3TrEf2" id="26Us85XSILS" role="2OqNvi">
                      <ref role="3Tt5mk" to="2gyk:m1E9k9eOTV" />
                    </node>
                  </node>
                  <node concept="zfrQC" id="26Us85XSJ71" role="2OqNvi" />
                </node>
              </node>
              <node concept="3clFbH" id="m1E9k9f46i" role="3cqZAp" />
              <node concept="3cpWs6" id="m1E9k9f4ev" role="3cqZAp">
                <node concept="37vLTw" id="m1E9k9f4iw" role="3cqZAk">
                  <ref role="3cqZAo" node="m1E9k9f2E3" resolve="expr" />
                </node>
              </node>
            </node>
          </node>
          <node concept="2h1dTh" id="m1E9k9f2_L" role="Cn2iK">
            <property role="2h1i$Z" value="-&gt;" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3UNGvq" id="m1E9k9f6l$" role="3UOs0v">
      <property role="3mWRNi" value=" same options as for containing arrow expression" />
      <ref role="3UNGvu" to="2gyk:m1E9k9ePok" resolve="IAccess" />
      <node concept="346O06" id="m1E9k9f6HL" role="_1QTJ">
        <node concept="1Ai3Oa" id="m1E9k9f6HM" role="3484EA">
          <node concept="3clFbS" id="m1E9k9f6HN" role="2VODD2">
            <node concept="3clFbF" id="m1E9k9f6MX" role="3cqZAp">
              <node concept="2OqwBi" id="m1E9k9f6Oy" role="3clFbG">
                <node concept="Cj7Ep" id="m1E9k9f6MW" role="2Oq$k0" />
                <node concept="1mfA1w" id="m1E9k9f6VQ" role="2OqNvi" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="3UOs0u" id="m1E9k9ibuG">
    <property role="3GE5qa" value="expr" />
    <property role="TrG5h" value="addIndexToPositionAccess" />
    <node concept="3UNGvq" id="m1E9k9ibuH" role="3UOs0v">
      <ref role="3UNGvu" to="2gyk:m1E9k9gn6i" resolve="PositionMemberAccess" />
    </node>
  </node>
  <node concept="37WguZ" id="6Wx7SFgapo$">
    <property role="3GE5qa" value="types" />
    <property role="TrG5h" value="ParticleListTypeFactory" />
    <node concept="37WvkG" id="6Wx7SFgapo_" role="37WGs$">
      <ref role="37XkoT" to="2gyk:6Wx7SFgabxi" resolve="ParticleListType" />
      <node concept="37Y9Zx" id="6Wx7SFgapKn" role="37ZfLb">
        <node concept="3clFbS" id="6Wx7SFgapKo" role="2VODD2">
          <node concept="3clFbF" id="6Wx7SFgauNs" role="3cqZAp">
            <node concept="2OqwBi" id="6Wx7SFgaFSA" role="3clFbG">
              <node concept="2OqwBi" id="6Wx7SFgaxp2" role="2Oq$k0">
                <node concept="1r4Lsj" id="6Wx7SFgauNr" role="2Oq$k0" />
                <node concept="3TrEf2" id="6Wx7SFgaF_U" role="2OqNvi">
                  <ref role="3Tt5mk" to="pfd6:3SvAy0XHWz0" />
                </node>
              </node>
              <node concept="zfrQC" id="6Wx7SFgaGbn" role="2OqNvi">
                <ref role="1A9B2P" to="2gyk:5gQ2EqXQH6v" resolve="ParticleType" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

