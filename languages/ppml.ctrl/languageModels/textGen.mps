<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:0dcd6089-415f-4779-a514-2e4a2f07b495(de.ppme.ctrl.textGen)">
  <persistence version="9" />
  <languages>
    <use id="b83431fe-5c8f-40bc-8a36-65e25f4dd253" name="jetbrains.mps.lang.textGen" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="lm2c" ref="r:10261a70-d383-49ff-b567-775cda9fba27(de.ppme.ctrl.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1225271408483" name="jetbrains.mps.baseLanguage.structure.IsNotEmptyOperation" flags="nn" index="17RvpY" />
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1163668896201" name="jetbrains.mps.baseLanguage.structure.TernaryOperatorExpression" flags="nn" index="3K4zz7">
        <child id="1163668914799" name="condition" index="3K4Cdx" />
        <child id="1163668922816" name="ifTrue" index="3K4E3e" />
        <child id="1163668934364" name="ifFalse" index="3K4GZi" />
      </concept>
    </language>
    <language id="b83431fe-5c8f-40bc-8a36-65e25f4dd253" name="jetbrains.mps.lang.textGen">
      <concept id="1237305208784" name="jetbrains.mps.lang.textGen.structure.NewLineAppendPart" flags="ng" index="l8MVK" />
      <concept id="1237305334312" name="jetbrains.mps.lang.textGen.structure.NodeAppendPart" flags="ng" index="l9hG8">
        <child id="1237305790512" name="value" index="lb14g" />
      </concept>
      <concept id="1237305557638" name="jetbrains.mps.lang.textGen.structure.ConstantStringAppendPart" flags="ng" index="la8eA">
        <property id="1237305576108" name="value" index="lacIc" />
      </concept>
      <concept id="1237306079178" name="jetbrains.mps.lang.textGen.structure.AppendOperation" flags="nn" index="lc7rE">
        <child id="1237306115446" name="part" index="lcghm" />
      </concept>
      <concept id="1233670071145" name="jetbrains.mps.lang.textGen.structure.ConceptTextGenDeclaration" flags="ig" index="WtQ9Q">
        <reference id="1233670257997" name="conceptDeclaration" index="WuzLi" />
        <child id="1233749296504" name="textGenBlock" index="11c4hB" />
      </concept>
      <concept id="1233748055915" name="jetbrains.mps.lang.textGen.structure.NodeParameter" flags="nn" index="117lpO" />
      <concept id="1233749247888" name="jetbrains.mps.lang.textGen.structure.GenerateTextDeclaration" flags="in" index="11bSqf" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
    </language>
  </registry>
  <node concept="WtQ9Q" id="3QxmN_LniOO">
    <ref role="WuzLi" to="lm2c:5z7tqBB6nSj" resolve="Ctrl" />
    <node concept="11bSqf" id="3QxmN_LniOP" role="11c4hB">
      <node concept="3clFbS" id="3QxmN_LniOQ" role="2VODD2">
        <node concept="lc7rE" id="Opj2YGrixs" role="3cqZAp">
          <node concept="la8eA" id="Opj2YGriBv" role="lcghm">
            <property role="lacIc" value="#-- CTRL ----------------------------------------" />
          </node>
          <node concept="l8MVK" id="Opj2YGtHKS" role="lcghm" />
        </node>
        <node concept="lc7rE" id="Opj2YGrlFl" role="3cqZAp">
          <node concept="la8eA" id="Opj2YGrlLt" role="lcghm">
            <property role="lacIc" value="# Generated from '" />
          </node>
          <node concept="l9hG8" id="Opj2YGrlMb" role="lcghm">
            <node concept="3K4zz7" id="Opj2YGtc97" role="lb14g">
              <node concept="2OqwBi" id="Opj2YGtcGL" role="3K4GZi">
                <node concept="117lpO" id="Opj2YGtcqW" role="2Oq$k0" />
                <node concept="3TrcHB" id="Opj2YGtcSB" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
              <node concept="2OqwBi" id="Opj2YGtce9" role="3K4E3e">
                <node concept="117lpO" id="Opj2YGtcb3" role="2Oq$k0" />
                <node concept="3TrcHB" id="Opj2YGtcpR" role="2OqNvi">
                  <ref role="3TsBF5" to="lm2c:Opj2YGta5e" resolve="origin" />
                </node>
              </node>
              <node concept="2OqwBi" id="Opj2YGtaSX" role="3K4Cdx">
                <node concept="2OqwBi" id="Opj2YGtas9" role="2Oq$k0">
                  <node concept="117lpO" id="Opj2YGtapG" role="2Oq$k0" />
                  <node concept="3TrcHB" id="Opj2YGtaBF" role="2OqNvi">
                    <ref role="3TsBF5" to="lm2c:Opj2YGta5e" resolve="origin" />
                  </node>
                </node>
                <node concept="17RvpY" id="Opj2YGtbAx" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="Opj2YGrAPz" role="lcghm">
            <property role="lacIc" value="'" />
          </node>
          <node concept="l8MVK" id="Opj2YGtHN8" role="lcghm" />
        </node>
        <node concept="lc7rE" id="Opj2YGrl$F" role="3cqZAp">
          <node concept="la8eA" id="Opj2YGrl$G" role="lcghm">
            <property role="lacIc" value="#------------------------------------------------" />
          </node>
          <node concept="l8MVK" id="Opj2YGtHQu" role="lcghm" />
        </node>
        <node concept="lc7rE" id="Opj2YGtIsz" role="3cqZAp">
          <node concept="l8MVK" id="Opj2YGtIzS" role="lcghm" />
        </node>
        <node concept="3clFbH" id="Opj2YGtMsG" role="3cqZAp" />
        <node concept="2Gpval" id="3QxmN_Lo9Zg" role="3cqZAp">
          <node concept="2GrKxI" id="3QxmN_Lo9Zh" role="2Gsz3X">
            <property role="TrG5h" value="p" />
          </node>
          <node concept="3clFbS" id="3QxmN_Lo9Zi" role="2LFqv$">
            <node concept="lc7rE" id="6IDtJdlg_zx" role="3cqZAp">
              <node concept="l9hG8" id="Opj2YGtMXa" role="lcghm">
                <node concept="2GrUjf" id="Opj2YGtMXU" role="lb14g">
                  <ref role="2Gs0qQ" node="3QxmN_Lo9Zh" resolve="p" />
                </node>
              </node>
              <node concept="l8MVK" id="Opj2YGtMYY" role="lcghm" />
            </node>
          </node>
          <node concept="2OqwBi" id="3QxmN_LoamD" role="2GsD0m">
            <node concept="117lpO" id="3QxmN_Lo9ZJ" role="2Oq$k0" />
            <node concept="3Tsc0h" id="3QxmN_Loku6" role="2OqNvi">
              <ref role="3TtcxE" to="lm2c:5z7tqBB6xAa" />
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="Opj2YGtKyI">
    <ref role="WuzLi" to="lm2c:5z7tqBB6nSJ" resolve="CtrlProperty" />
    <node concept="11bSqf" id="Opj2YGtKyJ" role="11c4hB">
      <node concept="3clFbS" id="Opj2YGtKyK" role="2VODD2">
        <node concept="lc7rE" id="Opj2YGtKz2" role="3cqZAp">
          <node concept="l9hG8" id="Opj2YGtKzg" role="lcghm">
            <node concept="2OqwBi" id="Opj2YGtKBe" role="lb14g">
              <node concept="117lpO" id="Opj2YGtKzW" role="2Oq$k0" />
              <node concept="3TrcHB" id="Opj2YGtKQD" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="Opj2YGtKSL" role="lcghm">
            <property role="lacIc" value=" = " />
          </node>
          <node concept="l9hG8" id="ON9GjniCPO" role="lcghm">
            <node concept="2OqwBi" id="ON9GjniDfn" role="lb14g">
              <node concept="117lpO" id="ON9GjniDbj" role="2Oq$k0" />
              <node concept="3TrEf2" id="ON9GjniDyk" role="2OqNvi">
                <ref role="3Tt5mk" to="lm2c:5z7tqBB6u1p" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="2NTMEjkWGa7">
    <ref role="WuzLi" to="lm2c:5mlvYxQWMvz" resolve="CtrlPropertyReference" />
    <node concept="11bSqf" id="2NTMEjkWGa8" role="11c4hB">
      <node concept="3clFbS" id="2NTMEjkWGa9" role="2VODD2">
        <node concept="lc7rE" id="2NTMEjkWGml" role="3cqZAp">
          <node concept="l9hG8" id="2NTMEjkWGmz" role="lcghm">
            <node concept="2OqwBi" id="2NTMEjkWGRU" role="lb14g">
              <node concept="2OqwBi" id="2NTMEjkWGp9" role="2Oq$k0">
                <node concept="117lpO" id="2NTMEjkWGnj" role="2Oq$k0" />
                <node concept="3TrEf2" id="2NTMEjkWGFQ" role="2OqNvi">
                  <ref role="3Tt5mk" to="lm2c:5mlvYxQWMv$" />
                </node>
              </node>
              <node concept="3TrcHB" id="2NTMEjkWH7y" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

