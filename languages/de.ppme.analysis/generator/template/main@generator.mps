<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:4549d4a6-6bcb-48b6-a27f-fde45ccde6db(de.ppme.analysis.generator.template.main@generator)">
  <persistence version="9" />
  <languages>
    <use id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core" version="-1" />
    <use id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator" version="-1" />
    <use id="d7706f63-9be2-479c-a3da-ae92af1e64d5" name="jetbrains.mps.lang.generator.generationContext" version="-1" />
    <use id="13ff48ad-ecdb-4625-9d4a-3a16f2228549" name="de.ppme.analysis" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="pfd6" ref="r:9e939139-f0d1-4eb5-9ac6-55348163d9b2(de.ppme.expressions.structure)" />
    <import index="c9eo" ref="r:493484be-3d66-4fd8-b261-ed47cd8c6981(de.ppme.statements.structure)" />
    <import index="2gyk" ref="r:be57a480-399b-470c-860e-bdfcf98e0b2c(de.ppme.core.structure)" />
    <import index="6fcb" ref="r:8d9fc76a-100f-4d44-8987-0a52314ba857(de.ppme.analysis.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts">
      <concept id="1161622665029" name="jetbrains.mps.lang.sharedConcepts.structure.ConceptFunctionParameter_model" flags="nn" index="1Q6Npb" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="1095416546421" name="jetbrains.mps.lang.generator.structure.MappingConfiguration" flags="ig" index="bUwia">
        <child id="1195502100749" name="preMappingScript" index="1puA0r" />
      </concept>
      <concept id="1195499912406" name="jetbrains.mps.lang.generator.structure.MappingScript" flags="lg" index="1pmfR0">
        <property id="1195595592106" name="scriptKind" index="1v3f2W" />
        <property id="1195595611951" name="modifiesModel" index="1v3jST" />
        <child id="1195501105008" name="codeBlock" index="1pqMTA" />
      </concept>
      <concept id="1195500722856" name="jetbrains.mps.lang.generator.structure.MappingScript_CodeBlock" flags="in" index="1pplIY" />
      <concept id="1195502151594" name="jetbrains.mps.lang.generator.structure.MappingScriptReference" flags="lg" index="1puMqW">
        <reference id="1195502167610" name="mappingScript" index="1puQsG" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1171323947159" name="jetbrains.mps.lang.smodel.structure.Model_NodesOperation" flags="nn" index="2SmgA7">
        <reference id="1171323947160" name="concept" index="2SmgA8" />
      </concept>
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1144146199828" name="jetbrains.mps.lang.smodel.structure.Node_CopyOperation" flags="nn" index="1$rogu" />
      <concept id="6407023681583036853" name="jetbrains.mps.lang.smodel.structure.NodeAttributeQualifier" flags="ng" index="3CFYIy">
        <reference id="6407023681583036854" name="attributeConcept" index="3CFYIx" />
      </concept>
      <concept id="6407023681583031218" name="jetbrains.mps.lang.smodel.structure.AttributeAccess" flags="nn" index="3CFZ6_">
        <child id="6407023681583036852" name="qualifier" index="3CFYIz" />
      </concept>
      <concept id="1140131837776" name="jetbrains.mps.lang.smodel.structure.Node_ReplaceWithAnotherOperation" flags="nn" index="1P9Npp">
        <child id="1140131861877" name="replacementNode" index="1P9ThW" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1153943597977" name="jetbrains.mps.baseLanguage.collections.structure.ForEachStatement" flags="nn" index="2Gpval">
        <child id="1153944400369" name="variable" index="2Gsz3X" />
        <child id="1153944424730" name="inputSequence" index="2GsD0m" />
      </concept>
      <concept id="1153944193378" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariable" flags="nr" index="2GrKxI" />
      <concept id="1153944233411" name="jetbrains.mps.baseLanguage.collections.structure.ForEachVariableReference" flags="nn" index="2GrUjf">
        <reference id="1153944258490" name="variable" index="2Gs0qQ" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="bUwia" id="aOl7efxQXn">
    <property role="TrG5h" value="main" />
    <node concept="1puMqW" id="4e730WeB0JE" role="1puA0r">
      <ref role="1puQsG" node="4e730WeAplp" resolve="HerbieOptimizations" />
    </node>
  </node>
  <node concept="1pmfR0" id="4e730WeAplp">
    <property role="TrG5h" value="HerbieOptimizations" />
    <property role="1v3f2W" value="pre_processing" />
    <property role="1v3jST" value="true" />
    <node concept="1pplIY" id="4e730WeAplq" role="1pqMTA">
      <node concept="3clFbS" id="4e730WeAplr" role="2VODD2">
        <node concept="2Gpval" id="4e730WeB6kl" role="3cqZAp">
          <node concept="2GrKxI" id="4e730WeB6km" role="2Gsz3X">
            <property role="TrG5h" value="expr" />
          </node>
          <node concept="3clFbS" id="4e730WeB6kn" role="2LFqv$">
            <node concept="3clFbJ" id="4e730WeBjHw" role="3cqZAp">
              <node concept="3clFbS" id="4e730WeBjHy" role="3clFbx">
                <node concept="3cpWs8" id="4e730WeCAhr" role="3cqZAp">
                  <node concept="3cpWsn" id="4e730WeCAhx" role="3cpWs9">
                    <property role="TrG5h" value="parentStmnt" />
                    <node concept="3Tqbb2" id="4e730WeCAme" role="1tU5fm">
                      <ref role="ehGHo" to="c9eo:5l83jlMhgCt" resolve="Statement" />
                    </node>
                    <node concept="2OqwBi" id="4e730WeCAuq" role="33vP2m">
                      <node concept="2GrUjf" id="4e730WeCArh" role="2Oq$k0">
                        <ref role="2Gs0qQ" node="4e730WeB6km" resolve="expr" />
                      </node>
                      <node concept="2Xjw5R" id="4e730WeCAPT" role="2OqNvi">
                        <node concept="1xMEDy" id="4e730WeCAPV" role="1xVPHs">
                          <node concept="chp4Y" id="4e730WeCAQm" role="ri$Ld">
                            <ref role="cht4Q" to="c9eo:5l83jlMhh0E" resolve="ExpressionStatement" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbJ" id="4e730WeCAWe" role="3cqZAp">
                  <node concept="3clFbS" id="4e730WeCAWg" role="3clFbx">
                    <node concept="34ab3g" id="4e730WeCrXx" role="3cqZAp">
                      <property role="35gtTG" value="info" />
                      <node concept="Xl_RD" id="4e730WeCrXz" role="34bqiv">
                        <property role="Xl_RC" value="[Herbie Generator] replacing expression with optimized regime ..." />
                      </node>
                    </node>
                    <node concept="3clFbF" id="4e730WeBisN" role="3cqZAp">
                      <node concept="2OqwBi" id="4e730WeBivh" role="3clFbG">
                        <node concept="37vLTw" id="4e730WeCBtx" role="2Oq$k0">
                          <ref role="3cqZAo" node="4e730WeCAhx" resolve="parentStmnt" />
                        </node>
                        <node concept="1P9Npp" id="4e730WeBiOX" role="2OqNvi">
                          <node concept="2OqwBi" id="4e730WeBjqD" role="1P9ThW">
                            <node concept="2OqwBi" id="4e730WeBiS0" role="2Oq$k0">
                              <node concept="2GrUjf" id="4e730WeBmrr" role="2Oq$k0">
                                <ref role="2Gs0qQ" node="4e730WeB6km" resolve="expr" />
                              </node>
                              <node concept="3CFZ6_" id="4e730WeBjdR" role="2OqNvi">
                                <node concept="3CFYIy" id="4e730WeBjiD" role="3CFYIz">
                                  <ref role="3CFYIx" to="6fcb:aOl7efxWUh" resolve="HerbieAnnotation" />
                                </node>
                              </node>
                            </node>
                            <node concept="3TrEf2" id="4e730WeBjDh" role="2OqNvi">
                              <ref role="3Tt5mk" to="6fcb:1P5nnDyLH4t" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="2OqwBi" id="4e730WeCB3l" role="3clFbw">
                    <node concept="37vLTw" id="4e730WeCB1x" role="2Oq$k0">
                      <ref role="3cqZAo" node="4e730WeCAhx" resolve="parentStmnt" />
                    </node>
                    <node concept="3x8VRR" id="4e730WeCBcd" role="2OqNvi" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="4e730WeBl3E" role="3clFbw">
                <node concept="2OqwBi" id="4e730WeBkhd" role="2Oq$k0">
                  <node concept="2OqwBi" id="4e730WeBjKY" role="2Oq$k0">
                    <node concept="2GrUjf" id="4e730WeBmhz" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="4e730WeB6km" resolve="expr" />
                    </node>
                    <node concept="3CFZ6_" id="4e730WeBk7E" role="2OqNvi">
                      <node concept="3CFYIy" id="4e730WeBkay" role="3CFYIz">
                        <ref role="3CFYIx" to="6fcb:aOl7efxWUh" resolve="HerbieAnnotation" />
                      </node>
                    </node>
                  </node>
                  <node concept="3TrEf2" id="4e730WeBkF5" role="2OqNvi">
                    <ref role="3Tt5mk" to="6fcb:1P5nnDyLH4t" />
                  </node>
                </node>
                <node concept="3x8VRR" id="4e730WeBl_7" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="4e730WeBfaA" role="2GsD0m">
            <node concept="2OqwBi" id="4e730WeB6DR" role="2Oq$k0">
              <node concept="1Q6Npb" id="4e730WeB6kJ" role="2Oq$k0" />
              <node concept="2SmgA7" id="4e730WeB6KL" role="2OqNvi">
                <ref role="2SmgA8" to="pfd6:5l83jlMf$Lu" resolve="AssignmentExpression" />
              </node>
            </node>
            <node concept="3zZkjj" id="4e730WeBhCy" role="2OqNvi">
              <node concept="1bVj0M" id="4e730WeBhC$" role="23t8la">
                <node concept="3clFbS" id="4e730WeBhC_" role="1bW5cS">
                  <node concept="3clFbF" id="4e730WeBhFa" role="3cqZAp">
                    <node concept="3y3z36" id="4e730WeBilj" role="3clFbG">
                      <node concept="10Nm6u" id="4e730WeBinM" role="3uHU7w" />
                      <node concept="2OqwBi" id="4e730WeBhKg" role="3uHU7B">
                        <node concept="37vLTw" id="4e730WeBhF9" role="2Oq$k0">
                          <ref role="3cqZAo" node="4e730WeBhCA" resolve="it" />
                        </node>
                        <node concept="3CFZ6_" id="4e730WeBi9H" role="2OqNvi">
                          <node concept="3CFYIy" id="4e730WeBie3" role="3CFYIz">
                            <ref role="3CFYIx" to="6fcb:aOl7efxWUh" resolve="HerbieAnnotation" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="4e730WeBhCA" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="4e730WeBhCB" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="3rj977HwH1g" role="3cqZAp" />
        <node concept="2Gpval" id="3rj977HwHbO" role="3cqZAp">
          <node concept="2GrKxI" id="3rj977HwHbQ" role="2Gsz3X">
            <property role="TrG5h" value="rhss" />
          </node>
          <node concept="3clFbS" id="3rj977HwHbS" role="2LFqv$">
            <node concept="3cpWs8" id="3rj977HwLYy" role="3cqZAp">
              <node concept="3cpWsn" id="3rj977HwLY_" role="3cpWs9">
                <property role="TrG5h" value="rhsExpr" />
                <node concept="3Tqbb2" id="3rj977HwLYw" role="1tU5fm">
                  <ref role="ehGHo" to="pfd6:5l83jlMf$Lb" resolve="Expression" />
                </node>
                <node concept="2OqwBi" id="3rj977HwM84" role="33vP2m">
                  <node concept="2GrUjf" id="3rj977HwLZ0" role="2Oq$k0">
                    <ref role="2Gs0qQ" node="3rj977HwHbQ" resolve="rhss" />
                  </node>
                  <node concept="3TrEf2" id="3rj977HwMrh" role="2OqNvi">
                    <ref role="3Tt5mk" to="2gyk:1aS1l$r67g" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3rj977HwLY6" role="3cqZAp">
              <node concept="3clFbS" id="3rj977HwLY7" role="3clFbx">
                <node concept="34ab3g" id="3rj977HwNob" role="3cqZAp">
                  <property role="35gtTG" value="info" />
                  <node concept="Xl_RD" id="3rj977HwNod" role="34bqiv">
                    <property role="Xl_RC" value="[Herbie Generator] replacing right-hand side statement with optimized regime ..." />
                  </node>
                </node>
                <node concept="3clFbF" id="3rj977HwNrf" role="3cqZAp">
                  <node concept="2OqwBi" id="3rj977HwNtu" role="3clFbG">
                    <node concept="2GrUjf" id="3rj977HwNrd" role="2Oq$k0">
                      <ref role="2Gs0qQ" node="3rj977HwHbQ" resolve="rhss" />
                    </node>
                    <node concept="1P9Npp" id="3rj977HwNKS" role="2OqNvi">
                      <node concept="2OqwBi" id="3rj977HwOxs" role="1P9ThW">
                        <node concept="2OqwBi" id="3rj977HwNYx" role="2Oq$k0">
                          <node concept="2OqwBi" id="3rj977HwNMR" role="2Oq$k0">
                            <node concept="37vLTw" id="3rj977HwNLd" role="2Oq$k0">
                              <ref role="3cqZAo" node="3rj977HwLY_" resolve="rhsExpr" />
                            </node>
                            <node concept="3CFZ6_" id="3rj977HwNU5" role="2OqNvi">
                              <node concept="3CFYIy" id="3rj977HwNUT" role="3CFYIz">
                                <ref role="3CFYIx" to="6fcb:aOl7efxWUh" resolve="HerbieAnnotation" />
                              </node>
                            </node>
                          </node>
                          <node concept="3TrEf2" id="3rj977HwOka" role="2OqNvi">
                            <ref role="3Tt5mk" to="6fcb:1P5nnDyLH4t" />
                          </node>
                        </node>
                        <node concept="1$rogu" id="3rj977HwOGc" role="2OqNvi" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="3rj977HwNah" role="3clFbw">
                <node concept="2OqwBi" id="3rj977HwMKQ" role="2Oq$k0">
                  <node concept="2OqwBi" id="3rj977HwMyl" role="2Oq$k0">
                    <node concept="37vLTw" id="3rj977HwMtw" role="2Oq$k0">
                      <ref role="3cqZAo" node="3rj977HwLY_" resolve="rhsExpr" />
                    </node>
                    <node concept="3CFZ6_" id="3rj977HwMGQ" role="2OqNvi">
                      <node concept="3CFYIy" id="3rj977HwMI4" role="3CFYIz">
                        <ref role="3CFYIx" to="6fcb:aOl7efxWUh" resolve="HerbieAnnotation" />
                      </node>
                    </node>
                  </node>
                  <node concept="3TrEf2" id="3rj977HwMXZ" role="2OqNvi">
                    <ref role="3Tt5mk" to="6fcb:1P5nnDyLH4t" />
                  </node>
                </node>
                <node concept="3x8VRR" id="3rj977HwNlR" role="2OqNvi" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="3rj977HwIbs" role="2GsD0m">
            <node concept="2OqwBi" id="3rj977HwHjL" role="2Oq$k0">
              <node concept="1Q6Npb" id="3rj977HwHj1" role="2Oq$k0" />
              <node concept="2SmgA7" id="3rj977HwHr8" role="2OqNvi">
                <ref role="2SmgA8" to="2gyk:1aS1l$r677" resolve="RightHandSideStatement" />
              </node>
            </node>
            <node concept="3zZkjj" id="3rj977HwKlY" role="2OqNvi">
              <node concept="1bVj0M" id="3rj977HwKm0" role="23t8la">
                <node concept="3clFbS" id="3rj977HwKm1" role="1bW5cS">
                  <node concept="3clFbF" id="3rj977HwKsi" role="3cqZAp">
                    <node concept="2OqwBi" id="3rj977HwLy5" role="3clFbG">
                      <node concept="2OqwBi" id="3rj977HwLbX" role="2Oq$k0">
                        <node concept="2OqwBi" id="3rj977HwKwv" role="2Oq$k0">
                          <node concept="37vLTw" id="3rj977HwKsh" role="2Oq$k0">
                            <ref role="3cqZAo" node="3rj977HwKm2" resolve="rhs" />
                          </node>
                          <node concept="3TrEf2" id="3rj977HwKVn" role="2OqNvi">
                            <ref role="3Tt5mk" to="2gyk:1aS1l$r67g" />
                          </node>
                        </node>
                        <node concept="3CFZ6_" id="3rj977HwLlu" role="2OqNvi">
                          <node concept="3CFYIy" id="3rj977HwLpg" role="3CFYIz">
                            <ref role="3CFYIx" to="6fcb:aOl7efxWUh" resolve="HerbieAnnotation" />
                          </node>
                        </node>
                      </node>
                      <node concept="3x8VRR" id="3rj977HwLUP" role="2OqNvi" />
                    </node>
                  </node>
                </node>
                <node concept="Rh6nW" id="3rj977HwKm2" role="1bW2Oz">
                  <property role="TrG5h" value="rhs" />
                  <node concept="2jxLKc" id="3rj977HwKm3" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

